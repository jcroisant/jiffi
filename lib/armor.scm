;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;; An "armor" is a record type that wraps bare data, whether it is a
;; struct/union record type or an array record type.
;;
;; An armor record has the following slots:
;;
;;   0. data: The bare data that the armor wraps
;;   1. meta: A jiffi:meta describing the armor (private)
;;   *. User slots.

(export armor?
        armor-data
        armor-data-set!
        armor-address
        armor-eq?
        armor-null?
        armor-nullify!
        nullify-armor! ;; deprecated

        armor-parent
        armor-parent-set!
        armor-tracks-children?
        armor-tracks-children-set!

        armor-has-lock?
        armor-lock-set!
        make-armor-lock

        define-armor-type
        armor-printer
        define-armor-printer)


(: armor? (* --> boolean))
(define (armor? obj)
  (and (record-instance? obj)
       (< 1 (record-instance-length obj))
       (%jiffi:meta? (record-instance-slot obj 1))))


(: armor-data (* --> (or pointer locative blob false)))
(define (armor-data armor)
  (assert (armor? armor))
  (record-instance-slot armor 0))

(: armor-data-set! (* (or pointer locative blob false) -> void))
(define (armor-data-set! armor data)
  (assert (armor? armor))
  (assert (or (pointer? data)
              (locative? data)
              (blob? data)
              (not data))
          "Invalid data for armor" data armor)
  (record-instance-slot-set! armor 0 data))

(set! (setter armor-data) armor-data-set!)


(cond-expand
 (chicken-4 (: armor-address (* --> number)))
 (else      (: armor-address (* --> integer))))
(define (armor-address obj)
  (define (handle x orig)
    (cond
     ((not x)
      0)
     ((or (pointer? x) (locative? x))
      (pointer->address x))
     ((blob? x)
      (pointer->address (make-locative x)))
     (else
      (error 'armor-address "not an armor" orig))))
  (if (armor? obj)
      (handle (%jiffi:armor-data obj) obj)
      (handle obj obj)))


(: armor-eq? (* * --> boolean))
(define (armor-eq? obj1 obj2)
  (= (armor-address obj1) (armor-address obj2)))


(: armor-null? (* --> boolean))
(define (armor-null? obj)
  (define (handle x orig)
    (cond
     ((not x)
      #t)
     ((pointer? x)
      (zero? (pointer->address x)))
     ((or (locative? x) (blob? x))
      #f)
     (else
      (error 'armor-null? "not an armor" orig))))
  (if (armor? obj)
      (handle (%jiffi:armor-data obj) obj)
      (handle obj obj)))


(: armor-nullify! (* -> undefined))
(define (armor-nullify! armor)
  (assert (armor? armor))
  (when (%jiffi:armor-data armor)
    (set! (%jiffi:armor-data armor) #f)
    (and-let* ((parent (%jiffi:armor-parent armor)))
      ;; Tell parent to unregister this armor.
      (%with-armor-lock parent
                        %armor-remove-child parent armor)
      (set! (%jiffi:armor-parent armor) #f))
    ;; Nullify and forget all children.
    (%with-armor-lock armor
                      %armor-clear-children armor)
    (void)))

(: nullify-armor! (deprecated armor-nullify!))
(define (nullify-armor! armor)
  (armor-nullify! armor))


(: armor-parent (* --> *))
(define (armor-parent armor)
  (assert (armor? armor))
  (%jiffi:armor-parent armor))

(: armor-parent-set!
   (forall (c p) (c p -> c)))
(define (armor-parent-set! child parent)
  (assert (armor? child))
  (assert (armor? parent))
  (set! (%jiffi:armor-parent child) parent)
  (%with-armor-lock parent
                    %armor-add-child parent child)
  child)


(: armor-tracks-children? (* --> boolean))
(define (armor-tracks-children? armor)
  (assert (armor? armor))
  (not (not (%jiffi:armor-kids armor))))

(: armor-tracks-children-set! (* boolean -> undefined))
(define (armor-tracks-children-set! armor enabled)
  (assert (armor? armor))
  (cond
   ;; Now disabled
   ((not enabled)
    (set! (%jiffi:armor-kids armor) #f))
   ;; Previously disabled, now enabled
   ((and enabled (not (%jiffi:armor-kids armor)))
    (set! (%jiffi:armor-kids armor) '()))
   ;; Otherwise it is already enabled, so keep the known children
   ))

(set! (setter armor-tracks-children?)
      armor-tracks-children-set!)


;; These should be considered private, not part of Jiffi's API. They
;; are only exported so that they can be used in macro-generated code.
(export %jiffi:armor-data
        %jiffi:armor-parent
        %jiffi:armor-kids
        %jiffi:armor-lock
        %jiffi:meta)


;; Get the `data' private slot of an armor, which holds a pointer,
;; locative, blob, or #f.
(define (%jiffi:armor-data armor)
  (record-instance-slot armor 0))
(set! (setter %jiffi:armor-data)
      (lambda (armor data)
        (set! (record-instance-slot armor 0) data)))


;; A jiffi:meta currently has the following slots:
;;
;;   0. parent: Other armor whose memory this armor uses, or #f.
;;   1. kids: List of weak locatives to children, or #f.
;;   2. lock: Lock procedure (see make-armor-lock), or #f.
;;
(define-record-type jiffi:meta
  (%jiffi:meta parent kids lock)
  %jiffi:meta?
  (parent %jiffi:meta-parent (setter %jiffi:meta-parent))
  (kids   %jiffi:meta-kids   (setter %jiffi:meta-kids))
  (lock   %jiffi:meta-lock   (setter %jiffi:meta-lock)))

;; Get the `meta' private slot of an armor, which holds a jiffi:meta.
(define (%jiffi:armor-meta armor)
  (record-instance-slot armor 1))
(set! (setter %jiffi:armor-meta)
      (lambda (armor meta)
        (set! (record-instance-slot armor 1) meta)))


;; Get the `parent' metadata of an armor, which is either another
;; armor whose memory this armor uses, or #f if there is no parent.
(define (%jiffi:armor-parent armor)
  (%jiffi:meta-parent (%jiffi:armor-meta armor)))
(set! (setter %jiffi:armor-parent)
      (lambda (armor parent)
        (set! (%jiffi:meta-parent (%jiffi:armor-meta armor)) parent)))


;; Get the `kids' metadata of an armor, which is a list of weak
;; locatives to other armors that use this armor's memory, so they can
;; be nullified when this armor is nullified. Or, if the armor doesn't
;; track its children, this is #f.
(define (%jiffi:armor-kids armor)
  (%jiffi:meta-kids (%jiffi:armor-meta armor)))
(set! (setter %jiffi:armor-kids)
      (lambda (armor kids)
        (set! (%jiffi:meta-kids (%jiffi:armor-meta armor)) kids)))


(define (%armor-add-child armor child)
  (if (armor-null? armor)
      ;; Armor is already null, so nullify child immediately instead
      ;; of adding it to the list to nullify later.
      (armor-nullify! child)
      ;; Otherwise, if armor tracks children, add child to the list.
      (and-let* ((kids (%jiffi:armor-kids armor)))
        (set! (%jiffi:armor-kids armor)
              (cons (make-weak-locative child)
                    kids)))))


(define (%armor-remove-child armor child)
  (and-let* ((kids (%jiffi:armor-kids armor)))
    ;; Remove weak locatives whose object is the child or #f (which
    ;; means object was GC'd), so stale locatives don't pile up.
    (set! (%jiffi:armor-kids armor)
          (remove! (lambda (weak-loc)
                     (let ((obj (locative->object weak-loc)))
                       (or (eq? child obj)
                           (not obj))))
                   kids))))


(define (%armor-clear-children armor)
  (and-let* ((kids (%jiffi:armor-kids armor)))
    (set! (%jiffi:armor-kids armor) '())
    (for-each (lambda (child-loc)
                (let ((child (locative->object child-loc)))
                  ;; Child may have already been GC'd
                  (when child
                    ;; Clear child's parent first, so armor-nullify!
                    ;; won't waste time acquiring a lock on the parent
                    ;; and calling %armor-remove-child.
                    (set! (%jiffi:armor-parent child) #f)
                    (armor-nullify! child))))
              kids)))


;; Get the `lock' metadata of an armor, which is a procedure that
;; invokes the given procedure when the current thread has exclusive
;; access to modify the armor.
(define (%jiffi:armor-lock armor)
  (%jiffi:meta-lock (%jiffi:armor-meta armor)))
(set! (setter %jiffi:armor-lock)
      (lambda (armor lock)
        (set! (%jiffi:meta-lock (%jiffi:armor-meta armor)) lock)))

;; Acquire armor's lock if it has one, and call proc with args.
(define (%with-armor-lock armor proc . args)
  (let ((lock (%jiffi:armor-lock armor)))
    (if lock
        (apply lock proc args)
        (apply proc args))))


(: armor-has-lock? (* --> boolean))
(define (armor-has-lock? armor)
  (assert (armor? armor))
  (not (not (%jiffi:armor-lock armor))))

(define (armor-lock-set! armor lock)
  (set! (%jiffi:armor-lock armor) lock))


(define-syntax make-armor-lock
  (syntax-rules ()
    ;; Evaluates to a thread-safe lock procedure. Assumes that srfi-18
    ;; has been imported into the current module.
    ((make-armor-lock)
     (let ((mutex (make-mutex "jiffi armor lock")))
       (lambda (proc . args)
         (thread-start!
          (lambda ()
            (mutex-lock! mutex #f (current-thread))
            (let ((result (apply proc args)))
              (mutex-unlock! mutex)
              result))))))))


;; (define-armor-type <ARMOR-NAME>
;;   pred: <PREDICATE>
;;   wrap: <WRAP>
;;   unwrap: <UNWRAP>
;;   children: <CHILD-MODE>
;;   locking: <LOCK-EXPR>
;;   (<SLOT> <SLOT-GETTER> [<SLOT-SETTER>])
;;   ...)
;;
(define-syntax define-armor-type
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <ARMOR-NAME>
                        <PREDICATE>
                        <WRAP>
                        <UNWRAP>
                        <CHILD-MODE>
                        <LOCK-EXPR>
                        <SLOTS>
                        %wrap)
       `(begin
          (: ,<PREDICATE>
             (* --> boolean : (struct ,<ARMOR-NAME>)))
          (define-record-type ,<ARMOR-NAME>
            (,%wrap data meta ,@(map car <SLOTS>))
            ,<PREDICATE>

            ;; We don't need getters for these slots, but
            ;; define-record-type requires a getter name.
            (data ,(gensym (symbol-append '% <ARMOR-NAME> '-data)))
            (meta ,(gensym (symbol-append '% <ARMOR-NAME> '-meta)))
            ,@<SLOTS>)

          (: ,<WRAP>
             ((or pointer locative blob false)
              #!optional
              ,@(map (lambda _ '*) <SLOTS>)
              -> (struct ,<ARMOR-NAME>)))
          (define (,<WRAP>
                   data
                   #!optional
                   ,@(map (lambda (slot)
                            (list (car slot) '(void)))
                          <SLOTS>))
            (,%wrap
             data
             (%jiffi:meta #f ;; parent
                          (and ,<CHILD-MODE> '())
                          ,<LOCK-EXPR>)
             ,@(map car <SLOTS>)))

          (: ,<UNWRAP>
             ((or (struct ,<ARMOR-NAME>) pointer locative blob false)
              #!optional *
              -> (or pointer locative false)))
          (define (,<UNWRAP> obj #!optional name)
            (let ((handle
                   (lambda (x orig)
                     (cond
                      ((or (pointer? x) (locative? x))
                       x)
                      ((blob? x)
                       (make-locative x))
                      ((not x)
                       #f)
                      (else
                       (error name
                              (sprintf "not a ~A" ',<ARMOR-NAME>)
                              orig))))))
              (cond
               ((,<PREDICATE> obj)
                (handle (%jiffi:armor-data obj) obj))
               (else
                (handle obj obj)))))))
     ;; end (construct)

     (let-values (((heads keys bodies)
                   (extract-args
                    form
                    head: 1
                    keys: '((pred:     required!)
                            (wrap:     required!)
                            (unwrap:   required!)
                            (children: #t)
                            (locking:  #f))
                    body: #t)))
       (let ((<ARMOR-NAME>  (first heads))
             (<PREDICATE>   (alist-ref #:pred keys))
             (<WRAP>        (alist-ref #:wrap keys))
             (<UNWRAP>      (alist-ref #:unwrap keys))
             (<CHILD-MODE>  (alist-ref #:children keys))
             (<LOCK-EXPR>   (alist-ref #:locking keys))
             (<SLOTS>       bodies))
         (let ((%wrap (gensym (symbol-append '%wrap- <ARMOR-NAME>))))
           (construct <ARMOR-NAME>
                      <PREDICATE>
                      <WRAP>
                      <UNWRAP>
                      <CHILD-MODE>
                      <LOCK-EXPR>
                      <SLOTS>
                      %wrap)))))))


(define-syntax armor-printer
  (syntax-rules (show-address:)
    ((armor-printer ARMOR-NAME
       show-address: SHOW-ADDRESS
       (FIELD-LABEL FIELD-GETTER)
       ...)
     (lambda (record out)
       (let ((address (armor-address record)))
         (cond
          ((= 0 address)
           (fprintf out "#<~A NULL>" 'ARMOR-NAME))
          (else
           (fprintf out "#<~A" 'ARMOR-NAME)
           (macro-when SHOW-ADDRESS
             (fprintf out " 0x~X" address))
           (begin
             (macro-when FIELD-LABEL
               (fprintf out " ~A:" 'FIELD-LABEL))
             (fprintf out " ~S" (FIELD-GETTER record)))
           ...
           (display ">" out))))))

    ;; Normalize keyword arguments
    ((define-armor-printer R ARGS ...)
     (%normalize-keyword-args
      (define-armor-printer R ARGS ...)
      head: 1
      keys: ((show-address: #f))
      body: #t))))


(define-syntax define-armor-printer
  (syntax-rules (show-address:)
    ((define-armor-printer ARMOR-NAME
       show-address: SHOW-ADDRESS
       (FIELD-LABEL FIELD-GETTER)
       ...)
     (cond-expand
      (chicken-4
       (define-record-printer (ARMOR-NAME record out)
         ((armor-printer ARMOR-NAME
            show-address: SHOW-ADDRESS
            (FIELD-LABEL FIELD-GETTER)
            ...)
          record out)))
      (chicken-5
       (set-record-printer!
        ARMOR-NAME
        (armor-printer ARMOR-NAME
          show-address: SHOW-ADDRESS
          (FIELD-LABEL FIELD-GETTER)
          ...)))))

    ;; Normalize keyword arguments
    ((define-armor-printer R ARGS ...)
     (%normalize-keyword-args
      (define-armor-printer R ARGS ...)
      head: 1
      keys: ((show-address: #f))
      body: #t))))
