;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export define-struct-allocators

        struct-copier

        define-struct-accessors
        struct-getter
        struct-setter)


;; (define-struct-allocators
;;   (<ARMOR-NAME> <STRUCT_NAME> <PRED> <WRAP>)
;;   free:       <FREE>
;;   alloc:      <ALLOC>
;;   alloc/blob: <ALLOC/BLOB>
;;   make:       <MAKE>
;;   make/af:    <MAKE/AUTOFREE>
;;   make/blob:  <MAKE/BLOB>
;;   defaults:   (<SLOT-DEFAULT> ...))
;;
(define-syntax define-struct-allocators
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <ARMOR-NAME>
                        <STRUCT_NAME>
                        <PRED>
                        <WRAP>
                        <FREE>
                        <ALLOC>
                        <ALLOC/BLOB>
                        <MAKE>
                        <MAKE/AUTOFREE>
                        <MAKE/BLOB>
                        <DEFAULTS>)
       `(begin
          ;; Procedure names are first declared, then later set! so
          ;; they can all use the helper procedures.

          ,(maybe-declare <FREE>
             `((struct ,<ARMOR-NAME>) -> (struct ,<ARMOR-NAME>)))
          ,(maybe-declare <ALLOC>
             '(--> pointer))
          ,(maybe-declare <ALLOC/BLOB>
             '(--> blob))
          ,(maybe-declare <MAKE>
             `(--> (struct ,<ARMOR-NAME>)))
          ,(maybe-declare <MAKE/AUTOFREE>
             `(--> (struct ,<ARMOR-NAME>)))
          ,(maybe-declare <MAKE/BLOB>
             `(--> (struct ,<ARMOR-NAME>)))

          (let ((%alloc-memory
                 (lambda ()
                   (define foreign
                     (foreign-lambda**
                      (c-pointer ,<STRUCT_NAME>)
                      ()
                      ("C_return(calloc(1, sizeof(~A)));" ,<STRUCT_NAME>)))
                   (tag-pointer (foreign) ',<ARMOR-NAME>)))

                (%alloc-blob
                 (lambda ()
                   (let ((blob (make-blob (foreign-type-size ,<STRUCT_NAME>))))
                     ((foreign-lambda**
                       void
                       ((c-pointer ptr))
                       ("memset(ptr, 0, sizeof(~A));" ,<STRUCT_NAME>))
                      (make-locative blob))
                     blob)))

                (%free-struct!
                 (lambda (struct)
                   (let ((data (%jiffi:armor-data struct))
                         (parent (%jiffi:armor-parent struct)))
                     (armor-nullify! struct)
                     (when (and (pointer? data)
                                (not parent))
                       (free data)))
                   struct)))

            ,(maybe-set! <FREE>
               `(lambda (struct)
                  (assert (,<PRED> struct))
                  (%free-struct! struct)
                  struct))

            ,(maybe-set! <ALLOC> '%alloc-memory)

            ,(maybe-set! <ALLOC/BLOB> '%alloc-blob)

            ,(maybe-set! <MAKE>
               `(lambda ()
                  (,<WRAP> (%alloc-memory) ,@<DEFAULTS>)))

            ,(maybe-set! <MAKE/AUTOFREE>
               `(lambda ()
                  (set-finalizer!
                   (,<WRAP> (%alloc-memory) ,@<DEFAULTS>)
                   %free-struct!)))

            ,(maybe-set! <MAKE/BLOB>
               `(lambda ()
                  (,<WRAP> (%alloc-blob) ,@<DEFAULTS>))))))
     ;; end (construct)

     (let-values (((heads keys _bodies)
                   (extract-args
                    form
                    head: 1
                    keys: '((free:       #f)
                            (alloc:      #f)
                            (alloc/blob: #f)
                            (make:       #f)
                            (make/af:    #f)
                            (make/blob:  #f)
                            (defaults:   ())))))
       (let ((<ARMOR-NAME>    (list-ref (first heads) 0))
             (<STRUCT_NAME>   (list-ref (first heads) 1))
             (<PRED>          (list-ref (first heads) 2))
             (<WRAP>          (list-ref (first heads) 3))
             (<FREE>          (alist-ref #:free keys))
             (<ALLOC>         (alist-ref #:alloc keys))
             (<ALLOC/BLOB>    (alist-ref #:alloc/blob keys))
             (<MAKE>          (alist-ref #:make keys))
             (<MAKE/AUTOFREE> (alist-ref #:make/af keys))
             (<MAKE/BLOB>     (alist-ref #:make/blob keys))
             (<DEFAULTS>      (alist-ref #:defaults keys)))
         (construct <ARMOR-NAME>
                    <STRUCT_NAME>
                    <PRED>
                    <WRAP>
                    <FREE>
                    <ALLOC>
                    <ALLOC/BLOB>
                    <MAKE>
                    <MAKE/AUTOFREE>
                    <MAKE/BLOB>
                    <DEFAULTS>))))))


(define-syntax struct-copier
  (syntax-rules ()
    ((struct-copier
      (STRUCT_NAME UNWRAP)
      name: NAME)
     (lambda (src dest)
       (assert (not (armor-null? src)))
       (assert (not (armor-null? dest)))
       ((foreign-lambda*
         void
         (((c-pointer STRUCT_NAME) src)
          ((c-pointer STRUCT_NAME) dest))
         "*dest = *src;")
        (UNWRAP src)
        (UNWRAP dest))
       dest))

    ;; Normalize keyword arguments
    ((struct-copier (S U) ARGS ...)
     (%normalize-keyword-args
      (struct-copier (S U) F ARGS ...)
      head: 1
      keys: ((name: #f))))))


(define-syntax define-struct-accessors
  (syntax-rules ()
    ((define-struct-accessors
       (<ARMOR-NAME> <STRUCT_NAME> <PRED> <UNWRAP>)
       (<FIELD_NAME> <ARGS> ...)
       ...)
     (begin
       (define-struct-accessor
         (<ARMOR-NAME> <STRUCT_NAME> <PRED> <UNWRAP>)
         <FIELD_NAME> <ARGS> ...)
       ...))))


;; (define-struct-accessor
;;   (<ARMOR-NAME> <STRUCT_NAME> <PRED> <UNWRAP>)
;;   <FIELD_NAME>
;;   type:   (<SCHEME-TYPE> <FOREIGN-TYPE>) ; or <SCHEME-TYPE>
;;   getter: <GETTER>
;;   g-conv: <G-CONV>
;;   setter: <SETTER>
;;   s-conv: <S-CONV>)
;;
(define-syntax define-struct-accessor
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <ARMOR-NAME>
                        <STRUCT_NAME>
                        <PRED>
                        <UNWRAP>
                        <FIELD_NAME>
                        <SCHEME-TYPE>
                        <FOREIGN-TYPE>
                        <GETTER>
                        <G-CONV>
                        <SETTER>
                        <S-CONV>)
       `(begin
          ,(and <GETTER>
                `(begin
                   (: ,<GETTER>
                      ((or (struct ,<ARMOR-NAME>) pointer locative blob false)
                       -> ,<SCHEME-TYPE>))
                   (define ,<GETTER>
                     (struct-getter
                      (,<ARMOR-NAME> ,<STRUCT_NAME> ,<PRED> ,<UNWRAP>)
                      ,<FIELD_NAME>
                      type: ,<FOREIGN-TYPE>
                      conv: ,<G-CONV>
                      name: ',<GETTER>))))
          ,(cond
            ((and <SETTER> (not (list? <SETTER>)))
             `(begin
                (: ,<SETTER>
                   ((or (struct ,<ARMOR-NAME>) pointer locative blob false)
                    ,<SCHEME-TYPE> -> void))
                (define ,<SETTER>
                  (struct-setter
                   (,<ARMOR-NAME> ,<STRUCT_NAME> ,<PRED> ,<UNWRAP>)
                   ,<FIELD_NAME>
                   type: ,<FOREIGN-TYPE>
                   conv: ,<S-CONV>
                   name: ',<SETTER>))))
            ((and <SETTER> (compare 'setter (car <SETTER>)))
             `(set! (setter ,(cadr <SETTER>))
                    (struct-setter
                     (,<ARMOR-NAME> ,<STRUCT_NAME> ,<PRED> ,<UNWRAP>)
                     ,<FIELD_NAME>
                     name: ',(cadr <SETTER>)
                     type: ,<FOREIGN-TYPE>
                     conv: ,<S-CONV>)))
            (else #f))))
     ;; end (construct)

     (let-values (((heads keys _bodies)
                   (extract-args
                    form
                    head: 2
                    keys: '((type:   required!)
                            (getter: #f)
                            (g-conv: #f)
                            (setter: #f)
                            (s-conv: #f)))))
       (let ((<ARMOR-NAME>  (first  (first heads)))
             (<STRUCT_NAME> (second (first heads)))
             (<PRED>        (third  (first heads)))
             (<UNWRAP>      (fourth (first heads)))
             (<FIELD_NAME>  (second heads))
             (<TYPE>        (alist-ref #:type keys))
             (<GETTER>      (alist-ref #:getter keys))
             (<G-CONV>      (alist-ref #:g-conv keys))
             (<SETTER>      (alist-ref #:setter keys))
             (<S-CONV>      (alist-ref #:s-conv keys)))
         (let ((<SCHEME-TYPE>  (if (list? <TYPE>)
                                   (first <TYPE>)
                                   <TYPE>))
               (<FOREIGN-TYPE> (if (list? <TYPE>)
                                   (second <TYPE>)
                                   <TYPE>)))
           (construct <ARMOR-NAME>
                      <STRUCT_NAME>
                      <PRED>
                      <UNWRAP>
                      <FIELD_NAME>
                      <SCHEME-TYPE>
                      <FOREIGN-TYPE>
                      <GETTER>
                      <G-CONV>
                      <SETTER>
                      <S-CONV>)))))))


;; Creates (but does not define) a struct field getter.
(define-syntax struct-getter
  (syntax-rules (type: conv:)
    ((struct-getter
      (ARMOR-NAME STRUCT_NAME PRED UNWRAP)
      FIELD_NAME
      type: FOREIGN-TYPE
      conv: CONV
      name: NAME)
     (let ((conv CONV)
           (raw (foreign-lambda**
                 FOREIGN-TYPE
                 (((c-pointer STRUCT_NAME) obj))
                 ("C_return(obj->~A);" FIELD_NAME))))
       (lambda (obj)
         (macro-if CONV
           (conv (raw (UNWRAP obj NAME)))
           (raw (UNWRAP obj NAME))))))

    ;; Normalize keyword arguments
    ((struct-getter (R S P U) F ARGS ...)
     (%normalize-keyword-args
      (struct-getter (R S P U) F ARGS ...)
      head: 2
      keys: ((type: required!)
             (conv: #f)
             (name: #f))))))


;; Creates (but does not define) a struct field setter.
(define-syntax struct-setter
  (syntax-rules (type: conv:)
    ((struct-setter
      (ARMOR-NAME STRUCT_NAME PRED UNWRAP)
      FIELD_NAME
      type: FOREIGN-TYPE
      conv: CONV
      name: NAME)
     (let ((conv CONV)
           (raw (foreign-lambda**
                 void
                 (((c-pointer STRUCT_NAME) obj)
                  (FOREIGN-TYPE val))
                 ("obj->~A = val;" FIELD_NAME))))
       (lambda (obj value)
         (macro-if CONV
           (raw (UNWRAP obj NAME) (conv value))
           (raw (UNWRAP obj NAME) value)))))

    ;; Normalize keyword arguments
    ((struct-setter (R S P U) F ARGS ...)
     (%normalize-keyword-args
      (struct-setter (R S P U) F ARGS ...)
      head: 2
      keys: ((type: required!)
             (conv: #f)
             (name: #f))))))

