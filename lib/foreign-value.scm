;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export define-foreign-values)


(define-syntax define-foreign-values
  (syntax-rules (export: type:)
    ((define-foreign-values
       export: EXPORT
       type: FOREIGN-TYPE
       SPECIFICATION ...)
     (begin
       (%define-foreign-value
        export: EXPORT
        type: FOREIGN-TYPE
        SPECIFICATION)
       ...))

    ;; Normalize keyword arguments
    ((define-foreign-values ARGS ...)
     (%normalize-keyword-args
      (define-foreign-values ARGS ...)
      keys: ((export: #f)
             (type:   required!))
      body: #t))))


(define-syntax %define-foreign-value
  (syntax-rules (export: type:)
    ;; IDENTIFIER and VALUE.
    ((%define-foreign-value
      export: EXPORT
      type: FOREIGN-TYPE
      (IDENTIFIER EXPRESSION))
     (begin (macro-when EXPORT (export IDENTIFIER))
            (define IDENTIFIER
              (foreign-value EXPRESSION FOREIGN-TYPE))))

    ;; Only IDENTIFIER. Reexpand with (IDENTIFIER IDENTIFIER).
    ((%define-foreign-value
      export: EXPORT
      type: FOREIGN-TYPE
      IDENTIFIER)
     (%define-foreign-value
      export: EXPORT
      type: FOREIGN-TYPE
      (IDENTIFIER IDENTIFIER)))))
