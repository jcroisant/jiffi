;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export define-enum-group
        define-enum-packer
        define-enum-unpacker)


;; (define-enum-group
;;   type: <FOREIGN-TYPE>
;;   vars: <VARS-MODE>
;;
;;   symbol->int: <SYMBOL->INT>
;;   allow-ints: <ALLOW-INTS>
;;
;;   int->symbol: <INT->SYMBOL>
;;
;;   ((requires: <FEATURE-CLAUSE> <C_PREPROCESSOR_EXPR>) ; optional
;;    (<SYMBOL> <VAR/CONSTANT> . <MAYBE-FLAGS>)
;;    ...)
;;   ...)
;;
(define-syntax define-enum-group
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <FOREIGN-TYPE>
                        <VARS-MODE>
                        <SYMBOL->INT>
                        <ALLOW-INTS>
                        <INT->SYMBOL>
                        <BRANCHES>)
       `(begin
          ,@(if (or (compare 'define <VARS-MODE>)
                    (compare 'export <VARS-MODE>))
                (map (lambda (branch)
                       (let ((<FEATURE-CLAUSE>
                              (second (first branch)))
                             (var/constants
                              (map second (inject (cdr branch)))))
                         `(cond-expand
                           (,<FEATURE-CLAUSE>
                            (define-foreign-values
                              export: ,(compare 'export <VARS-MODE>)
                              type: ,<FOREIGN-TYPE>
                              ,@var/constants))
                           (else))))
                     (strip-syntax <BRANCHES>))
                '())

          ,(and <SYMBOL->INT>
                (%define-symbol->int
                 <FOREIGN-TYPE>
                 <SYMBOL->INT>
                 <ALLOW-INTS>
                 <BRANCHES>))

          ,(and <INT->SYMBOL>
                (%define-int->symbol
                 <INT->SYMBOL>
                 <FOREIGN-TYPE>
                 <BRANCHES>))))

     ;; If the branch does not already have a (requires:) clause, add
     ;; one that will always be satisfied.
     (define (normalize-branch branch)
       (if (compare #:requires (first (first branch)))
           branch
           (cons '(requires: chicken "1") branch)))

     (let-values (((_heads keys bodies)
                   (extract-args
                    form
                    keys: '((type: required!)
                            (vars: define)
                            (symbol->int: #f)
                            (allow-ints:  #f)
                            (int->symbol: #f))
                    body: #t)))
       (let ((<FOREIGN-TYPE> (alist-ref #:type keys))
             (<VARS-MODE>    (alist-ref #:vars keys))
             (<SYMBOL->INT>  (alist-ref #:symbol->int keys))
             (<ALLOW-INTS>   (alist-ref #:allow-ints keys))
             (<INT->SYMBOL>  (alist-ref #:int->symbol keys))
             (<BRANCHES>     (map normalize-branch bodies)))
         (construct <FOREIGN-TYPE>
                    <VARS-MODE>
                    <SYMBOL->INT>
                    <ALLOW-INTS>
                    <INT->SYMBOL>
                    <BRANCHES>))))))


(begin-for-syntax
 (define (%define-symbol->int
          <FOREIGN-TYPE>
          <SYMBOL->INT>
          <ALLOW-INTS>
          <BRANCHES>)
   (define (case-for-branch branch)
     (let ((<FEATURE-CLAUSE> (second (first branch)))
           (entries (cdr branch)))
       `(cond-expand
         (,<FEATURE-CLAUSE>
          (case symbol
            ,@(map clause-for-entry entries)))
         (else))))

   (define (clause-for-entry entry)
     (let* ((<SYMBOL> (first entry))
            (<VAR/CONSTANT> (second entry))
            (<CONSTANT> (if (list? <VAR/CONSTANT>)
                            (second <VAR/CONSTANT>)
                            <VAR/CONSTANT>)))
       `((,<SYMBOL>)
         (return (foreign-value ,<CONSTANT> ,<FOREIGN-TYPE>)))))

   `(begin
      `(let ((int (integer-type)))
         (if <ALLOW-INTS>
             `(: ,<SYMBOL->INT>
                 ((or symbol ,int) #!optional (* -> *)
                  -> ,int))
             `(: ,<SYMBOL->INT>
                 (symbol #!optional (* -> *) -> ,int))))
      (define (,<SYMBOL->INT> symbol #!optional not-found-callback)
        (if (and ',<ALLOW-INTS> (integer? symbol))
            symbol
            (call-with-current-continuation
             (lambda (return)
               ,@(map case-for-branch <BRANCHES>)
               (if not-found-callback
                   (not-found-callback symbol)
                   (error ',<SYMBOL->INT>
                          "unrecognized symbol"
                          symbol))))))))


 (define (%define-int->symbol
          <INT->SYMBOL>
          <FOREIGN-TYPE>
          <BRANCHES>)
   `(begin
      (: ,<INT->SYMBOL> (* #!optional (* -> *) -> symbol))
      (define (,<INT->SYMBOL> int #!optional not-found-callback)
        (define foreign
          ,(%foreign-int->symbol
            <FOREIGN-TYPE>
            (map (lambda (branch)
                   (let ((<C_PREPROCESSOR_EXPR> (third (first branch)))
                         (entries (cdr branch)))
                     (cons <C_PREPROCESSOR_EXPR> entries)))
                 <BRANCHES>)))
        (or (foreign int)
            (if not-found-callback
                (not-found-callback int)
                (error ',<INT->SYMBOL> "unrecognized value" int))))))


 ;; Usage:
 ;;
 ;;   (%foreign-int->symbol
 ;;    '<FOREIGN-TYPE>
 ;;    '("<C_PREPROCESSOR_EXPR>"
 ;;      (<SYMBOL> <CONSTANT> [<FLAG> ...])
 ;;      ...)
 ;;    ...)
 ;;
 ;; Expands to something like:
 ;;
 ;;   (foreign-primitive
 ;;    ((<FOREIGN-TYPE> x))
 ;;    "C_word av[2+1] = {C_SCHEME_UNDEFINED, C_k, C_SCHEME_FALSE};"
 ;;    "C_word* a;"
 ;;    "switch(x) {"
 ;;    "#if <C_PREPROCESSOR_EXPR>
 ;;    "case <CONSTANT>:
 ;;      a = C_alloc(C_SIZEOF_INTERNED_SYMBOL(sizeof(\"<SYMBOL>\")));
 ;;      av[2+1-1] = C_intern2(&a, \"<SYMBOL>\");
 ;;      break;
 ;;    }"
 ;;    ...
 ;;    "#endif"
 ;;    ...
 ;;    "C_values(2+1, av);")
 ;;
 ;; This allocates and returns an interned symbol based on which case
 ;; of the switch statement matches. If no case matches, it returns #f.
 ;;
 (define (%foreign-int->symbol <FOREIGN-TYPE> branches)
   (define (feature-branch <C_PREPROCESSOR_EXPR> . symbol-consts)
     (append
      (list (string-append "\n#if " <C_PREPROCESSOR_EXPR> "\n"))
      (map (cut apply enum-branch <>)
           (map (cut map strip-syntax <>) symbol-consts))
      (list "\n#endif\n")))

   (define (enum-branch <SYMBOL> <VAR/CONSTANT> . <FLAGS>)
     (let ((<CONSTANT> (if (list? <VAR/CONSTANT>)
                           (second <VAR/CONSTANT>)
                           <VAR/CONSTANT>)))
       (cond
        ;; Omit aliases to avoid "duplicate case value" C error.
        ((memq 'alias <FLAGS>)
         "")
        (else
         (sprintf
          "case ~A:
             a = C_alloc(C_SIZEOF_INTERNED_SYMBOL(sizeof(\"~A\")));
             av[2+1-1] = C_intern2(&a, \"~A\");
             break;"
          <CONSTANT> <SYMBOL> <SYMBOL>)))))

   `(foreign-primitive
     ((,(strip-syntax <FOREIGN-TYPE>) x))
     "C_word av[2+1] = {C_SCHEME_UNDEFINED, C_k, C_SCHEME_FALSE};"
     "C_word* a;"
     "switch(x) {"
     ,@(apply append
         (map (cut apply feature-branch <>) branches))
     "}"
     "C_values(2+1, av);")))


;; (define-enum-packer <PACKER>
;;   (<SYMBOL->INT>)
;;   allow-ints: <ALLOW-INTS>)
;;
(define-syntax define-enum-packer
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <PACKER>
                        <SYMBOL->INT>
                        <ALLOW-INTS>)
       `(begin
          ,(let ((int (integer-type)))
             (if <ALLOW-INTS>
                 `(: ,<PACKER>
                     ((or symbol ,int (list-of (or symbol ,int)))
                      #!optional procedure
                      -> ,int))
                 `(: ,<PACKER>
                     ((or symbol (list-of symbol))
                      #!optional procedure
                      -> ,int))))

          (define (,<PACKER> enums #!optional not-found-callback)
            (define (enum->int enum)
              (if (and ',<ALLOW-INTS> (integer? enum))
                  enum
                  (,<SYMBOL->INT> enum not-found-callback)))
            (if (list? enums)
                (apply bitwise-ior (map enum->int enums))
                (enum->int enums)))))
     ;; end (construct)

     (let-values (((heads keys _bodies)
                   (extract-args
                    form
                    head: 2
                    keys: '((allow-ints: #f)))))
       (let ((<PACKER>      (first heads))
             (<SYMBOL->INT> (first (second heads)))
             (<ALLOW-INTS>  (alist-ref #:allow-ints keys)))
         (construct <PACKER>
                    <SYMBOL->INT>
                    <ALLOW-INTS>))))))


;; (define-enum-unpacker UNPACKER
;;   (INT->SYMBOL)
;;   masks: BITMASKS)
;;
(define-syntax define-enum-unpacker
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <UNPACKER>
                        <INT->SYMBOL>
                        <BITMASKS>)
       `(begin
          (: UNPACKER (,(integer-type) --> (list-of symbol)))
          (define ,<UNPACKER>
            (let* ((bitmasks ,<BITMASKS>)
                   (symbols (map ,<INT->SYMBOL> bitmasks)))
              (lambda (bitfield)
                (filter-map
                 (lambda (mask symbol)
                   (and (= mask (bitwise-and bitfield mask))
                        symbol))
                 bitmasks symbols))))))

     (let-values (((heads keys _bodies)
                   (extract-args
                    form
                    head: 2
                    keys: '((masks: 'required!)))))
       (let ((<UNPACKER>    (first heads))
             (<INT->SYMBOL> (first (second heads)))
             (<BITMASKS>    (alist-ref #:masks keys)))
         (construct <UNPACKER>
                    <INT->SYMBOL>
                    <BITMASKS>))))))
