;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;; Macro expansion time if expression. Checks whether <TEST> is the
;; literal #f at macro expansion time.
(define-syntax macro-if
  (syntax-rules ()
    ((macro-if #f <PASS> <FAIL>)
     <FAIL>)
    ((macro-if <TEST> <PASS> <FAIL>)
     <PASS>)))

(define-syntax macro-when
  (syntax-rules ()
    ((macro-when #f <BODY> ...)
     (begin))
    ((macro-when <TEST> <BODY> ...)
     (begin <BODY> ...))))


(begin-for-syntax
 ;; Used by macros that declare multiple procedures then later set!
 ;; them so they can all use some shared helper procedures.
 (define (maybe-declare fn-name . type)
   (and fn-name
        `(begin
           (: ,fn-name ,@type)
           (define ,fn-name))))

 (define (maybe-set! fn-name fn)
   (and fn-name `(set! ,fn-name ,fn)))

 ;; CHICKEN 4 did not have the `integer` type specifier. The range of
 ;; the `fixnum` type specifier might be too small for some integers,
 ;; so the best type on CHICKEN 4 is `number`.
 (define (integer-type)
   (if (feature? 'chicken-4)
       'number
       'integer))

 ;; extract-args is in a separate file to make it easier to test.
 (include "lib/extract-args.scm"))


;; Accepts a macro form that has keyword arguments. Expands to the
;; same form with all the keywords at the canonical order with missing
;; keyword arguments filled in with defaults. See extract-args.
;;
;; Usage:
;;
;;   (%normalize-keyword-args
;;    <FORM>
;;    head: <NUM-HEADS>
;;    keys: ((KEY1: <DEFAULT1>)
;;           (KEY2: required!))
;;    body: <BODY-ARGS?>)
;;
(define-syntax %normalize-keyword-args
  (ir-macro-transformer
   (lambda (norm-form inject compare?)
     (let-values (((heads keys bodies)
                   (apply extract-args (cdr norm-form))))
       `(,(caadr norm-form)
         ,@heads
         ,@(apply append
             (map (lambda (key-val)
                    (list (car key-val) (cdr key-val)))
                  keys))
         ,@bodies)))))
