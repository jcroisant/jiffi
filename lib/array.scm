;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export define-array-allocators
        define-array-accessors

        define-array-copy!)


;; (define-array-allocators
;;   (<ARMOR-NAME> <STRUCT_NAME> <PRED> <WRAP>)
;;   free:       <FREE>
;;   alloc:      <ALLOC>
;;   alloc/blob: <ALLOC/BLOB>
;;   make:       <MAKE>
;;   make/af:    <MAKE/AUTOFREE>
;;   make/blob:  <MAKE/BLOB>
;;   defaults:   (<DEFAULT> ...))
;;
(define-syntax define-array-allocators
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <ARMOR-NAME>
                        <STRUCT_NAME>
                        <PRED>
                        <WRAP>
                        <FREE>
                        <ALLOC>
                        <ALLOC/BLOB>
                        <MAKE>
                        <MAKE/AUTOFREE>
                        <MAKE/BLOB>
                        <DEFAULTS>
                        %alloc-memory
                        %alloc-blob
                        %free-array!)
       `(begin
          ;; Procedure names are first declared, then later set! so
          ;; they can all use the helper procedures.

          ,(maybe-declare <FREE>
             `((struct ,<ARMOR-NAME>) -> (struct ,<ARMOR-NAME>)))
          ,(maybe-declare <ALLOC>
             '(fixnum --> pointer))
          ,(maybe-declare <ALLOC/BLOB>
             '(fixnum --> blob))
          ,(maybe-declare <MAKE>
             `(fixnum --> (struct ,<ARMOR-NAME>)))
          ,(maybe-declare <MAKE/AUTOFREE>
             `(fixnum --> (struct ,<ARMOR-NAME>)))
          ,(maybe-declare <MAKE/BLOB>
             `(fixnum --> (struct ,<ARMOR-NAME>)))

          (let ((,%alloc-memory
                 (lambda (length)
                   (define foreign
                     (foreign-lambda**
                      (c-pointer ,<STRUCT_NAME>)
                      ((size_t length))
                      ("C_return(calloc(length, sizeof(~A)));"
                       ,<STRUCT_NAME>)))
                   (tag-pointer (foreign length) ',<ARMOR-NAME>)))

                (,%alloc-blob
                 (lambda (length)
                   (let* ((size (* length (foreign-type-size ,<STRUCT_NAME>)))
                          (blob (make-blob size)))
                     ((foreign-lambda*
                       void
                       ((c-pointer ptr) (size_t size))
                       "memset(ptr, 0, size);")
                      (make-locative blob) size)
                     blob)))

                (,%free-array!
                 (lambda (array)
                   (let ((data (%jiffi:armor-data array)))
                     (armor-nullify! array)
                     (when (pointer? data)
                       (free data))))))

            ,(maybe-set! <FREE>
               `(lambda (array)
                  (assert (,<PRED> array))
                  (,%free-array! array)
                  array))

            ,(maybe-set! <ALLOC> %alloc-memory)

            ,(maybe-set! <ALLOC/BLOB> %alloc-blob)

            ,(maybe-set! <MAKE>
               `(lambda (length)
                  (,<WRAP> (,%alloc-memory length)
                           length ,@<DEFAULTS>)))

            ,(maybe-set! <MAKE/AUTOFREE>
               `(lambda (length)
                  (set-finalizer!
                   (,<WRAP> (,%alloc-memory length)
                            length ,@<DEFAULTS>)
                   ,%free-array!)))

            ,(maybe-set! <MAKE/BLOB>
               `(lambda (length)
                  (,<WRAP> (,%alloc-blob length)
                           length ,@<DEFAULTS>))))))
     ;; end (construct)

     (let-values (((heads keys _bodies)
                   (extract-args
                    form
                    head: 1
                    keys: '((free:       #f)
                            (alloc:      #f)
                            (alloc/blob: #f)
                            (make:       #f)
                            (make/af:    #f)
                            (make/blob:  #f)
                            (defaults:   ())))))
       (let ((<ARMOR-NAME>    (first  (first heads)))
             (<STRUCT_NAME>   (second (first heads)))
             (<PRED>          (third  (first heads)))
             (<WRAP>          (fourth (first heads)))
             (<FREE>          (alist-ref #:free keys))
             (<ALLOC>         (alist-ref #:alloc keys))
             (<ALLOC/BLOB>    (alist-ref #:alloc/blob keys))
             (<MAKE>          (alist-ref #:make keys))
             (<MAKE/AUTOFREE> (alist-ref #:make/af keys))
             (<MAKE/BLOB>     (alist-ref #:make/blob keys))
             (<DEFAULTS>      (alist-ref #:defaults keys)))
         (construct <ARMOR-NAME>
                    <STRUCT_NAME>
                    <PRED>
                    <WRAP>
                    <FREE>
                    <ALLOC>
                    <ALLOC/BLOB>
                    <MAKE>
                    <MAKE/AUTOFREE>
                    <MAKE/BLOB>
                    <DEFAULTS>
                    (gensym (symbol-append '%alloc- (strip-syntax <ARMOR-NAME>) '-memory))
                    (gensym (symbol-append '%alloc- (strip-syntax <ARMOR-NAME>) '-blob))
                    (gensym (symbol-append '%free- (strip-syntax <ARMOR-NAME>)))))))))


;; (define-array-accessors
;;   (<ARMOR-NAME> <STRUCT_NAME> <PRED> <UNWRAP> <LENGTH>)
;;   (<ITEM-ARMOR-NAME> <ITEM-PRED> <ITEM-WRAP> <ITEM-UNWRAP>)
;;   ref:       <REF>
;;   set:       <SET>
;;   map:       <MAP>
;;   for-each:  <FOR-EACH>
;;   ref*:      <REF*>
;;   map*:      <MAP*>
;;   for-each*: <FOR-EACH*>)
(define-syntax define-array-accessors
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <ARMOR-NAME>
                        <STRUCT_NAME>
                        <PRED>
                        <UNWRAP>
                        <LENGTH>
                        <ITEM-ARMOR-NAME>
                        <ITEM-PRED>
                        <ITEM-WRAP>
                        <ITEM-UNWRAP>
                        <REF>
                        <SET>
                        <MAP>
                        <FOR-EACH>
                        <REF*>
                        <MAP*>
                        <FOR-EACH*>
                        %get-item-ptr
                        %get-item-as-child)
       `(begin
          ;; Procedure names are first declared, then later set! so
          ;; they can all use the helper procedures.

          ,(maybe-declare <REF>
             `((struct ,<ARMOR-NAME>) fixnum
               --> (struct ,<ITEM-ARMOR-NAME>)))

          ;; Don't declare anything if it is a (setter <REF>) form.
          ,(and <SET> (not (list? <SET>))
                `(begin
                   (: ,<SET>
                      ((struct ,<ARMOR-NAME>)
                       fixnum
                       (or (struct ,<ITEM-ARMOR-NAME>)
                           pointer locative blob)
                       -> void))
                   (define ,<SET>)))

          ,(maybe-declare <MAP>
             `(forall (a)
                ((fixnum #!rest (struct ,<ITEM-ARMOR-NAME>) -> a)
                 #!rest (struct ,<ARMOR-NAME>)
                 -> (list-of a))))

          ,(maybe-declare <FOR-EACH>
             `((fixnum #!rest (struct ,<ITEM-ARMOR-NAME>) -> *)
               #!rest (struct ,<ARMOR-NAME>)
               -> void))

          ,(maybe-declare <REF*>
             `((struct ,<ARMOR-NAME>) fixnum
               --> (or pointer locative)))

          ,(maybe-declare <MAP*>
             `(forall (a)
                ((fixnum #!rest (or pointer locative) -> a)
                 #!rest (struct ,<ARMOR-NAME>)
                 -> (list-of a))))

          ,(maybe-declare <FOR-EACH*>
             `((fixnum #!rest (or pointer locative) -> *)
               #!rest (struct ,<ARMOR-NAME>)
               -> void))

          ;; Helpers used by multiple accessors.
          (let* ((,%get-item-ptr
                  ,(%make-get-item-ptr <STRUCT_NAME> <ITEM-ARMOR-NAME>))
                 (,%get-item-as-child
                  (lambda (fn array array* i)
                    (armor-parent-set!
                     (,<ITEM-WRAP> (,%get-item-ptr fn array array* i))
                     array))))

            ,(maybe-set! <REF>
               (%array-ref <REF> <PRED> <LENGTH> %get-item-as-child))

            ,(maybe-set! <SET>
               (%array-set <SET> <PRED> <LENGTH> <STRUCT_NAME>
                           <UNWRAP> <ITEM-UNWRAP>))

            ,(maybe-set! <MAP>
               (%array-map <MAP> <PRED> <LENGTH> %get-item-as-child))

            ,(maybe-set! <FOR-EACH>
               (%array-for-each <FOR-EACH> <PRED> <LENGTH>
                                <ITEM-WRAP> %get-item-ptr))

            ,(maybe-set! <REF*>
               (%array-ref* <REF*> <PRED> <LENGTH> %get-item-ptr))

            ,(maybe-set! <MAP*>
               (%array-map* <MAP*> <PRED> <LENGTH> %get-item-ptr))

            ,(maybe-set! <FOR-EACH*>
               (%array-for-each* <FOR-EACH*> <PRED> <LENGTH>
                                 %get-item-ptr)))))
     ;; end (construct)

     (let-values (((heads keys bodies)
                   (extract-args
                    form
                    head: 2
                    keys: '((ref:       #f)
                            (set:       #f)
                            (map:       #f)
                            (for-each:  #f)
                            (ref*:      #f)
                            (map*:      #f)
                            (for-each*: #f)))))
       (let ((<ARMOR-NAME>      (first  (first heads)))
             (<STRUCT_NAME>     (second (first heads)))
             (<PRED>            (third  (first heads)))
             (<UNWRAP>          (fourth (first heads)))
             (<LENGTH>          (fifth  (first heads)))
             (<ITEM-ARMOR-NAME> (first  (second heads)))
             (<ITEM-PRED>       (second (second heads)))
             (<ITEM-WRAP>       (third  (second heads)))
             (<ITEM-UNWRAP>     (fourth (second heads)))
             (<REF>             (alist-ref #:ref keys))
             (<SET>             (alist-ref #:set keys))
             (<MAP>             (alist-ref #:map keys))
             (<FOR-EACH>        (alist-ref #:for-each keys))
             (<REF*>            (alist-ref #:ref* keys))
             (<MAP*>            (alist-ref #:map* keys))
             (<FOR-EACH*>       (alist-ref #:for-each* keys)))
         (construct <ARMOR-NAME>
                    <STRUCT_NAME>
                    <PRED>
                    <UNWRAP>
                    <LENGTH>
                    <ITEM-ARMOR-NAME>
                    <ITEM-PRED>
                    <ITEM-WRAP>
                    <ITEM-UNWRAP>
                    <REF>
                    <SET>
                    <MAP>
                    <FOR-EACH>
                    <REF*>
                    <MAP*>
                    <FOR-EACH*>
                    (symbol-append '%get- (strip-syntax <ARMOR-NAME>)
                                   '-item-ptr)
                    (symbol-append '%get- (strip-syntax <ARMOR-NAME>)
                                   '-item-as-child)))))))


(begin-for-syntax
 (define (%make-get-item-ptr <STRUCT_NAME> <ITEM-ARMOR-NAME>)
   `(lambda (fn array array-data index)
      (cond
       ((pointer? array-data)
        (tag-pointer
         ((foreign-lambda*
           (nonnull-c-pointer ,<STRUCT_NAME>)
           (((nonnull-c-pointer ,<STRUCT_NAME>) a)
            (size_t i))
           "C_return( &a[i] );")
          array-data index)
         ',<ITEM-ARMOR-NAME>))
       ((locative? array-data)
        (let ((obj (locative->object array-data))
              (offset (* index (foreign-type-size ,<STRUCT_NAME>))))
          (if obj
              (make-locative obj offset)
              (error fn
                     "array locative object has been garbage collected"
                     array))))
       ((blob? array-data)
        (make-locative
         array-data
         (* index (foreign-type-size ,<STRUCT_NAME>))))
       (else
        (error fn "invalid array data" array-data)))))

 (define (%array-ref <REF> <PRED> <LENGTH> %get-item-as-child)
   `(lambda (array i)
      (assert (,<PRED> array))
      (assert (not (armor-null? array)))

      (let ((len (,<LENGTH> array))
            (array-data (%jiffi:armor-data array)))
        (unless (< -1 i len)
          (error ',<REF> "index out of bounds for array"
                 i array (list 'length len)))
        (,%get-item-as-child ',<REF> array array-data i))))

 (define (%array-set <SET> <PRED> <LENGTH> <STRUCT_NAME>
                     <UNWRAP> <ITEM-UNWRAP>)
   `(lambda (array i struct)
      (define set-item-data
        (foreign-lambda*
         void
         (((nonnull-c-pointer ,<STRUCT_NAME>) array)
          (size_t i)
          ((nonnull-c-pointer ,<STRUCT_NAME>) st))
         "array[i] = *st;"))

      (assert (,<PRED> array))
      (assert (not (armor-null? array)))
      (assert (not (armor-null? struct)))

      (let ((len (,<LENGTH> array)))
        (unless (< -1 i len)
          (error ',<SET> "index out of bounds for array"
                 i array (list 'length len))))

      (set-item-data (,<UNWRAP> array) i
                     (,<ITEM-UNWRAP> struct))))

 (define (%array-map <MAP> <PRED> <LENGTH> %get-item-as-child)
   `(lambda (proc . arrays)
      (for-each
       (lambda (array)
         (assert (,<PRED> array)
                 "invalid array" array)
         (assert (not (armor-null? array))))
       arrays)

      (let ((array-datas
             (map %jiffi:armor-data arrays))
            (min-length
             (apply min (map ,<LENGTH> arrays))))
        (let loop ((i (- min-length 1))
                   (accum '()))
          (if (< i 0)
              accum
              (loop (sub1 i)
                    (cons
                     (apply proc i
                            (map (cut ,%get-item-as-child
                                      ',<MAP> <> <> i)
                                 arrays array-datas))
                     accum)))))))

 (define (%array-for-each <FOR-EACH> <PRED> <LENGTH> <ITEM-WRAP> %get-item-ptr)
   `(lambda (proc . arrays)
      (define (make-item array)
        (let ((item (,<ITEM-WRAP> #f)))
          (set! (%jiffi:armor-parent item) array)
          ;; item is not registered as a child of array because item
          ;; will be nullified after iteration finishes anyway.
          item))

      ;; Modify item to hold a pointer to the new index. The item
      ;; record instance is re-used for efficiency.
      (define (ref-replace! array array-data index item)
        (set! (%jiffi:armor-data item)
              (,%get-item-ptr ',<FOR-EACH> array
                              array-data index)))

      (for-each (lambda (array)
                  (assert (,<PRED> array)
                          "invalid array" array)
                  (assert (not (armor-null? array))))
                arrays)

      (let ((items
             (map make-item arrays))
            (array-datas
             (map %jiffi:armor-data arrays))
            (min-length
             (apply min (map ,<LENGTH> arrays))))
        (do ((i 0 (add1 i)))
            ((= i min-length))
          (for-each (cut ref-replace! <> <> i <>)
                    arrays array-datas items)
          (apply proc i items))

        (for-each armor-nullify! items))))

 (define (%array-ref* <REF*> <PRED> <LENGTH> %get-item-ptr)
   `(lambda (array i)
      (assert (,<PRED> array))
      (assert (not (armor-null? array)))
      (let ((len (,<LENGTH> array))
            (array-data (%jiffi:armor-data array)))
        (unless (< -1 i len)
          (error ',<REF*> "index out of bounds for array"
                 i array (list 'length len)))
        (,%get-item-ptr ',<REF*> array array-data i))))

 (define (%array-map* <MAP*> <PRED> <LENGTH> %get-item-ptr)
   `(lambda (proc . arrays)
      (for-each
       (lambda (array)
         (assert (,<PRED> array)
                 "invalid array" array)
         (assert (not (armor-null? array))))
       arrays)

      (let ((array-datas
             (map %jiffi:armor-data arrays))
            (min-length
             (apply min (map ,<LENGTH> arrays))))
        (let loop ((i (- min-length 1))
                   (accum '()))
          (if (< i 0)
              accum
              (loop (sub1 i)
                    (cons (apply proc i
                                 (map (cut ,%get-item-ptr ',<MAP*> <> <> i)
                                      arrays array-datas))
                          accum)))))))

 (define (%array-for-each* <FOR-EACH*> <PRED> <LENGTH> %get-item-ptr)
   `(lambda (proc . arrays)
      (for-each
       (lambda (array)
         (assert (,<PRED> array)
                 "invalid array" array)
         (assert (not (armor-null? array))))
       arrays)

      (let ((array-datas
             (map %jiffi:armor-data arrays))
            (min-length
             (apply min (map ,<LENGTH> arrays))))
        (do ((i 0 (add1 i)))
            ((= i min-length))
          (apply proc i
                 (map (cut ,%get-item-ptr ',<FOR-EACH*> <> <> i)
                      arrays
                      array-datas))))))
 )


;;; (define-array-copy! COPY!
;;;   (ARMOR-NAME "STRUCT_NAME" PRED UNWRAP LENGTH))
;;;
(define-syntax define-array-copy!
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <COPY!>
                        <ARMOR-NAME>
                        <STRUCT_NAME>
                        <PRED>
                        <UNWRAP>
                        <LENGTH>)
       `(begin
          (: ,<COPY!>
             ((struct ,<ARMOR-NAME>) fixnum
              (struct ,<ARMOR-NAME>) #!optional fixnum fixnum
              -> void))
          (define (,<COPY!> to at from #!optional
                            (start 0)
                            (end (,<LENGTH> from)))
            (define-binding** foreign-copy!
              args: (((c-pointer ,<STRUCT_NAME>) to)
                     (size_t at)
                     ((c-pointer ,<STRUCT_NAME>) from)
                     (size_t start)
                     (size_t end))
              ;; Copy in forward or backward direction, to handle possible
              ;; overlapping regions of the same array.
              ;; TODO: Optimize with memcpy if arrays are different or
              ;; regions do not overlap.
              "if (at <= start) {"
              "  size_t i_to = at;"
              "  size_t i_from = start;"
              "  for (; i_from < end; i_to++, i_from++) {"
              "    to[i_to] = from[i_from];"
              "  }"
              "} else {"
              "  size_t i_to = at + (end - start) - 1;"
              "  size_t i_from = end - 1;"
              "  for (; i_to >= at; i_to--, i_from--) {"
              "    to[i_to] = from[i_from];"
              "    "
              "  }"
              "}")
            (assert (not (armor-null? to)))
            (assert (not (armor-null? from)))
            (assert (<= 0 at))
            (assert (<= 0 start))
            (when (armor? from)
              (assert (<= end (,<LENGTH> from))))
            (assert (<= start end))
            (when (armor? to)
              (assert (<= (- end start) (- (,<LENGTH> to) at))
                      (sprintf "destination array too short to receive ~A items at ~A"
                               (- end start) at)
                      to))
            (foreign-copy! (,<UNWRAP> to ,<COPY!>) at
                           (,<UNWRAP> from ,<COPY!>) start end))))

     (let-values (((heads keys bodies)
                   (extract-args
                    form
                    head: 2)))
       (let ((<COPY!>       (first heads))
             (<ARMOR-NAME>  (first  (second heads)))
             (<STRUCT_NAME> (second (second heads)))
             (<PRED>        (third  (second heads)))
             (<UNWRAP>      (fourth (second heads)))
             (<LENGTH>      (fifth  (second heads))))
         (construct <COPY!>
                    <ARMOR-NAME>
                    <STRUCT_NAME>
                    <PRED>
                    <UNWRAP>
                    <LENGTH>))))))
