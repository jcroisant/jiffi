;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export define-binding
        define-binding**
        foreign-lambda**
        foreign-safe-lambda**
        define-callback)


;;; (define-binding (SCHEME-NAME FUNC_NAME) ; or SCHEME-NAME
;;;   safe:   SAFE
;;;   return: RETURN-TYPE
;;;   args:   ((ARG-TYPE ARG_NAME) ...))
;;;
(define-syntax define-binding
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <SCHEME-NAME>
                        <FUNC_NAME>
                        <SAFE>
                        <RETURN-TYPE>
                        <ARG-TYPES>)
       `(define ,<SCHEME-NAME>
          (,(if <SAFE> 'foreign-safe-lambda 'foreign-lambda)
           ,<RETURN-TYPE>
           ,<FUNC_NAME>
           ,@<ARG-TYPES>)))

     (let-values (((heads keys _bodies)
                   (extract-args
                    form
                    head: 1
                    keys: '((safe: #f)
                            (return: void)
                            (args: ())))))
       (let* ((<NAME>        (first heads))
              (<SAFE>        (alist-ref #:safe keys))
              (<RETURN-TYPE> (alist-ref #:return keys))
              (<ARGS>        (alist-ref #:args keys))
              (<SCHEME-NAME> (if (list? <NAME>)
                                 (first <NAME>)
                                 <NAME>))
              (<FUNC_NAME>   (if (list? <NAME>)
                                 (second <NAME>)
                                 <NAME>)))
         (construct <SCHEME-NAME>
                    <FUNC_NAME>
                    <SAFE>
                    <RETURN-TYPE>
                    (map car <ARGS>)))))))


;;; (define-binding** SCHEME-NAME
;;;   safe:   SAFE
;;;   return: RETURN-TYPE
;;;   args:   ((ARG-TYPE ARG_NAME) ...)
;;;   BODY-CLAUSE ...)
;;;
(define-syntax define-binding**
  (ir-macro-transformer
   (lambda (form inject compare)
     (define (construct <SCHEME-NAME>
                        <SAFE>
                        <RETURN-TYPE>
                        <ARGS>
                        <BODY-CLAUSES>)
       `(define ,<SCHEME-NAME>
          (,(if <SAFE> 'foreign-safe-lambda** 'foreign-lambda**)
           ,<RETURN-TYPE>
           ,<ARGS>
           ,@<BODY-CLAUSES>)))

     (let-values (((heads keys bodies)
                   (extract-args
                    form
                    head: 1
                    keys: '((safe: #f)
                            (return: void)
                            (args: ()))
                    body: #t)))
       (let ((<SCHEME-NAME> (first heads))
             (<SAFE>        (alist-ref #:safe keys))
             (<RETURN-TYPE> (alist-ref #:return keys))
             (<ARGS>        (alist-ref #:args keys)))
         (construct <SCHEME-NAME>
                    <SAFE>
                    <RETURN-TYPE>
                    <ARGS>
                    bodies))))))


(define-syntax foreign-lambda**
  (ir-macro-transformer
   (lambda (form inject compare?)
     (let ((RETURN-TYPE  (list-ref form 1))
           (ARGS         (list-ref form 2))
           (BODY-CLAUSES (cdddr form)))
       `(foreign-lambda*
         ,RETURN-TYPE
         ,ARGS
         ,@(map (lambda (BODY-CLAUSE)
                  (string-append
                   (if (list? BODY-CLAUSE)
                       (eval (cons (inject 'sprintf) BODY-CLAUSE))
                       BODY-CLAUSE)
                   ;; In CHICKEN 5 each body string becomes a separate
                   ;; line, but in CHICKEN 4 they don't. Add a newline
                   ;; for consistent behavior in CHICKEN 4.
                   (cond-expand
                    (chicken-4 "\n")
                    (else ""))))
                BODY-CLAUSES))))))


(define-syntax foreign-safe-lambda**
  (ir-macro-transformer
   (lambda (form inject compare?)
     (let ((RETURN-TYPE  (list-ref form 1))
           (ARGS         (list-ref form 2))
           (BODY-CLAUSES (cdddr form)))
       `(foreign-safe-lambda*
         ,RETURN-TYPE
         ,ARGS
         ,@(map (lambda (BODY-CLAUSE)
                  (string-append
                   (if (list? BODY-CLAUSE)
                       (eval (cons (inject 'sprintf) BODY-CLAUSE))
                       BODY-CLAUSE)
                   ;; In CHICKEN 5 each body string becomes a separate
                   ;; line, but in CHICKEN 4 they don't. Add a newline
                   ;; for consistent behavior in CHICKEN 4.
                   (cond-expand
                    (chicken-4 "\n")
                    (else ""))))
                BODY-CLAUSES))))))


;;; (define-callback (<SCHEME-NAME> <C_NAME>) ;; or <SCHEME-NAME>
;;;   quals:  "QUALIFIER ..."
;;;   return: <RETURN-TYPE>
;;;   args:   ((<ARG-TYPE> <ARG_NAME>) ...)
;;;   <BODY> ...)
;;;
(define-syntax define-callback
  (er-macro-transformer
   (lambda (form rename compare)
     (define (construct
              <SCHEME-NAME>
              <C_NAME>
              <QUALIFIERS>
              <RETURN-TYPE>
              <ARGS>
              <BODY...>)
       `(,(rename 'begin)
         (,(rename 'define-external)
          ,<QUALIFIERS>
          (,<C_NAME> ,@<ARGS>)
          ,<RETURN-TYPE>
          ,@<BODY...>)
         (,(rename 'define) ,<SCHEME-NAME>
          (location ,<C_NAME>))))

     (define (gen-c-identifier name)
       (string->symbol
        (let ((name (if (symbol? name)
                        (symbol->string name)
                        name)))
          (string-append
           "callback"
           (number->string
            (+ 100000000
               (pseudo-random-integer 900000000)))
           "__"
           (irregex-replace/all "[^0-9A-Za-z]" name "_")))))

     (let-values (((heads keys bodies)
                   (extract-args
                    form
                    head: 1
                    keys: '((quals: "")
                            (return: void)
                            (args: ()))
                    body: #t)))
       (let ((<NAME>        (first heads))
             (<QUALIFIERS>  (alist-ref #:quals keys))
             (<RETURN-TYPE> (alist-ref #:return keys))
             (<ARGS>        (alist-ref #:args keys)))
         (let ((<SCHEME-NAME>
                (if (list? <NAME>)
                    (first <NAME>)
                    <NAME>))
               (<C_NAME>
                (if (list? <NAME>)
                    (second <NAME>)
                    (gen-c-identifier <NAME>))))
           (construct <SCHEME-NAME>
                      <C_NAME>
                      <QUALIFIERS>
                      <RETURN-TYPE>
                      <ARGS>
                      bodies)))))))
