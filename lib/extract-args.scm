;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;; extract-args accepts a quoted macro form that can have a fixed
;; number of "head" arguments, maybe followed by some keyword
;; arguments, and maybe followed by any number of "body" arguments.
;;
;; Returns three values: a list of the head arguments, a list of
;; (KEYWORD . VALUE) pairs with the default value for any missing
;; keywords, and a list of the body arguments.
;;
;; Signals an error if:
;;
;;   - There are too few head arguments
;;   - A keyword argument is missing and its default is 'required!
;;   - A body argument is found when not allowed
;;
;; Prints a warning if:
;;
;;   - An unrecognized keyword is found
;;   - A duplicate keyword is found
;;
;; Example:
;;
;;   (extract-args
;;    '(my-macro
;;      "head1"
;;      "head2"
;;      key3: "value3"
;;      key1: "value1"
;;      "body1"
;;      "body2")
;;    head: 2
;;    keys: '((key1: required!)
;;            (key2: "default2")
;;            (key3: "default3"))
;;    body: #t)
;;
;; Returns three values:
;;
;;   ("head1" "head2")
;;
;;   ((key1: . "value1")
;;    (key2: . "default2")
;;    (key3: . "value3"))
;;
;;   ("body1" "body2")
;;
(define (extract-args form #!key
                      ;; Number of required head arguments
                      (head 0)
                      ;; Canonical list of (KEYWORD: DEFAULT) lists
                      (keys '())
                      ;; Are body arguments allowed?
                      (body #f))

  ;; Returns an list of (KEYWORD . VALUE) pairs in same order as
  ;; `keys`, with defaults for any missing keywords. Signals a syntax
  ;; error if a missing keyword's default is 'required! .
  (define (normalize-keywords ks)
    (map (lambda (key-default)
           (let ((key (car key-default))
                 (default (cadr key-default))
                 (found (assq (car key-default) ks)))
             (cond (found
                    (cons key (cadr found)))
                   ((eq? 'required! (strip-syntax default))
                    (syntax-error
                     (sprintf "missing required keyword `~A' in form"
                              key)
                     (strip-syntax form)))
                   (else
                    (cons key default)))))
         keys))

  (let continue
      ((state 'heads)
       ;; remaining arguments to scan
       (args (cdr form))
       ;; accumulated list of head args found so far, in reverse
       (hs '())
       ;; alist of (KEYWORD . VALUE) pairs found so far.
       (ks '())
       ;; accumulated list of body args found so far, in reverse
       (bs '()))

    (case state
      ((heads)
       (cond
        ((= head (length hs))
         (continue 'keywords args hs ks bs))

        ((null? args)
         (syntax-error "too few arguments in form"
                       (strip-syntax form)))

        ;; Found keyword but haven't found enough head args yet.
        ((keyword? (car args))
         (syntax-error "too few arguments before keywords in form"
                       (strip-syntax form)))

        ;; Push non-keyword argument onto hs.
        (else
         (continue 'heads (cdr args)
                   (cons (car args) hs)
                   ks
                   bs))))

      ((keywords)
       (cond
        ((null? args)
         (continue 'done args hs ks bs))

        ((not (keyword? (car args)))
         (continue 'bodies args hs ks bs))

        ((< (length args) 2)
         (syntax-error
          (sprintf "missing value for keyword ~A in form" (car args))
          (strip-syntax form)))

        (else
         ;; Print warning about duplicate or unrecognized keywords,
         ;; but still capture them. This means later duplicate
         ;; keywords clauses override earlier ones.
         (cond
          ((assq (car args) ks)
           (warning
            (sprintf "duplicate keyword `~A' in form" (car args))
            (strip-syntax form)))
          ((not (assq (car args) keys))
           (warning
            (sprintf "unrecognized keyword `~A' in form" (car args))
            (strip-syntax form))))

         ;; Push (KEYWORD . VALUE) pair to ks and continue.
         (continue state (cddr args)
                   hs
                   (cons (list (car args) (cadr args))
                         ks)
                   bs))))

      ((bodies)
       (cond
        ((null? args)
         (continue 'done args hs ks bs))

        ;; There are more args, but body args are not allowed.
        ((not body)
         (syntax-error "too many arguments in form" form))

        ;; Push argument onto bs.
        (else
         (continue state (cdr args)
                   hs
                   ks
                   (cons (car args) bs)))))

      ((done)
       (values
        (reverse hs)
        (normalize-keywords ks)
        (reverse bs)))

      (else
       (error 'extract-args "invalid state" state)))))
