;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export gc-root-ref
        gc-root-set!
        call-with-gc-root
        make-gc-root
        gc-root-delete!)


(define-binding (gc-root-ref CHICKEN_gc_root_ref)
  return: scheme-object
  args: ((nonnull-c-pointer root)))

(define-binding (gc-root-set! CHICKEN_gc_root_set)
  args: ((nonnull-c-pointer root)
         (scheme-object value)))

(set! (setter gc-root-ref) gc-root-set!)


(define (call-with-gc-root value proc)
  (let* ((root (make-gc-root value))
         (result (proc root)))
    (gc-root-delete! root)
    result))


(define-binding** make-gc-root
  return: c-pointer
  args: ((scheme-object value))
  "void *root = CHICKEN_new_gc_root();"
  "CHICKEN_gc_root_set(root, value);"
  "C_return(root);")


(define-binding (gc-root-delete! CHICKEN_delete_gc_root)
  args: ((nonnull-c-pointer root)))
