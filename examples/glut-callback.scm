;;; Demonstrates various techniques to define a callback, using the
;;; freeglut library as an example. See docs/callbacks.md.
;;;
;;; Compile with:
;;;   csc -L -lglut glut-callback.scm

(cond-expand
 (chicken-4
  (import foreign)
  (use jiffi))
 (else
  (import (chicken foreign)
          (only (chicken process-context) argc+argv)
          jiffi)))

(foreign-declare "#include <GL/glut.h>")


(define-binding** glutInit
  args: ((int argc)
         (c-pointer argv))
  "glutInit(&argc, (char**)&argv);")

(define-binding glutCreateWindow
  return: int
  args: ((c-string name)))

(define-binding glutMouseFunc
  args: (((function void (int int int int)) func)))


;;; Technique 1: Defining callback function in C

(foreign-declare "
  void my_mouse_func(int button, int state, int x, int y) {
    printf(\"[func] Mouse button %d state %d at position %d, %d\\n\",
           button, state, x, y);
  }
")

(define my_mouse_func
  (foreign-value "my_mouse_func" c-pointer))


;;; Technique 2: Using define-callback

(define-callback my-mouse-callback
  args: ((int button)
         (int state)
         (int x)
         (int y))
  (printf "[callback] Mouse button ~A state ~A at position ~A, ~A~%"
          button state x y))


;;; Technique 3: Using an adapter callback

(define *mouse-callback* (make-parameter #f))

(define-callback mouse-func-adapter
  args: ((int button)
         (int state)
         (int x)
         (int y))
  (let ((proc (*mouse-callback*)))
    (when proc
      (proc button state x y))))

(define (my-mouse-procedure button state x y)
  (printf "[procedure] Mouse button ~A state ~A at position ~A, ~A~%"
          button state x y))

(*mouse-callback* my-mouse-procedure)


;;; Technique 4: Flexible approach

;; callback can be a procedure, function pointer, or #f
(define (mouse-func-set! callback)
  (cond
   ;; For a procedure, set the global variable and pass the adapter as
   ;; the callback function.
   ((procedure? callback)
    (*mouse-callback* callback)
    (glutMouseFunc mouse-func-adapter))

   ;; For a C function pointer, including from define-callback, clear
   ;; the global variable and pass the pointer as the callback.
   ((pointer? callback)
    (*mouse-callback* #f)
    (glutMouseFunc callback))

   ;; glutMouseFunc also allows the callback to be NULL (#f) to
   ;; disable mouse callbacks.
   ((not callback)
    (*mouse-callback* #f)
    (glutMouseFunc callback))

   (else
    (error "Invalid callback" callback))))


(call-with-values argc+argv glutInit)
(glutCreateWindow "Press mouse button")

(glutMouseFunc my_mouse_func)
;; (glutMouseFunc my-mouse-callback)
;; (glutMouseFunc mouse-func-adapter)
;; (mouse-func-set! my_mouse_func)
;; (mouse-func-set! my-mouse-callback)
;; (mouse-func-set! my-mouse-procedure)
;; (mouse-func-set! #f)

(glutMainLoop)
