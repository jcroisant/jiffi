# Jiffi

High-level helpers for writing [CHICKEN Scheme](https://call-cc.org/)
foreign function interface (FFI) bindings to C libraries.

- Version:     0.0.0 (not yet published)
- Project:     https://gitlab.com/jcroisant/jiffi
- Issues:      https://gitlab.com/jcroisant/jiffi/issues
- Docs:        http://wiki.call-cc.org/eggref/5/jiffi
- License:     [BSD](LICENSE.txt)
- Maintainer:  John Croisant (john (at) croisant (dot) net)


## About

Jiffi is designed specifically for writing FFI bindings by hand,
rather than automatic generation. Its design goals are:

1. **Usability:** Bindings should be easy to use. They should follow
   Scheme idioms, and not require the user to understand low-level
   details about C memory management, pointers, etc.
2. **Safety:** Bindings should, by default, protect users against type
   errors and memory errors at the C level. Writing unsafe code should
   require a deliberate choice.
3. **Maintainability:** The binding code itself should be easy for its
   authors to understand, extend, and debug. It should be clear,
   explicit, predictable, and greppable.
4. **Flexibility:** Jiffi should be flexible enough able to handle the
   most common 80% of FFI use cases, and give binding authors a high
   degree of control over their API style.
5. **Performance:** Bindings should be as efficient as possible
   without sacrificing the other goals.

Jiffi is especially well-suited for writing FFI bindings for C
libraries that are so large that it is difficult for a human to
maintain bindings using CHICKEN's
[FFI primitives](https://wiki.call-cc.org/man/5/Module%20(chicken%20foreign)),
and so syntactically complex that the
[`bind` egg](https://wiki.call-cc.org/egg/bind)
cannot parse it and generate bindings automatically. Jiffi evolved
from the FFI helpers that were used to write the
[`sdl2` egg](https://wiki.call-cc.org/egg/sdl2).

Jiffi is also good for beginners who are not familiar with the
intricacies of CHICKEN's FFI features, or who are not very comfortable
with C concepts like pointers and memory management, which can cause
crashes and security vulnerabilities if not done correctly. Jiffi
helps you create easy-to-use, safe, and efficient FFI bindings, even
if you are not a C expert.

All of Jiffi's features are built using the FFI primitives provided by
CHICKEN, so they are cross-compatible. The documentation for each
macro describes which primitives it uses, so you can understand what
is happening behind the scenes. In most cases you can use one feature
of Jiffi without using the other features, although some are closely
coupled out of necessity.


## Installation

Jiffi has not yet been published, so you must clone or download the
git repository, then run `chicken-install` within the directory.

Jiffi requires CHICKEN 4.13 or higher.
It is compatible with both CHICKEN 4 and CHICKEN 5.

If you define any
[thread-safe armors](http://wiki.call-cc.org/eggref/5/jiffi#armor-thread-safety),
you should also add the
[`srfi-18` egg](https://wiki.call-cc.org/eggref/5/srfi-18)
to your dependencies. (This is not necessary for CHICKEN 4, which has
SRFI-18 built in.)


## Getting Started

If you are new to Jiffi, the best place to start is the
[Getting Started with Jiffi](docs/getting-started.md) guide.
After gaining a basic understanding, visit the
[API reference documentation](http://wiki.call-cc.org/eggref/5/jiffi)
to look up details as needed.


## Code of Conduct

We are committed to making participation in this project a welcoming
and harassment-free experience. All project participants are expected
to follow the [Code of Conduct](code_of_conduct.md).
