(test-group "gc-root-ref / gc-root-set!"
  (let* ((value1 (lambda () 1))
         (value2 (lambda () 2))
         (root (make-gc-root value1)))
    (test (void) (gc-root-set! root value1))
    (test value1 (gc-root-ref root))
    (test (void) (set! (gc-root-ref root) value2))
    (test value2 (gc-root-ref root))))


(test-group "call-with-gc-root"
  (test "Passes GC root to proc and returns result"
        'ok!
        (let ((foo (lambda () 42)))
          (call-with-gc-root foo
            (lambda (foo-root)
              (assert (eq? foo (gc-root-ref foo-root)))
              'ok!)))))


(test-group "make-gc-root"
  (test-assert "Returns a GC root pointer"
    (let ((a (list 1 2 3)))
      (pointer? (make-gc-root a))))

  (test-assert "The GC root points to the correct object"
    (let ((b (lambda () 42)))
      (eq? b (gc-root-ref (make-gc-root b))))))


(test-group "gc-root-delete!"
  (define foo (lambda () 42))

  ;; Don't know how to test that it actually deleted the GC root
  (test "Can be called with a GC root pointer"
        (void)
        (gc-root-delete! (make-gc-root foo))))
