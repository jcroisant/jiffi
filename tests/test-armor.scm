
(test-group "armor?"
  (test "Returns #t for any struct/union armor type"
        '(#t #t)
        (list (armor? (make-event/blob))
              (armor? (make-simplum/autofree))))

  (test "Returns #t for any array type"
        '(#t #t)
        (list (armor? (make-event-array/blob 3))
              (armor? (make-simplum-array/autofree 3))))

  (test "Returns #f for non-jiffi record instances"
        #f
        (armor? (make-record-instance 'foo 1 2)))

  (test "Returns #f for pointers, etc."
        '(#f #f #f #f)
        (list (armor? (address->pointer 123))
              (armor? (make-locative (make-blob 16)))
              (armor? (make-blob 16))
              (armor? #f))))


(test-group "armor-data / armor-data-set!"
  (define ptr (address->pointer 123))
  (define blob #${1234567890})
  (define loc (make-locative blob))
  (define false-armor (wrap-event #f))
  (define ptr-armor (wrap-event ptr))
  (define blob-armor (wrap-event blob))
  (define loc-armor (wrap-event loc))

  (test-assert (not (armor-data false-armor)))
  (test-assert (eq? ptr (armor-data ptr-armor)))
  (test-assert (eq? blob (armor-data blob-armor)))
  (test-assert (eq? loc (armor-data loc-armor)))

  (test (void) (armor-data-set! false-armor blob))
  (test-assert (eq? blob (armor-data false-armor)))

  (test (void) (set! (armor-data blob-armor) ptr))
  (test-assert (eq? ptr (armor-data blob-armor))))



(test-group "armor-parent"
  (test "Returns #f if armor has no parent"
        #f
        (armor-parent (make-event/blob)))

  (let ((array (make-event-array/autofree 5)))
    (test "Returns the armor's parent if it has one"
          array
          (armor-parent (event-array-ref array 0))))

  (test-error "Error if argument is not an armor"
    (armor-parent (unwrap-event (make-event/autofree)))))


(test-group "armor-parent-set!"
  (let* ((array (make-event-array/autofree 5))
         (child (wrap-event (event-array-ref* array 1))))
    (test "Modifies armor to set its parent"
          array
          (begin
            (armor-parent-set! child array)
            (armor-parent child))))

  (let* ((array (make-event-array/autofree 5))
         (child (wrap-event (event-array-ref* array 1))))
    (test "Modifies parent to register child"
          #t
          (begin
            (armor-parent-set! child array)
            (armor-has-child? array child))))

  (let* ((array (make-event-array/autofree 5))
         (child (wrap-event (event-array-ref* array 1))))
    (test "Returns the child"
          child
          (armor-parent-set! child array)))

  (let* ((array (make-event-array/autofree 5))
         (child (wrap-event (event-array-ref* array 1))))
    (test-error "Error if child is not an armor"
      (armor-parent-set!
       (event-array-ref* array 1)
       array)))

  (let* ((array (make-event-array/autofree 5))
         (child (wrap-event (event-array-ref* array 1))))
    (test-error "Error if parent is not an array"
      (armor-parent-set!
       (wrap-event (event-array-ref* array 1))
       (unwrap-event-array array)))))


(test-group "armor-tracks-children?"
  (test "Returns #t if armor tracks its children"
        #t
        (armor-tracks-children? (make-event-array/blob 10)))

  (test "Returns #f if armor does not track its children"
        #f
        (armor-tracks-children? (make-bad-array/blob 10)))

  (test "Can be set! to #t"
        #t
        (let ((array (make-bad-array/blob 10)))
          (assert (not (armor-tracks-children? array)))
          (set! (armor-tracks-children? array) #t)
          (armor-tracks-children? array)))

  (test "Can be set! to #f"
        #f
        (let ((array (make-event-array/blob 10)))
          (assert (armor-tracks-children? array))
          (set! (armor-tracks-children? array) #f)
          (armor-tracks-children? array)))

  (test "Armors really do track children if #t"
        10
        (let* ((array (make-event-array/blob 10))
               (kids (event-array-map list array)))
          (armor-kids-count array)))

  (test "Armors really do not track children if #f"
        #f
        (let* ((array (make-bad-array/blob 10))
               (kids (bad-array-map list array)))
          (with-armor-kids array identity))))


(test-group "armor-tracks-children-set!"
  (test "Can change from #t to #f"
        #f
        (let ((array (make-event-array/blob 10)))
          (assert (armor-tracks-children? array))
          (set! (armor-tracks-children? array) #f)
          (armor-tracks-children? array)))

  (test "Can change from #f to #t"
        #t
        (let ((array (make-bad-array/blob 10)))
          (assert (not (armor-tracks-children? array)))
          (armor-tracks-children-set! array #t)
          (armor-tracks-children? array)))

  (test "Can set to #f when already #f"
        #f
        (let ((array (make-bad-array/blob 10)))
          (assert (not (armor-tracks-children? array)))
          (armor-tracks-children-set! array #f)
          (armor-tracks-children? array)))

  (test "Setting to #t when already #t does not forget children"
        10
        (let* ((array (make-event-array/blob 10))
               (kids (event-array-map list array)))
          (armor-tracks-children-set! array #t)
          (armor-kids-count array)))

  (test "Disabling forgets children even if re-enabled later"
        0
        (let* ((array (make-event-array/blob 10))
               (kids (event-array-map list array)))
          (armor-tracks-children-set! array #f)
          (armor-tracks-children-set! array #t)
          (armor-kids-count array)))

  (test "Enabling tracking actually works even if default is #f"
        10
        (let ((array (make-bad-array/blob 10)))
          (armor-tracks-children-set! array #t)
          (let ((kids (bad-array-map list array)))
            (armor-kids-count array))))

  (test "Disabling tracking actually works even if default is #t"
        #f
        (let ((array (make-event-array/blob 10)))
          (armor-tracks-children-set! array #f)
          (let ((kids (event-array-map list array)))
            (with-armor-kids array identity)))))


(test-group "armor-address"
  (test "Returns 0 if armor wraps #f"
        0
        (armor-address (wrap-simplum #f)))

  (test "Returns 0 if given null pointer"
        0
        (armor-address (wrap-simplum (address->pointer 0))))

  (test "Returns address if given non-null pointer"
        1234
        (armor-address (wrap-simplum (address->pointer 1234))))

  (let* ((blob (object-evict (make-blob 8)))
         (locative (make-locative blob))
         (struct (wrap-simplum locative)))
    (test "Returns address if given locative"
          (pointer->address locative)
          (armor-address struct)))

  (let* ((blob (object-evict (make-blob 8)))
         (locative (make-locative blob))
         (struct (wrap-simplum blob)))
    (test "Returns address if given blob"
          (pointer->address locative)
          (armor-address struct)))

  (test "Returns 0 if given #f"
        0
        (armor-address #f))

  (test "Returns 0 if given null pointer"
        0
        (armor-address (address->pointer 0)))

  (test "Returns address if given non-null pointer"
        1234
        (armor-address (address->pointer 1234)))

  (let* ((blob (make-blob 8))
         (locative (make-locative blob)))
    (test "Returns address if given locative"
          (pointer->address locative)
          (armor-address locative))

    (test "Returns address if given blob"
          (pointer->address locative)
          (armor-address blob))))


(test-group "armor-eq?"
  (define mem (allocate 32))
  (define blob (object-evict (make-blob 32)))

  (define nulls
    `(("#f" . ,(lambda () #f))
      ("null pointer" . ,(lambda () (address->pointer 0)))
      ("null simplum struct" . ,(lambda () (wrap-simplum (address->pointer 0))))
      ("null event struct" . ,(lambda () (wrap-event (address->pointer 0) 1 2)))
      ("null simplum array" . ,(lambda () (wrap-simplum-array (address->pointer 0) 0)))
      ("null event array" . ,(lambda () (wrap-event-array (address->pointer 0) 1 'a 'b)))))

  (define a-addrs
    `(("pointer A" . ,(lambda () mem))
      ("pointer simplum struct A" . ,(lambda () (wrap-simplum mem)))
      ("pointer event struct A" . ,(lambda () (wrap-event mem 1 2)))
      ("pointer simplum array A" . ,(lambda () (wrap-simplum-array mem 1)))
      ("pointer event array A" . ,(lambda () (wrap-event-array mem 1 'a 'b)))))

  (define b-addrs
    `(("blob B" . ,(lambda () blob))
      ("blob locative B" . ,(lambda () (make-locative blob)))
      ("pointer B" . ,(lambda () (address->pointer (pointer->address (make-locative blob)))))
      ("blob simplum struct B" . ,(lambda () (wrap-simplum blob)))
      ("blob event struct B" . ,(lambda () (wrap-event blob 1 2)))
      ("locative simplum struct B" . ,(lambda () (wrap-simplum (make-locative blob))))
      ("locative event struct B" . ,(lambda () (wrap-event (make-locative blob) 1 2)))
      ("pointer simplum struct B" . ,(lambda () (wrap-simplum (address->pointer (pointer->address (make-locative blob))))))
      ("pointer event struct B" . ,(lambda () (wrap-event (address->pointer (pointer->address (make-locative blob))) 1 2)))
      ("blob simplum array B" . ,(lambda () (wrap-simplum-array blob 3)))
      ("blob event array B" . ,(lambda () (wrap-event-array blob 3 'a 'b)))
      ("locative simplum array B" . ,(lambda () (wrap-simplum-array (make-locative blob) 3)))
      ("locative event array B" . ,(lambda () (wrap-event-array (make-locative blob) 3 'a 'b)))
      ("pointer simplum array B" . ,(lambda () (wrap-simplum-array (address->pointer (pointer->address (make-locative blob))) 3)))
      ("pointer event array B" . ,(lambda () (wrap-event-array (address->pointer (pointer->address (make-locative blob))) 3 'a 'b)))))


  (define (compare as bs expected)
    (for-each
     (lambda (a)
       (for-each
        (lambda (b)
          (assert (eq? expected (armor-eq? ((cdr a)) ((cdr b))))
                  (sprintf "~A vs ~A = ~A failed"
                           (car a) (car b) expected)))
        bs))
     as)
    #t)

  (test-assert "nulls vs nulls = #t"
    (compare nulls nulls #t))
  (test-assert "b-addrs vs nulls = #f"
    (compare b-addrs nulls #f))
  (test-assert "a-addrs vs b-addrs = #f"
    (compare a-addrs b-addrs #f))
  (test-assert "b-addrs vs b-addrs = #t"
    (compare b-addrs b-addrs #t)))


(test-group "armor-null?"
  (test "Returns #t if armor holds a null pointer"
        #t
        (armor-null? (wrap-simplum (address->pointer 0))))
  (test "Returns #t if armor holds #f"
        #t
        (armor-null? (wrap-simplum #f)))
  (test "Returns #f if armor holds a non-null pointer"
        #f
        (armor-null? (wrap-simplum (address->pointer 123))))
  (test "Returns #f if armor holds a locative"
        #f
        (armor-null? (wrap-simplum (make-locative (make-blob 8)))))
  (test "Returns #f if armor holds a blob"
        #f
        (armor-null? (wrap-simplum (make-blob 8))))

  (test "Returns #t if array holds a null pointer"
        #t
        (armor-null? (wrap-simplum-array (address->pointer 0) 0)))
  (test "Returns #t if array holds #f"
        #t
        (armor-null? (wrap-simplum-array #f 0)))
  (test "Returns #f if array holds a non-null pointer"
        #f
        (armor-null? (wrap-simplum-array (address->pointer 123) 2)))
  (test "Returns #f if array holds a locative"
        #f
        (armor-null? (wrap-simplum-array (make-locative (make-blob 8)) 2)))
  (test "Returns #f if array holds a blob"
        #f
        (armor-null? (wrap-simplum-array (make-blob 8) 2))))


(test-group "armor-nullify!"
  (test "Sets armor pointer to #f"
        #f
        (let ((subject (make-simplum/autofree)))
          (armor-nullify! subject)
          (%jiffi:armor-data subject)))

  (test "Deregisters with parent (basic)"
        #f
        (let* ((array (make-event-array/autofree 3))
               (before (event-array-map (lambda (i ev) ev) array))
               (subject (event-array-ref array 0))
               (after (event-array-map (lambda (i ev) ev) array)))
          (assert (every (cut armor-has-child? array <>) before))
          (assert (armor-has-child? array subject))
          (assert (every (cut armor-has-child? array <>) after))

          (armor-nullify! subject)

          (begin0
           (armor-has-child? subject array)
           (assert (every (cut armor-has-child? array <>) before))
           (assert (every (cut armor-has-child? array <>) after))
           (assert (= (+ (length before) (length after))
                      (armor-kids-count array))))))

  (test "Deregisters with parent (thread-safe)"
        #f
        (let* ((array (make-safe-array 3))
               (before (safe-array-map (lambda (i ev) ev) array))
               (subject (safe-array-ref array 0))
               (after (safe-array-map (lambda (i ev) ev) array)))
          (thread-sleep! 0.01)
          (assert (every (cut armor-has-child? array <>) before))
          (assert (armor-has-child? array subject))
          (assert (every (cut armor-has-child? array <>) after))

          (armor-nullify! subject)

          (thread-sleep! 0.01)
          (begin0
           (armor-has-child? array subject)
           (assert (every (cut armor-has-child? array <>) before))
           (assert (every (cut armor-has-child? array <>) after))
           (assert (= (+ (length before) (length after))
                      (armor-kids-count array)))
           (free-safe-array! array))))

  (test "Sets armor parent to #f"
        #f
        (let* ((array (make-event-array/autofree 3))
               (subject (event-array-ref array 0)))
          (assert (eq? array (%jiffi:armor-parent subject)))
          (armor-nullify! subject)
          (%jiffi:armor-parent subject)))

  (test "Basic array nullifies all children"
        '(#t #t #t #f #f)
        (let* ((subject (make-event-array/blob 3))
               (item0 (event-array-ref subject 0))
               (items (event-array-map (lambda (i ev) ev) subject)))
          ;; Verify assumptions
          (assert (not (armor-null? item0)))
          (assert (not (any armor-null? items)))
          (assert (= 4 (armor-kids-count subject)))
          (assert (armor-has-child? subject item0))
          (assert (every (cut armor-has-child? subject <>) items))

          (armor-nullify! subject)

          (list (armor-null? item0)
                (every armor-null? items)
                (= 0 (armor-kids-count subject))
                (armor-has-child? subject item0)
                (any (cut armor-has-child? subject <>) items))))

  (test "Thread-safe array nullifies all children"
        '(#t #t #t #f #f)
        (let* ((subject (make-safe-array 3))
               (item0 (safe-array-ref subject 0))
               (items (safe-array-map (lambda (i ev) ev) subject)))
          ;; Verify assumptions
          (thread-sleep! 0.1)
          (assert (not (armor-null? item0)))
          (assert (not (any armor-null? items)))
          (assert (= 4 (armor-kids-count subject)))
          (assert (armor-has-child? subject item0))
          (assert (every (cut armor-has-child? subject <>) items))

          (armor-nullify! subject)
          (thread-sleep! 0.1)

          (begin0
           (list (armor-null? item0)
                 (every armor-null? items)
                 (= 0 (armor-kids-count subject))
                 (armor-has-child? subject item0)
                 (any (cut armor-has-child? subject <>) items))
           (free-safe-array! subject))))

  (test "Neglectful array does not nullify children"
        '(#f #f)
        (let* ((subject (make-bad-array 3))
               (item0 (bad-array-ref subject 0))
               (items (bad-array-map (lambda (i ev) ev) subject)))
          ;; Verify assumptions
          (assert (not (armor-null? item0)))
          (assert (not (any armor-null? items)))

          (armor-nullify! subject)

          (begin0
           (list (armor-null? item0)
                 (any armor-null? items))
           (free-bad-array! subject)))))


(test-group "define-armor-type"
  (test-group "for struct/union"
    (test-group "predicate"
      (test "Returns #t for structs of correct type"
            #t
            (simplum? (make-simplum/autofree)))

      (test "Returns #f for structs of wrong type"
            #f
            (simplum? (make-event/blob)))

      (test "Returns #f for non-structs"
            #f
            (or (simplum? #t)
                (simplum? 'simplum)
                (simplum? 0))))


    (test-group "wrapper"
      (test-assert "Returns a struct wrapping the given pointer"
        (let* ((pointer (allocate 8))
               (subject (wrap-simplum pointer)))
          (begin0
           (and (simplum? subject)
                (eq? pointer
                     (%jiffi:armor-data subject)))
           (free pointer))))

      (test "Accepts arguments for extra slots"
            '(1234 5678)
            (let* ((pointer (allocate 8))
                   (subject (wrap-event pointer 1234 5678)))
              (begin0
               (list (event-data1 subject)
                     (event-data2 subject))
               (free pointer)))))


    (test-group "unwrapper"
      (let* ((pointer (allocate 8))
             (subject (wrap-simplum pointer)))
        (test "Returns pointer if struct wraps pointer"
              pointer
              (unwrap-simplum subject))
        (free pointer))

      (let* ((blob (make-blob 8))
             (locative (make-locative blob))
             (subject (wrap-simplum locative)))
        (test "Returns locative if struct wraps locative"
              locative
              (unwrap-simplum subject)))

      (let* ((blob (make-blob 8))
             (subject (wrap-simplum blob)))
        (test "Returns locative if struct wraps blob"
              (make-locative blob)
              (unwrap-simplum subject)))

      (test "Returns #f if struct wraps #f"
            #f
            (unwrap-simplum (wrap-simplum #f)))

      (let ((pointer (allocate 8)))
        (test "Returns pointer if given pointer"
              pointer
              (unwrap-simplum pointer))
        (free pointer))

      (let* ((blob (make-blob 8))
             (locative (make-locative blob)))
        (test "Returns locative if given locative"
              locative
              (unwrap-simplum locative)))

      (let ((blob (make-blob 8)))
        (test "Returns locative if given blob"
              (make-locative blob)
              (unwrap-simplum blob)))

      (test "Returns #f if given #f"
            #f
            (unwrap-simplum #f))

      (test-assert "[test eval import]"
        (eval '(begin
                 (import libfoo)
                 (unwrap-simplum (make-simplum/autofree)))))

      (test-error "Error if given wrong struct type"
        (eval '(begin
                 (import libfoo)
                 (unwrap-simplum (make-event/blob)))))

      (test-error "Error if given other type"
        (eval '(begin
                 (import libfoo)
                 (unwrap-simplum 123)))))


    (test-group "additional slot getters and getters"
      (test "Getters return the slot values"
            '("d1" d2)
            (let ((subject (make-event/blob)))
              (list (event-data1 subject)
                    (event-data2 subject))))

      (test "(setter GETTER) allows getter to be set"
            "foo"
            (let ((subject (make-event/blob)))
              (set! (event-data1 subject) "foo")
              (event-data1 subject)))

      (test "setter procedures set the slot"
            "bar"
            (let ((subject (make-event/blob)))
              (event-data2-set! subject "bar")
              (event-data2 subject)))))

  (test-group "for array"
    (test-group "predicate"
      (test "Returns #t for arrays of correct type"
            '(#t #t)
            (list (event-array? (make-event-array/blob 8))
                  (simplum-array? (make-simplum-array/autofree 8))))

      (test "Returns #f for arrays of wrong type"
            '(#f #f)
            (list (event-array? (make-simplum-array/autofree 8))
                  (simplum-array? (make-event-array/blob 8))))

      (test "Returns #f for non-arrays"
            #f
            (or (simplum-array? #t)
                (simplum-array? 'simplum)
                (simplum-array? 0))))


    (test-group "wrapper"
      (test-assert "Returns an array wrapping the given pointer"
        (let* ((pointer (allocate 8))
               (subject (wrap-simplum-array pointer 2)))
          (begin0
           (and (simplum-array? subject)
                (eq? pointer
                     (%jiffi:armor-data subject)))
           (free pointer))))

      (test "Sets the array length"
            13
            (let* ((pointer (allocate 8))
                   (subject (wrap-simplum-array pointer 13)))
              (begin0
               (simplum-array-length subject)
               (free pointer))))

      (test "Accepts arguments for data slots"
            '(1234 5678)
            (let* ((pointer (allocate 32))
                   (subject (wrap-event-array pointer 2 1234 5678)))
              (begin0
               (list (event-array-data1 subject)
                     (event-array-data2 subject))
               (free pointer)))))


    (test-group "unwrapper"
      (let* ((pointer (allocate 8))
             (subject (wrap-simplum-array pointer 2)))
        (test "Returns pointer if array wraps pointer"
              pointer
              (unwrap-simplum-array subject))
        (free pointer))

      (let* ((blob (make-blob 8))
             (locative (make-locative blob))
             (subject (wrap-simplum-array locative 2)))
        (test "Returns locative if array wraps locative"
              locative
              (unwrap-simplum-array subject)))

      (let* ((blob (make-blob 8))
             (subject (wrap-simplum-array blob 2)))
        (test "Returns locative if array wraps blob"
              (make-locative blob)
              (unwrap-simplum-array subject)))

      (test "Returns #f if array wraps #f"
            #f
            (unwrap-simplum-array (wrap-simplum-array #f 0)))

      (let ((pointer (allocate 8)))
        (test "Returns pointer if given pointer"
              pointer
              (unwrap-simplum-array pointer))
        (free pointer))

      (let* ((blob (make-blob 8))
             (locative (make-locative blob)))
        (test "Returns locative if given locative"
              locative
              (unwrap-simplum-array locative)))

      (let ((blob (make-blob 8)))
        (test "Returns locative if given blob"
              (make-locative blob)
              (unwrap-simplum-array blob)))

      (test "Returns #f if given #f"
            #f
            (unwrap-simplum-array #f))

      (test-assert "[test eval import]"
        (eval '(begin
                 (import libfoo)
                 (unwrap-simplum-array (make-simplum-array/autofree 8)))))

      (test-error "Error if given wrong array type"
        (eval '(begin
                 (import libfoo)
                 (unwrap-simplum-array (make-event-array/blob 4)))))

      (test-error "Error if given other type"
        (eval '(begin
                 (import libfoo)
                 (unwrap-simplum-array 123)))))


    (test-group "length"
      (test "Returns the number of items in the array"
            3
            (simplum-array-length
             (wrap-simplum-array (make-blob 24) 3))))


    (test-group "additional slot getters and getters"
      (test "Getters return the slot values"
            '("a1" a2)
            (let ((subject (make-event-array/blob 4)))
              (list (event-array-data1 subject)
                    (event-array-data2 subject))))

      (test "(setter GETTER) allows getter to be set"
            "foo"
            (let ((subject (make-event-array/blob 4)))
              (set! (event-array-data1 subject) "foo")
              (event-array-data1 subject)))

      (test "setter procedures set the slot"
            "bar"
            (let ((subject (make-event-array/blob 4)))
              (event-array-data2-set! subject "bar")
              (event-array-data2 subject))))))


(test-group "define-armor-printer"
  (let ((subject (make-simplum/autofree)))
    (test "Has expected format (simplum)"
          (sprintf "#<simplum 0x~X>"
                   (pointer->address (unwrap-simplum subject)))
          (sprintf "~A" subject)))

  (test "Has expected format (event)"
        "#<event sensor>"
        (let ((subject (make-event/autofree)))
          (set! (event-type subject) 'sensor)
          (sprintf "~A" subject)))

  (test "Has expected format (event #f)"
        "#<event NULL>"
        (sprintf "~A" (wrap-event #f)))

  (test "Has expected format (event address 0)"
        "#<event NULL>"
        (sprintf "~A" (wrap-event (address->pointer 0))))

  (test "Has expected format (key event)"
        "#<key-event z mods: (lctrl rctrl ctrl rshift)>"
        (let ((subject (make-key-event)))
          (set! (key-event-sym subject) 'z)
          (set! (key-event-mods subject) '(ctrl rshift))
          (sprintf "~A" subject))))
