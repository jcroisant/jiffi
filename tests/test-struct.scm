
(test-group "define-struct-allocators"
  (test-group "free"
    (test-group "non-child struct"
      (test-assert "Nullifies the struct"
        (let ((subject (make-event)))
          (free-event! subject)
          (armor-null? subject)))

      (test-assert "Returns the struct"
        (let ((subject (make-event)))
          (eq? subject
               (free-event! subject)))))

    (test-group "child struct"
      (define (make-parent-and-child)
        (let ((parent (make-event-array/autofree 2)))
          (values parent (event-array-ref parent 1))))

      (test-assert "Nullifies the struct"
        (let-values (((parent child) (make-parent-and-child)))
          (free-event! child)
          (armor-null? child)))

      (test-assert "Does not free memory if struct is a child"
        (let-values (((parent child) (make-parent-and-child)))
          (assert (eq? (armor-parent child) parent))
          ;; Program would crash if this pointer was freed
          (set! (%jiffi:armor-data child) (address->pointer -1))
          (free-event! child)
          (armor-null? child))))

    (test-error "Signals error if given wrong struct type"
      (eval '(begin
               (import libfoo)
               (free-event! (make-simplum/autofree))))))

  (test-group "alloc"
    (test-assert "Returns a tagged pointer"
      (let ((subject (alloc-event)))
        (begin0
         (tagged-pointer? subject 'event)
         (free subject))))

    (test-assert "The pointer memory is zeroed out"
      (bytes-zeroed?
       (alloc-event)
       sizeof-FOO_Event)))

  (test-group "alloc/blob"
    (test-assert "Returns a blob"
      (blob? (alloc-event/blob)))

    (test "The blob is the correct size"
          sizeof-FOO_Event
          (blob-size (alloc-event/blob)))

    (test-assert "The blob is zeroed out"
      (bytes-zeroed?
       (make-locative (alloc-event/blob))
       sizeof-FOO_Event)))

  (test-group "make"
    (test-assert "Returns a struct of the correct type"
      (let ((subject (make-event)))
        (begin0
         (event? subject)
         (free-event! subject))))

    (test-assert "The struct wraps a tagged pointer"
      (let ((subject (make-event)))
        (begin0
         (tagged-pointer? (%jiffi:armor-data subject) 'event)
         (free-event! subject))))

    (test-assert "The pointer memory is zeroed out"
      (bytes-zeroed?
       (unwrap-event (make-event))
       sizeof-FOO_Event))

    (test "Sets additional slots to default values"
          '("d1" d2)
          (let ((subject (make-event)))
            (begin0
             (list (event-data1 subject)
                   (event-data2 subject))
             (free-event! subject)))))

  (test-group "make/autofree"
    (test-assert "Returns a struct of the correct type"
      (event? (make-event/autofree)))

    (test-assert "The struct wraps a tagged pointer"
      (let ((subject (make-event/autofree)))
        (tagged-pointer? (%jiffi:armor-data subject) 'event)))

    (test-assert "The pointer memory is zeroed out"
      (bytes-zeroed?
       (unwrap-event (make-event/autofree))
       sizeof-FOO_Event))

    (test "Sets additional slots to default values"
          '("d1" d2)
          (let ((subject (make-event/autofree)))
            (list (event-data1 subject)
                  (event-data2 subject)))))

  (test-group "make/blob"
    (test-assert "Returns a struct of the correct type"
      (event? (make-event/blob)))

    (test-assert "The struct wraps a blob"
      (blob? (%jiffi:armor-data (make-event/blob))))

    (test "The blob is the correct size"
          sizeof-FOO_Event
          (blob-size (%jiffi:armor-data (make-event/blob))))

    (test-assert "The blob is zeroed out"
      (bytes-zeroed?
       (unwrap-event (make-event/blob))
       sizeof-FOO_Event))

    (test "Sets additional slots to default values"
          '("d1" d2)
          (let ((subject (make-event/blob)))
            (list (event-data1 subject)
                  (event-data2 subject))))))


(test-group "struct-copier"
  (define constructors
    `(("pointer"             . ,alloc-key-event)
      ("blob"                . ,alloc-key-event/blob)
      ("struct with pointer" . ,make-key-event)
      ("struct with blob"    . ,make-key-event/blob)))

  (define null-constructors
    `(("#f"             . ,(lambda () #f))
      ("pointer 0"      . ,(lambda () (address->pointer 0)))
      ("struct with #f" . ,(lambda () (wrap-event #f)))
      ("struct with pointer 0" .
       ,(lambda () (wrap-event (address->pointer 0))))))

  (test-assert "Copies bytes from one struct to another"
    (begin
      (for-each
       (lambda (src-constructor)
         (for-each
          (lambda (dest-constructor)
            (let ((src ((cdr src-constructor)))
                  (dest ((cdr dest-constructor))))
              (set! (event-type (unwrap-key-event src)) 'key)
              (set! (key-event-sym src) 'a)
              (set! (key-event-mods src) '(lctrl))

              (copy-key-event! src dest)

              (let ((result (list (event-type (unwrap-key-event dest))
                                  (key-event-sym dest)
                                  (key-event-mods dest))))
                (assert (equal? result '(key a (lctrl)))
                        (sprintf "Copy ~A to ~A: ~A"
                                 (car src-constructor)
                                 (car dest-constructor)
                                 result)))))
          constructors))
       constructors)
      #t))

  (test-assert "Has no effect if src and dest are same"
    (begin
      (for-each
       (lambda (constructor)
         (let ((src ((cdr constructor))))
           (set! (event-type (unwrap-key-event src)) 'key)
           (set! (key-event-sym src) 'a)
           (set! (key-event-mods src) '(lctrl))

           (copy-key-event! src src)

           (let ((result (list (event-type (unwrap-key-event src))
                               (key-event-sym src)
                               (key-event-mods src))))
             (assert (equal? result '(key a (lctrl)))
                     (sprintf "Copy ~A to same ~A: ~A"
                              (car constructor)
                              (car constructor)
                              result)))))
       constructors)
      #t))

  (test-assert "Signals error if src is null"
    (begin
      (for-each
       (lambda (src-constructor)
         (for-each
          (lambda (dest-constructor)
            (let ((src ((cdr src-constructor)))
                  (dest ((cdr dest-constructor))))
              (assert (error? (copy-key-event! src dest))
                      (sprintf "Copy ~A to ~A"
                               (car src-constructor)
                               (car dest-constructor)))))
          constructors))
       null-constructors)
      #t))

  (test-assert "Signals error if dest is null"
    (begin
      (for-each
       (lambda (src-constructor)
         (for-each
          (lambda (dest-constructor)
            (let ((src ((cdr src-constructor)))
                  (dest ((cdr dest-constructor))))
              (assert (error? (copy-key-event! src dest))
                      (sprintf "Copy ~A to ~A"
                               (car src-constructor)
                               (car dest-constructor)))))
          null-constructors))
       constructors)
      #t)))


(test-group "rewrappers"
  (test "Returns a record of the new type"
        '(#t #t)
        (let ((subject1 (event->key-event (make-event)))
              (subject2 (event->sensor-event (make-event))))
          (begin0
           (list (key-event? subject1)
                 (sensor-event? subject2))
           (free-key-event! subject1)
           (free-sensor-event! subject2))))

  (test "Points to the same memory address as the original"
        '(#t #t)
        (let* ((ev1 (make-event))
               (ev2 (make-event))
               (addr1 (armor-address ev1))
               (addr2 (armor-address ev2))
               (subject1 (event->key-event ev1))
               (subject2 (event->sensor-event ev2)))
          (begin0
           (list (= addr1 (armor-address subject1))
                 (= addr2 (armor-address subject2)))
           (free-key-event! subject1)
           (free-sensor-event! subject2))))

  (test "Nullifies the original"
        '(#t #t)
        (let* ((ev1 (make-event))
               (ev2 (make-event))
               (subject1 (event->key-event ev1))
               (subject2 (event->sensor-event ev2)))
          (begin0
           (list (armor-null? ev1)
                 (armor-null? ev2))
           (free-key-event! subject1)
           (free-sensor-event! subject2)))))


(test-group "define-struct-field-accessors"
  (test-group "accessors without converters"
    (test "Get and set the field values"
          123
          (let ((subject (make-key-event/blob)))
            (key-event-sym-int-set! subject 123)
            (key-event-sym-int subject)))

    (test "(setter GETTER) allows getter to be set"
          456
          (let ((subject (make-key-event/blob)))
            (set! (key-event-mods-int subject) 456)
            (key-event-mods-int subject))))


  (test-group "accessors with converters"
    (test "Getter (symbol)"
          'c
          (let ((subject (make-key-event/blob)))
            (key-event-sym-int-set! subject FOO_KEY_C)
            (key-event-sym subject)))

    (test "Getter (symbol list)"
          '(lctrl rctrl ctrl rshift)
          (let ((subject (make-key-event/blob)))
            (set! (key-event-mods-int subject)
                  (+ FOO_KMOD_CTRL FOO_KMOD_RSHIFT))
            (key-event-mods subject)))

    (test "Setter"
          26
          (let ((subject (make-key-event/blob)))
            (key-event-sym-set! subject 'z)
            (key-event-sym-int subject)))

    (test-error "Setter signals error if conversion fails"
      (let ((subject (make-key-event/blob)))
        (key-event-sym-set! subject 'x)))

    (test "set! getter with converter"
          (+ FOO_KMOD_LCTRL FOO_KMOD_SHIFT)
          (let ((subject (make-key-event/blob)))
            (set! (key-event-mods subject)
                  '(lctrl lshift rshift))
            (key-event-mods-int subject)))

    (test-error "set! getter signals error if conversion fails"
      (let ((subject (make-event/blob)))
        (set! (key-event-mods subject)
              '(zzz))))))


(test-group "struct-getter"
  (test "Gets the struct field (no conversion)"
        FOO_EVENT_TYPE_SENSOR
        (let ((subject (make-event/blob)))
          (set! (event-type subject) 'sensor)
          (event-type-int-getter subject)))

  (test "Gets the struct field (conversion)"
        'sensor
        (let ((subject (make-event/blob)))
          (set! (event-type subject) 'sensor)
          (event-type-getter subject)))

  (test-error "Signals error if given wrong struct type"
    (event-type-getter (make-simplum/blob))))


(test-group "struct-setter"
  (test "Sets the struct field (no conversion)"
        123
        (let ((subject (make-event/blob)))
          (event-type-int-setter subject 123)
          (event-type-int-getter subject)))

  (test "Sets the struct field (conversion)"
        FOO_EVENT_TYPE_SENSOR
        (let ((subject (make-event/blob)))
          (event-type-setter subject 'sensor)
          (event-type-int-getter subject)))

  (test-error "Signals error if given wrong struct type"
    (event-type-setter (make-simplum/blob) 'sensor))

  (test-error "Signals error if given unrecognized value"
    (event-type-setter (make-event/blob) 'zzz)))
