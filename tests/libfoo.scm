
;; Bindings to the example library in libfoo.h, and various other code
;; to test jiffi features.

(foreign-declare "#include \"libfoo.h\"")


(module libfoo ()
  (import scheme)
  (cond-expand
   (chicken-4
    (import chicken foreign)
    (use extras
         lolevel
         srfi-18
         jiffi)
    (define-type integer number))
   (else
    (import (chicken base)
            (chicken format)
            (chicken foreign)
            (chicken module)
            (chicken syntax)
            (chicken type)
            (srfi 18)
            jiffi)
    (import-for-syntax
     ;; This is only needed to register the 'foo-1.1+ feature, so it
     ;; can be used in define-enum-group requirements
     (only (chicken platform)
           register-feature!))))


  ;; For unit tests
  (export sizeof-FOO_Event)
  (define sizeof-FOO_Event (foreign-type-size "FOO_Event"))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-FOREIGN-VALUES

  ;; #define
  (define-foreign-values
    type: c-string
    export: #t
    FOO_BAR
    FOO_BUZZ)

  ;; const
  (define-foreign-values
    export: #t
    type: float
    (φ FOO_PHI)
    (π FOO_PI))

  ;; enum
  (define-foreign-values
    export: #t
    type: int
    FOO_BLEND_NONE
    FOO_BLEND_ADD
    FOO_BLEND_SUB
    FOO_BLEND_MUL)

  ;; C expressions
  (define-foreign-values
    type: double
    export: #t
    (four "1.5 + 2.5")
    (five "FOO_ADD1(4)")
    (sqrt2 "sqrt(2.0)"))

  ;; export: #f
  (define-foreign-values
    export: #f
    type: int
    FOO_SECRET1)

  ;; No export clause
  (define-foreign-values
    type: int
    FOO_SECRET2)


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ENUM-GROUP

  (export power-level->int
          int->power-level)

  ;; Or compile with flag: -D foo-1.1+
  (begin-for-syntax
   (register-feature! 'foo-1.1+))

  (define-enum-group
    vars: export
    type: int

    symbol->int: power-level->int
    allow-ints: #t

    int->symbol: int->power-level

    ;; These requirements are always satisfied.
    ((requires: chicken "1")
     (empty  (power/empty  FOO_POWER_EMPTY))
     (none   (power/none   FOO_POWER_NONE)   alias)
     (low    (power/low    "FOO_POWER_LOW + 1 - 1"))
     (high   (power/high   FOO_POWER_HIGH))
     (full   (power/full   FOO_POWER_FULL)))

    ;; Feature ID foo-1.1+ is registered above. These are satisfied.
    ((requires: (and chicken foo-1.1+) "FOO_VERSION >= 110")
     (wired  (power/wired  FOO_POWER_WIRED)))

    ;; These requirements are not satisfied, so this will be ignored.
    ((requires: foo-1.2+ "FOO_VERSION >= 120")
     (charge (power/charge FOO_POWER_CHARGE))))


  (export event-type->int
          int->event-type)

  (define-enum-group
    vars: export
    type: int
    symbol->int: event-type->int
    int->symbol: int->event-type
    ((none   FOO_EVENT_TYPE_NONE)
     (key    FOO_EVENT_TYPE_KEY)
     (sensor FOO_EVENT_TYPE_SENSOR)))


  (export keysym->int
          int->keysym
          keymod->int
          int->keymod)

  (define-enum-group
    type: int
    vars: export
    symbol->int: keysym->int
    allow-ints: #t
    int->symbol: int->keysym
    ((a FOO_KEY_A)
     (b FOO_KEY_B)
     (c FOO_KEY_C)
     (z FOO_KEY_Z)))

  (define-enum-group
    type: int
    vars: export
    symbol->int: keymod->int
    allow-ints: #t
    int->symbol: int->keymod
    ((none   FOO_KMOD_NONE)
     (lctrl  FOO_KMOD_LCTRL)
     (rctrl  FOO_KMOD_RCTRL)
     (ctrl   FOO_KMOD_CTRL)
     (lshift FOO_KMOD_LSHIFT)
     (rshift FOO_KMOD_RSHIFT)
     (shift  FOO_KMOD_SHIFT)))


  ;; Only procedures are exported
  (export secret1->int
          int->secret1)
  (define-enum-group
    type: int
    symbol->int: secret1->int
    int->symbol: int->secret1
    ((secret1 FOO_SECRET1)))

  ;; Neither procedures nor constants are exported
  (define-enum-group
    int->symbol: int->secret2
    symbol->int: secret2->int
    type: int
    vars: export
    ((secret2 FOO_SECRET2)))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ENUM-PACKER / DEFINE-ENUM-UNPACKER

  (export pack-keymods
          unpack-keymods)

  (define-enum-packer pack-keymods
    (keymod->int)
    allow-ints: #t)

  (define-enum-unpacker unpack-keymods
    (int->keymod)
    masks: (list FOO_KMOD_LCTRL
                 FOO_KMOD_RCTRL
                 FOO_KMOD_CTRL
                 FOO_KMOD_LSHIFT
                 FOO_KMOD_RSHIFT
                 FOO_KMOD_SHIFT))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ARMOR-TYPE FOR STRUCTS/UNIONS

  (export simplum?
          wrap-simplum
          unwrap-simplum)

  (define-armor-type simplum
    pred:   simplum?
    wrap:   wrap-simplum
    unwrap: unwrap-simplum)


  (export blackbox?
          wrap-blackbox
          unwrap-blackbox)

  ;; Opaque struct pointer
  (define-armor-type blackbox
    pred:   blackbox?
    wrap:   wrap-blackbox
    unwrap: unwrap-blackbox)


  (export event?
          wrap-event
          unwrap-event
          event-data1
          event-data2
          event-data2-set!

          key-event?
          wrap-key-event
          unwrap-key-event

          sensor-event?
          wrap-sensor-event
          unwrap-sensor-event

          event->key-event
          event->sensor-event)

  (define-armor-type event
    pred: event?
    wrap: wrap-event
    unwrap: unwrap-event
    (data1  event-data1  (setter event-data1))
    (data2  event-data2  event-data2-set!))

  ;; Also make data2 settable with (set! (event-data2 struct) value)
  (set! (setter event-data2) event-data2-set!)

  (define-armor-type key-event
    pred:   key-event?
    wrap:   wrap-key-event
    unwrap: unwrap-key-event)

  (define-armor-type sensor-event
    pred:   sensor-event?
    wrap:   wrap-sensor-event
    unwrap: unwrap-sensor-event)

  ;; Rewrap the event pointer in a new record type. Nullifies the
  ;; original record to avoid unwanted auto-freeing of the memory.
  (: event->key-event
     ((struct event) -> (struct key-event)))
  (define (event->key-event ev)
    (let ((pointer (unwrap-event ev)))
      (armor-nullify! ev)
      (wrap-key-event pointer)))

  (: event->sensor-event
     ((struct event) -> (struct sensor-event)))
  (define (event->sensor-event ev)
    (let ((pointer (unwrap-event ev)))
      (armor-nullify! ev)
      (wrap-sensor-event pointer)))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-STRUCT-ALLOCATORS

  (export free-simplum!
          make-simplum
          make-simplum/autofree

          free-event!
          alloc-event
          alloc-event/blob
          make-event
          make-event/autofree
          make-event/blob

          free-key-event!
          alloc-key-event
          alloc-key-event/blob
          make-key-event
          make-key-event/autofree
          make-key-event/blob

          free-sensor-event!
          alloc-sensor-event
          alloc-sensor-event/blob
          make-sensor-event
          make-sensor-event/autofree
          make-sensor-event/blob)

  (define-struct-allocators
    (simplum "FOO_Simplum" simplum? wrap-simplum)
    free:    free-simplum!
    make:    make-simplum
    make/af: make-simplum/autofree)

  (define-struct-allocators
    (event "FOO_Event" event? wrap-event)
    make:       make-event
    alloc:      alloc-event
    make/blob:  make-event/blob
    alloc/blob: alloc-event/blob
    make/af:    make-event/autofree
    free:       free-event!
    defaults:   ("d1" 'd2))

  ;; A key-event record actually wraps a FOO_Event union
  (define-struct-allocators
    (key-event "FOO_Event" key-event? wrap-key-event)
    free:       free-key-event!
    alloc:      alloc-key-event
    alloc/blob: alloc-key-event/blob
    make:       make-key-event
    make/blob:  make-key-event/blob
    make/af:    make-key-event/autofree)

  ;; A sensor-event record actually wraps a FOO_Event union
  (define-struct-allocators
    (sensor-event "FOO_Event" sensor-event? wrap-sensor-event)
    free:       free-sensor-event!
    alloc:      alloc-sensor-event
    alloc/blob: alloc-sensor-event/blob
    make:       make-sensor-event
    make/blob:  make-sensor-event/blob
    make/af:    make-sensor-event/autofree)


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; FOREIGN TYPES WITH AUTO-CONVERSION

  (define-foreign-type FOO_PowerLevel
    int
    power-level->int
    int->power-level)

  (define-foreign-type FOO_EventType
    int
    event-type->int
    int->event-type)

  (define-foreign-type FOO_KeySym
    int
    keysym->int
    int->keysym)

  (define-foreign-type FOO_KeyMod
    int
    pack-keymods
    unpack-keymods)

  ;; FOO_Blackbox is an opaque pointer type.
  (define-foreign-type FOO_Blackbox
    c-pointer
    unwrap-blackbox
    wrap-blackbox)

  (define-foreign-type FOO_Event*
    (nonnull-c-pointer "FOO_Event")
    unwrap-event
    wrap-event)

  ;; A key-event record actually wraps a FOO_Event union
  (define-foreign-type FOO_KeyEvent*
    (nonnull-c-pointer "FOO_Event")
    unwrap-key-event
    wrap-key-event)

  ;; A sensor-event record actually wraps a FOO_Event union
  (define-foreign-type FOO_SensorEvent*
    (nonnull-c-pointer "FOO_Event")
    unwrap-sensor-event
    wrap-sensor-event)


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; STRUCT-COPIER

  (export copy-key-event!
          copy-key-event)

  (define copy-key-event!
    (struct-copier
     ("FOO_Event" unwrap-key-event)
     name: 'copy-key-event!))

  (define (copy-key-event ev)
    ((struct-copier
      ("FOO_Event" unwrap-key-event)
      name: 'copy-key-event)
     ev (make-key-event/blob)))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-STRUCT-ACCESSORS

  (export simplum-x
          simplum-x-set!

          event-type
          event-type-set!
          event-type-int-getter
          event-type-getter
          event-type-int-setter
          event-type-setter)

  (define-struct-accessors
    (simplum "FOO_Simplum" simplum? unwrap-simplum)
    ("x"
     type:   (fixnum byte)
     getter: simplum-x
     setter: simplum-x-set!))


  (define-struct-accessors
    (event "FOO_Event" event? unwrap-event)
    ("type"
     type:   (symbol int)
     getter: event-type
     g-conv: int->event-type
     setter: event-type-set!
     s-conv: event-type->int))

  ;; Also make type settable with (set! (event-type struct) value)
  (set! (setter event-type) event-type-set!)


  ;; Redundant, but tests struct-getter and struct-setter
  (define event-type-int-getter
    (struct-getter
     (event "FOO_Event" event? unwrap-event)
     "type"
     type: int))

  (define event-type-getter
    (struct-getter
     (event "FOO_Event" event? unwrap-event)
     "type"
     type: int
     conv: int->event-type
     name: 'event-type-getter))

  (define event-type-int-setter
    (struct-setter
     (event "FOO_Event" event? unwrap-event)
     "type"
     type: int))

  (define event-type-setter
    (struct-setter
     (event "FOO_Event" event? unwrap-event)
     "type"
     type: int
     conv: event-type->int
     name: 'event-type-setter))


  (export key-event-sym
          key-event-sym-set!
          key-event-sym-int
          key-event-sym-int-set!
          key-event-mods
          key-event-mods-int)

  (define-struct-accessors
    ;; A key-event record actually wraps a FOO_Event union
    (key-event "FOO_Event" key-event? unwrap-key-event)

    ;; Access the "key" union member, then the "sym" struct field
    ("key.sym"
     type:   (fixnum int)
     getter: key-event-sym-int
     setter: key-event-sym-int-set!)

    ;; With converters
    ("key.sym"
     type:   (symbol int)
     getter: key-event-sym
     g-conv: int->keysym
     setter: key-event-sym-set!
     s-conv: keysym->int)

    ("key.mods"
     type:   (fixnum int)
     getter: key-event-mods-int
     setter: (setter key-event-mods-int))

    ("key.mods"
     type:   (list int)
     getter: key-event-mods
     g-conv: unpack-keymods
     setter: (setter key-event-mods)
     s-conv: pack-keymods))

  ;; Also make sym settable with (set! (key-event-sym struct) value)
  (set! (setter key-event-sym) key-event-sym-set!)


  (export sensor-event-id
          sensor-event-value)

  (define-struct-accessors
    ;; A sensor-event record actually wraps a FOO_Event union
    (sensor-event "FOO_Event" sensor-event? unwrap-sensor-event)

    ;; Access the "sensor" union member, then the "id" struct field
    ("sensor.id"
     type:   (fixnum int)
     getter: sensor-event-id
     setter: (setter sensor-event-id))

    ("sensor.value"
     type:   float
     getter: sensor-event-value
     setter: (setter sensor-event-value)))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ARMOR-PRINTER

  (define-armor-printer simplum
    show-address: #t)

  (define-armor-printer event
    (#f event-type))

  (define-armor-printer key-event
    (#f key-event-sym)
    (mods key-event-mods))

  (define-armor-printer sensor-event
    (id sensor-event-id)
    (value sensor-event-value))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ARMOR-TYPE FOR ARRAYS

  (export simplum-array?
          wrap-simplum-array
          unwrap-simplum-array
          simplum-array-length

          event-array?
          wrap-event-array
          unwrap-event-array
          event-array-length
          event-array-data1
          event-array-data2
          event-array-data2-set!

          safe-array?
          wrap-safe-array
          unwrap-safe-array
          safe-array-length

          bad-array?
          wrap-bad-array
          unwrap-bad-array
          bad-array-length)

  (define-armor-type simplum-array
    pred:   simplum-array?
    wrap:   wrap-simplum-array
    unwrap: unwrap-simplum-array
    (length simplum-array-length))

  (define-armor-type event-array
    pred: event-array?
    wrap: wrap-event-array
    unwrap: unwrap-event-array
    (length event-array-length)
    (data1  event-array-data1  (setter event-array-data1))
    (data2  event-array-data2  event-array-data2-set!))

  (set! (setter event-array-data2) event-array-data2-set!)

  ;; Thread-safe array.
  (define-armor-type safe-array
    pred: safe-array?
    wrap: wrap-safe-array
    unwrap: unwrap-safe-array
    locking: (make-armor-lock)
    (length safe-array-length))

  ;; Does not track children.
  (define-armor-type bad-array
    pred: bad-array?
    wrap: wrap-bad-array
    unwrap: unwrap-bad-array
    children: #f
    (length bad-array-length))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ARRAY-ALLOCATORS

  (export make-simplum-array/autofree
          free-simplum-array!

          alloc-event-array
          alloc-event-array/blob
          make-event-array
          make-event-array/autofree
          make-event-array/blob
          free-event-array!

          make-safe-array
          free-safe-array!

          make-bad-array
          make-bad-array/blob
          free-bad-array!)

  (define-array-allocators
    (simplum-array "FOO_Simplum" simplum-array? wrap-simplum-array)
    make/af:    make-simplum-array/autofree
    free:       free-simplum-array!)

  (define-array-allocators
    (event-array "FOO_Event" event-array? wrap-event-array)
    make:       make-event-array
    alloc:      alloc-event-array
    make/blob:  make-event-array/blob
    alloc/blob: alloc-event-array/blob
    make/af:    make-event-array/autofree
    free:       free-event-array!
    defaults:   ("a1" 'a2))

  (define-array-allocators
    (safe-array "FOO_Event" safe-array? wrap-safe-array)
    make: make-safe-array
    free: free-safe-array!)

  (define-array-allocators
    (bad-array "FOO_Event" bad-array? wrap-bad-array)
    make:      make-bad-array
    make/blob: make-bad-array/blob
    free:      free-bad-array!)


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ARRAY-ACCESSORS

  (export simplum-array-ref
          simplum-array-set!
          simplum-array-map
          simplum-array-for-each

          event-array-ref
          event-array-set!
          event-array-map
          event-array-for-each
          event-array-ref*
          event-array-map*
          event-array-for-each*
          event-array-ref/typed

          safe-array-ref
          safe-array-map

          bad-array-ref
          bad-array-map)

  (define-array-accessors
    (simplum-array "FOO_Simplum" simplum-array?
                   unwrap-simplum-array simplum-array-length)
    (simplum simplum? wrap-simplum unwrap-simplum)
    ref:       simplum-array-ref
    set:       simplum-array-set!
    map:       simplum-array-map
    for-each:  simplum-array-for-each)

  (define-array-accessors
    (event-array "FOO_Event" event-array? unwrap-event-array event-array-length)
    (event event? wrap-event unwrap-event)
    ref:       event-array-ref
    set:       (setter event-array-ref)
    map:       event-array-map
    for-each:  event-array-for-each
    ref*:      event-array-ref*
    map*:      event-array-map*
    for-each*: event-array-for-each*)

  ;; Test the `set: SET` form instead of `set: (setter REF)`
  (define-array-accessors
    (event-array "FOO_Event" event-array? unwrap-event-array event-array-length)
    (event event? wrap-event unwrap-event)
    set: event-array-set!)

  ;; Return an event from the array, wrapped in either a key-event or
  ;; sensor-event instance, depending on what type of event it is.
  (define (event-array-ref/typed array i)
    ;; Use the array item pointer getter to get a bare pointer/locative.
    (let* ((ptr (event-array-ref* array i))
           ;; Wrap ptr in the correct struct record type.
           (struct (case (event-type ptr)
                     ((key) (wrap-key-event ptr))
                     ((sensor) (wrap-sensor-event ptr)))))
      ;; If array is wrapped in armor, set struct's parent and return
      ;; struct. Otherwise, just return struct.
      (if (armor? array)
          (armor-parent-set! struct array)
          struct)))

  (define-array-accessors
    (safe-array "FOO_Event" safe-array? unwrap-safe-array safe-array-length)
    (event event? wrap-event unwrap-event)
    ref: safe-array-ref
    map: safe-array-map)

  (define-array-accessors
    (bad-array "FOO_Event" bad-array? unwrap-bad-array bad-array-length)
    (event event? wrap-event unwrap-event)
    ref: bad-array-ref
    map: bad-array-map)


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-ARRAY-COPY!

  (export simplum-array-copy!
          event-array-copy!
          event-array-copy)

  (define-array-copy! simplum-array-copy!
    (simplum-array "FOO_Simplum" simplum-array?
                   unwrap-simplum-array simplum-array-length))


  (define-array-copy! event-array-copy!
    (event-array "FOO_Event" event-array?
                 unwrap-event-array event-array-length))

  ;; Define a non-destructive version which creates a new array.
  (define (event-array-copy from #!optional
                            (start 0)
                            (end (event-array-length from)))
    (let ((new-array (make-event-array (- end start))))
      (event-array-copy! new-array 0 from start end)
      new-array))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-BINDING

  (export FOO_ADD1
          add-one

          create-blackbox
          destroy-blackbox!
          blackbox-power

          create-key-event
          create-key-event/manual
          FOO_CreateKeyEvent

          create-sensor-event
          create-sensor-event/manual
          FOO_CreateSensorEvent)


  ;; Using the foreign function name
  (define-binding FOO_ADD1
    return: int
    args: ((int x)))

  ;; Using a different Scheme variable name
  (define-binding (add-one FOO_ADD1)
    return: int
    args: ((int x)))


  (define-binding (create-blackbox FOO_CreateBlackbox)
    safe: #t
    return: FOO_Blackbox)

  (define (destroy-blackbox! bb)
    ;; FOO_DestroyBlackbox frees bb's data, but if bb is an armor it
    ;; will still wrap a pointer to the old address, which could cause
    ;; a memory access violation if bb is used again later.

    ;; Unwrap bb to get its pointer before it is nullified.
    (let ((ptr (unwrap-blackbox bb)))
      ;; Nullify bb if it is an armor (rather than bare data)
      (when (armor? bb)
        (armor-nullify! bb))
      ;; Pass the original pointer.
      (FOO_DestroyBlackbox ptr)))

  (define-binding FOO_DestroyBlackbox
    args: ((FOO_Blackbox bb)))

  (define-binding (blackbox-power FOO_BlackboxPower)
    return: FOO_PowerLevel
    args: ((FOO_Blackbox bb)))


  ;; Using foreign types with automatic conversion
  (define-binding (create-key-event FOO_CreateKeyEvent)
    return: FOO_KeyEvent*
    args: ((FOO_KeySym sym)
           (FOO_KeyMod mods)))

  ;; Manually converting return and argument types
  (define (create-key-event/manual sym mods)
    (wrap-key-event
     (FOO_CreateKeyEvent
      (keysym->int sym)
      (pack-keymods mods))))

  (define-binding FOO_CreateKeyEvent
    return: (nonnull-c-pointer "FOO_Event")
    args: ((int sym)
           (int mods)))


  ;; Using foreign types with automatic conversion
  (define-binding (create-sensor-event FOO_CreateSensorEvent)
    return: FOO_SensorEvent*
    args: ((int id)
           (float value)))

  ;; Manually converting return type
  (define (create-sensor-event/manual id value)
    (wrap-sensor-event
     (FOO_CreateSensorEvent id value)))

  (define-binding FOO_CreateSensorEvent
    return: (nonnull-c-pointer "FOO_Event")
    args: ((int id)
           (float value)))


  (export populate-events!
          get-events
          event-array-get-type)

  (define-binding FOO_PopulateEvents
    return: int
    args: ((int count)
           ((nonnull-c-pointer "FOO_Event") array_out)))

  (define (populate-events! count array-out)
    (FOO_PopulateEvents count (unwrap-event-array array-out)))

  (define-binding FOO_GetEvents
    return: (nonnull-c-pointer "FOO_Event")
    args: ((int count)))

  (define (get-events count)
    (wrap-event-array (FOO_GetEvents count) count))


  (define-binding FOO_EventArray_GetType
    return: FOO_EventType
    args: (((nonnull-c-pointer "FOO_Event") array)
           (int i)))

  (define (event-array-get-type array i)
    (FOO_EventArray_GetType (unwrap-event-array array) i))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; DEFINE-BINDING**

  (export squaref
          square-int
          add-three
          empty-function)

  (define-binding** squaref
    safe:   #t ;; not needed, just for testing
    return: float
    args:   ((float x))
    "C_return(x * x);")

  ;; Expands into a define-binding** that performs an integer math
  ;; operation described by the macro caller.
  (define-syntax define-math-fn
    (syntax-rules ()
      ((define-math-fn (FN VAR)
         OPERATOR N)
       (define-binding** FN
         return: int
         args:   ((int VAR))
         ("C_return(~A ~A ~A);" 'VAR 'OPERATOR N)))))

  ;; Body becomes "C_return(y * y);"
  (define-math-fn (square-int y)
    * 'y)

  ;; Body becomes "C_return(x + 3);"
  (define-math-fn (add-three x)
    + (/ 6 2))

  (define-binding** empty-function
    "")


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; FOREIGN-LAMBDA**

  (export return-foo
          do-some-math
          return-42)

  (define return-foo
    (foreign-lambda**
     c-string
     () ;; no arguments
     "C_return(\"foo\");"))

  (define do-some-math
    (foreign-lambda**
     int
     ((int a)
      (int b))
     ;; Quoted symbols, strings, numbers, and expressions using
     ;; built-in procedures are valid FORMAT-ARGs.
     ("int ~A = ~A + a + b;" 'x (* 42 42))
     "C_return(x);"))

  ;; Expands into a foreign-lambda** that performs a simple integer
  ;; math operation described by the macro caller. X and Y are
  ;; evaluated at macro-expansion time.
  (define-syntax c-math
    (syntax-rules ()
      ((math-fn X OPERATOR Y)
       (foreign-lambda**
        int
        () ;; no args
        ("C_return(~A ~A ~A);" X 'OPERATOR Y)))))

  (define return-42 (c-math (* 8 5) + (/ 4 2)))


  ;;:;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; CALLBACKS

  (export do-callback
          do-callback2
          squaref-ptr
          cubef-ptr
          FOO_Cubef
          iterate)

  (define-binding (do-callback FOO_DoCallback)
    safe: #t
    return: float
    args: (((function float (float)) cb)
           (float x)))

  (define-binding** do-callback2
    safe: #t
    return: float
    args: (((function float (float)) cb)
           (float x))
    "C_return(cb(x));")

  (define squaref-ptr
    (foreign-value "FOO_Squaref" (function float (float))))

  (define-callback (cubef-ptr FOO_Cubef)
    return: float
    args: ((float y))
    (* y y y))

  (define-callback iterate-adapter
    args: ((int i)
           (c-pointer userdata))
    (let ((proc (gc-root-ref userdata)))
      (proc i)))

  (define (iterate loops proc)
    (define-binding FOO_Iterate
      safe: #t
      args: ((int loops)
             ((function void (int c-pointer)) callback)
             (c-pointer userdata)))
    (call-with-gc-root proc
      (lambda (proc-root)
        (FOO_Iterate loops iterate-adapter proc-root))))


  (export event-callback-set!
          run-event-callbacks)

  (define event-callbacks
    (let ((count (foreign-value FOO_EVENT_TYPE_COUNT int)))
      (make-vector count #f)))

  (define-callback event-callback-adapter
    args: ((FOO_Event* event))
    (let* ((type-int (event-type-int-getter event))
           (proc (vector-ref event-callbacks type-int)))
      (when proc
        (proc event))))

  (define (event-callback-set! type proc)
    (define-binding FOO_SetEventCallback
      args: ((int type)
             ((function void (FOO_Event*)) callback)))
    (let ((type-int (event-type->int type)))
      (vector-set! event-callbacks type-int proc)
      (FOO_SetEventCallback type-int event-callback-adapter)))


  (define (run-event-callbacks array)
    (define-binding FOO_RunEventCallbacks
      safe: #t
      args: (((nonnull-c-pointer "FOO_Event") array)
             (int size)))
    (FOO_RunEventCallbacks (unwrap-event-array array)
                           (event-array-length array)))

  ) ; end module libfoo
