
/* Example library for tests to bind to. */


#define FOO_VERSION 110

#define FOO_BAR  "Bar!"
#define FOO_BUZZ "Buzz!"

const float FOO_PHI = 2.718281828459045;
const float FOO_PI  = 3.141592653589793;

#define FOO_SECRET1 1357
#define FOO_SECRET2 2468

typedef enum {
  FOO_BLEND_NONE = 0,
  FOO_BLEND_ADD = 1,
  FOO_BLEND_SUB = 2,
  FOO_BLEND_MUL = 4
} FOO_BlendMode;


typedef enum {
  FOO_POWER_EMPTY = 0,
  FOO_POWER_NONE = 0, /* alias */
  FOO_POWER_LOW = 1,
  FOO_POWER_HIGH,
  FOO_POWER_FULL = 9,
  FOO_POWER_WIRED = 10
} FOO_PowerLevel;


typedef enum {
  FOO_EVENT_TYPE_NONE = 0,
  FOO_EVENT_TYPE_KEY,
  FOO_EVENT_TYPE_SENSOR,
  FOO_EVENT_TYPE_COUNT
} FOO_EventType;

typedef enum {
  FOO_KEY_A = 1,
  FOO_KEY_B,
  FOO_KEY_C,
  FOO_KEY_Z = 26
} FOO_KeySym;

typedef enum {
  FOO_KMOD_NONE   = 0b0000,
  FOO_KMOD_LCTRL  = 0b0001,   /* left Ctrl key */
  FOO_KMOD_RCTRL  = 0b0010,   /* right Ctrl key */
  FOO_KMOD_CTRL   = 0b0011,   /* left or right Ctrl key */
  FOO_KMOD_LSHIFT = 0b0100,   /* left Shift key */
  FOO_KMOD_RSHIFT = 0b1000,   /* right Shift key */
  FOO_KMOD_SHIFT  = 0b1100    /* left or right Shift key */
} FOO_KeyMod;


typedef struct {
  char x;
} FOO_Simplum;


/* Private struct (would usually not be defined in header) */
typedef struct {
  FOO_PowerLevel power;
} _FOO_Blackbox;

/* Opaque struct pointer */
typedef _FOO_Blackbox* FOO_Blackbox;


typedef struct {
  int type;
  FOO_KeySym sym;
  FOO_KeyMod mods;
} FOO_KeyEvent;

typedef struct {
  int type;
  int id;
  float value;
} FOO_SensorEvent;

/* Union of two structs. */
typedef union {
  int type;
  FOO_KeyEvent key;
  FOO_SensorEvent sensor;
} FOO_Event;


/* Function-style macro */
#define FOO_ADD1(x) ((x) + 1)


/* A function with some unknown behavior to create a FOO_Blackbox.
 * Maybe it allocates a child struct or something.
 */
FOO_Blackbox FOO_CreateBlackbox() {
  FOO_Blackbox bb = malloc(sizeof(_FOO_Blackbox));
  bb->power = FOO_POWER_HIGH;
  return bb;
}

/* A function with some unknown behavior to clean up a FOO_Blackbox.
 *  Maybe it frees a child struct pointer or something.
 */
void FOO_DestroyBlackbox(FOO_Blackbox bb) {
  if (NULL == bb) { return; }
  free(bb);
}

FOO_PowerLevel FOO_BlackboxPower(FOO_Blackbox bb) {
  return bb->power;
}


FOO_Event* FOO_CreateKeyEvent(FOO_KeySym sym, FOO_KeyMod mods) {
  FOO_Event* ev = malloc(sizeof(FOO_Event));
  ev->type = FOO_EVENT_TYPE_KEY;
  ev->key.sym = sym;
  ev->key.mods = mods;
  return ev;
}

FOO_Event* FOO_CreateSensorEvent(int id, float value) {
  FOO_Event* ev = malloc(sizeof(FOO_Event));
  ev->type = FOO_EVENT_TYPE_SENSOR;
  ev->sensor.id = id;
  ev->sensor.value = value;
  return ev;
}


/* Populate `array_out` with at most `size` events. Returns the actual
 * number of events that were populated.
 */
int FOO_PopulateEvents(int size, FOO_Event* array_out) {
  FOO_Event* temp;
  int i = 0;

  /* Zero out the array */
  memset(array_out, 0, sizeof(FOO_Event[size]));

  /* Simulate that 4 events exist. */
  for (; i < 4 && i < size; i++) {
    if (0 == i % 2) {
      temp = FOO_CreateKeyEvent(1 + i % 3, i % 6);
    } else {
      temp = FOO_CreateSensorEvent(i, i * 1.5);
    }
    array_out[i] = *temp;
    free(temp);
  }

  return i;
}

FOO_Event* FOO_GetEvents(int size) {
  FOO_Event* arr = calloc(sizeof(FOO_Event), size);
  FOO_PopulateEvents(size, arr);
  return arr;
}

int FOO_EventArray_GetType(FOO_Event* array, int i) {
  return array[i].type;
}


typedef void (*FOO_EventCallback)(FOO_Event *event);

FOO_EventCallback __FOO_EventCallbacks[FOO_EVENT_TYPE_COUNT] = {};

/* Callback without userdata pointer. */
void FOO_SetEventCallback(FOO_EventType type, FOO_EventCallback callback) {
  if (type < 0 || FOO_EVENT_TYPE_COUNT <= type) return;
  __FOO_EventCallbacks[type] = callback;
}


void FOO_RunEventCallbacks(FOO_Event* array, int size) {
  for (int i=0; i < size; i++) {
    FOO_Event *ev = &array[i];
    FOO_EventCallback callback = __FOO_EventCallbacks[ev->type];
    if (NULL != callback) {
      callback(ev);
    }
  }
}


typedef void (*FOO_IterateCallback)(int i, void *userdata);

void FOO_Iterate(int loops, FOO_IterateCallback callback, void *userdata) {
  for (int i=0; i < loops; i++) {
    callback(i, userdata);
  }
}


float FOO_DoCallback(float(*cb)(float), float x) {
  return cb(x);
}

float FOO_Squaref(float y) {
  return y*y;
}
