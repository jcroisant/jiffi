
(test-group "define-array-allocators"
  (test-group "free"
    (test-assert "Nullifies the array"
      (let ((subject (make-event-array 2)))
        (free-event-array! subject)
        (armor-null? subject)))

    (test-assert "Returns the array"
      (let ((subject (make-event-array 3)))
        (eq? subject
             (free-event-array! subject))))

    (test-error "Signals error if given wrong array type"
      (eval '(begin
               (import libfoo)
               (free-event-array!
                (make-simplum-array/autofree 4))))))

  (test-group "alloc"
    (test-assert "Returns a tagged pointer"
      (let ((subject (alloc-event-array 1)))
        (begin0
         (tagged-pointer? subject 'event-array)
         (free subject))))

    (test-assert "The pointer memory is zeroed out"
      (let ((length 25))
        (bytes-zeroed?
         (alloc-event-array length)
         (* length sizeof-FOO_Event)))))

  (test-group "alloc/blob"
    (test-assert "Returns a blob"
      (blob? (alloc-event-array/blob 13)))

    (test "The blob is the correct size"
          (* 13 sizeof-FOO_Event)
          (blob-size (alloc-event-array/blob 13)))

    (test-assert "The blob is zeroed out"
      (let ((length 13))
        (bytes-zeroed?
         (make-locative (alloc-event-array/blob 13))
         (* length sizeof-FOO_Event)))))

  (test-group "make"
    (test-assert "Returns an array of the correct type"
      (let ((subject (make-event-array 6)))
        (begin0
         (event-array? subject)
         (free-event-array! subject))))

    (test-assert "The array wraps a pointer"
      (let ((subject (make-event-array 7)))
        (begin0
         (tagged-pointer? (%jiffi:armor-data subject) 'event-array)
         (free-event-array! subject))))

    (test-assert "The pointer memory is zeroed out"
      (let* ((length 9)
             (subject (make-event-array length)))
        (begin0
         (bytes-zeroed?
          (unwrap-event-array subject)
          (* length sizeof-FOO_Event))
         (free-event-array! subject))))

    (test "Sets the array's length"
          7
          (let ((subject (make-event-array 7)))
            (begin0
             (event-array-length subject)
             (free-event-array! subject))))

    (test "Sets additional slots to default values"
          '("a1" a2)
          (let ((subject (make-event-array 9)))
            (begin0
             (list (event-array-data1 subject)
                   (event-array-data2 subject))
             (free-event-array! subject)))))

  (test-group "make/autofree"
    (test-assert "Returns an array of the correct type"
      (event-array? (make-event-array/autofree 4)))

    (test-assert "The array wraps a tagged pointer"
      (let ((subject (make-event-array/autofree 4)))
        (tagged-pointer? (%jiffi:armor-data subject) 'event-array)))

    (test-assert "The pointer memory is zeroed out"
      (let ((length 9))
        (bytes-zeroed?
         (unwrap-event-array (make-event-array/autofree length))
         (* length sizeof-FOO_Event))))

    (test "Sets the array's length"
          4
          (let ((subject (make-event-array/autofree 4)))
            (begin0
             (event-array-length subject)
             (free-event-array! subject))))

    (test "Sets additional slots to default values"
          '("a1" a2)
          (let ((subject (make-event-array/autofree 4)))
            (list (event-array-data1 subject)
                  (event-array-data2 subject)))))

  (test-group "make/blob"
    (test-assert "Returns an array of the correct type"
      (event-array? (make-event-array/blob 7)))

    (test-assert "The array wraps a blob"
      (blob? (%jiffi:armor-data (make-event-array/blob 7))))

    (test "The blob is the correct size"
          (* 7 sizeof-FOO_Event)
          (blob-size (%jiffi:armor-data (make-event-array/blob 7))))

    (test-assert "The blob is zeroed out"
      (let ((length 7))
        (bytes-zeroed?
         (unwrap-event-array (make-event-array/blob length))
         (* length sizeof-FOO_Event))))

    (test "Sets the array's length"
          7
          (let ((subject (make-event-array/blob 7)))
            (begin0
             (event-array-length subject)
             (free-event-array! subject))))

    (test "Sets additional slots to default values"
          '("a1" a2)
          (let ((subject (make-event-array/blob 7)))
            (list (event-array-data1 subject)
                  (event-array-data2 subject))))))


(test-group "define-array-accessors"
  (test-group "array item getter (ref)"
    (test "Returns the correct struct type"
          '(#t #t)
          (let ((arr (make-event-array/autofree 5)))
            (list (event? (event-array-ref arr 0))
                  (event? (event-array-ref arr 4)))))

    (test-assert "Struct is a child of the array"
      (let ((arr (make-event-array/autofree 1)))
        (eq? arr (%jiffi:armor-parent (event-array-ref arr 0)))))

    (test "Struct points to correct address (pointer)"
          (list 1234
                (+ 1234 (* 4 sizeof-FOO_Event)))
          (let ((arr (wrap-event-array (address->pointer 1234) 5)))
            (list (armor-address (event-array-ref arr 0))
                  (armor-address (event-array-ref arr 4)))))

    (let* ((blob (object-evict (alloc-event-array/blob 5)))
           (loc (make-locative blob)))
      (test "Struct points to correct address (locative)"
            (list (pointer->address loc)
                  (+ (pointer->address loc)
                     (* 4 sizeof-FOO_Event)))
            (let ((arr (wrap-event-array loc 5)))
              (list (armor-address (event-array-ref arr 0))
                    (armor-address (event-array-ref arr 4))))))

    (let ((blob (object-evict (alloc-event-array/blob 5))))
      (test "Struct points to correct address (blob)"
            (list (pointer->address (make-locative blob))
                  (+ (pointer->address (make-locative blob))
                     (* 4 sizeof-FOO_Event)))
            (let ((arr (wrap-event-array blob 5)))
              (list (armor-address (event-array-ref arr 0))
                    (armor-address (event-array-ref arr 4))))))

    (test "Structs are readable (populate-events!)"
          '("#<key-event a mods: ()>"
            "#<sensor-event id: 1 value: 1.5>"
            "#<key-event c mods: (rctrl)>"
            "#<sensor-event id: 3 value: 4.5>"
            "#<event none>"
            "#<event none>"
            "#<event none>")
          (let* ((count 7)
                 (evs (make-event-array/autofree count)))
            (populate-events! count evs)
            (map (lambda (i)
                   (let ((ev (event-array-ref evs i)))
                     (case (event-type ev)
                       ((key)    (sprintf "~A" (event->key-event ev)))
                       ((sensor) (sprintf "~A" (event->sensor-event ev)))
                       (else     (sprintf "~A" ev)))))
                 (iota (event-array-length evs)))))

    (test "Structs are readable (get-events)"
          '("#<key-event a mods: ()>"
            "#<sensor-event id: 1 value: 1.5>"
            "#<key-event c mods: (rctrl)>")
          (let ((evs (get-events 3)))
            (map (lambda (i)
                   (let ((ev (event-array-ref evs i)))
                     (case (event-type ev)
                       ((key)    (sprintf "~A" (event->key-event ev)))
                       ((sensor) (sprintf "~A" (event->sensor-event ev)))
                       (else     (sprintf "~A" ev)))))
                 (iota (event-array-length evs)))))

    (test "Modifying struct modifies the array memory"
          '(sensor key none)
          (let* ((evs (make-event-array/blob 3)))
            (set! (event-type (event-array-ref evs 0)) 'sensor)
            (set! (event-type (event-array-ref evs 1)) 'key)
            (set! (event-type (event-array-ref evs 2)) 'none)
            (list (event-array-get-type evs 0)
                  (event-array-get-type evs 1)
                  (event-array-get-type evs 2))))

    (let ((n 1000))
      ;; Two threads each get N items, so the final children count
      ;; should be 2*N. But if the event type is not thread-safe, a
      ;; race condition will cause the count to be less.
      (define (race-cond-check make-arr arr-ref free-arr!)
        (let* ((size 10)
               (arr (make-arr size))
               ;; Prevent the items from being GC'd, which could affect
               ;; the final children count.
               (items1 '())
               (items2 '())
               (thread1
                (make-thread
                 (lambda ()
                   (do ((i 0 (add1 i)))
                       ((= i n))
                     (set! items1
                           (cons (arr-ref arr (modulo i size))
                                 items1))))))
               (thread2
                (make-thread
                 (lambda ()
                   (do ((i 0 (add1 i)))
                       ((= i n))
                     (set! items2
                           (cons (arr-ref arr (modulo i size))
                                 items2)))))))
          ;; Verify assumptions
          (assert (= 0 (armor-kids-count arr)))

          (thread-start! thread1)
          (thread-start! thread2)
          (thread-join! thread2)
          (thread-join! thread1)

          (begin0
           (armor-kids-count arr)
           (free-arr! arr))))

      (test-assert "May have a race condition if array is not thread-safe"
        (>= (* 2 n)
            (race-cond-check make-event-array
                             event-array-ref
                             free-event-array!)
            n))

      (test "No race condition if array is thread-safe"
            (* 2 n)
            (race-cond-check make-safe-array
                             safe-array-ref
                             free-safe-array!)))

    (test-error "Error if is index too low"
      (event-array-ref
       (make-event-array/autofree 5)
       -1))

    (test-error "Error if is index too high"
      (event-array-ref
       (make-event-array/autofree 5)
       5)))


  (test-group "array item setter (set)"
    (define struct-makers
      `(("struct with ptr" . ,make-event/autofree)
        ("struct with blob" . ,make-event/blob)
        ("struct with loc" . ,(lambda ()
                                (wrap-event
                                 (make-locative
                                  (alloc-key-event/blob)))))
        ("bare struct ptr" . ,alloc-event)
        ("bare struct blob" . ,alloc-event/blob)
        ("bare struct loc" . ,(lambda ()
                                (make-locative
                                 (alloc-key-event/blob))))))

    (define array-makers
      `(("array with ptr" . ,make-event-array/autofree)
        ("array with blob" . ,make-event-array/blob)
        ("array with loc" . ,(lambda (count)
                               (wrap-event-array
                                (make-locative
                                 (alloc-event-array/blob count))
                                count)))))

    (test-assert "Copies struct data to array (set: (setter REF))"
      (begin
        (for-each
         (lambda (struct-maker)
           (for-each
            (lambda (array-maker)
              (assert
               (string=?
                "#<key-event b mods: (lctrl)>"
                (let ((arr ((cdr array-maker) 3))
                      (ev ((cdr struct-maker))))
                  (set! (key-event-sym (unwrap-event ev)) 'b)
                  (set! (key-event-mods (unwrap-event ev)) '(lctrl))
                  (set! (event-array-ref arr 2) ev)
                  (sprintf "~A" (event->key-event
                                 (event-array-ref arr 2)))))
               (sprintf "Using ~A, ~A"
                        (car struct-maker)
                        (car array-maker))))
            array-makers))
         struct-makers)
        #t))

    (test "Copies struct data to array (set: SET)"
          "#<key-event b mods: (lctrl)>"
          (let ((arr (make-event-array/blob 3))
                (ev (make-event/blob)))
            (set! (key-event-sym (unwrap-event ev)) 'b)
            (set! (key-event-mods (unwrap-event ev)) '(lctrl))
            (event-array-set! arr 2 ev)
            (sprintf "~A" (event->key-event
                           (event-array-ref arr 2)))))

    (test "Doesn't modify the struct"
          "#<key-event b mods: (lctrl)>"
          (let ((arr (make-event-array/blob 3))
                (ev (make-event/blob)))
            (set! (key-event-sym (unwrap-event ev)) 'b)
            (set! (key-event-mods (unwrap-event ev)) '(lctrl))
            (set! (event-array-ref arr 2) ev)
            (sprintf "~A" (event->key-event ev))))

    (test "Can overwrite with another item from the same array"
          "#<sensor-event id: 1 value: 1.5>"
          (let ((arr (get-events 3)))
            (set! (event-array-ref arr 0)
                  (event-array-ref arr 1))
            (begin0
             (sprintf "~A" (event->sensor-event
                            (event-array-ref arr 0)))
             (free-event-array! arr))))

    (test "Can overwrite with the same item"
          "#<sensor-event id: 1 value: 1.5>"
          (let ((arr (get-events 3)))
            (set! (event-array-ref arr 1)
                  (event-array-ref arr 1))
            (begin0
             (sprintf "~A" (event->sensor-event
                            (event-array-ref arr 1)))
             (free-event-array! arr))))

    (test-error "Error if is index too low"
      (set! (event-array-ref (make-event-array/blob 5) -1)
            (make-event/blob)))

    (test-error "Error if is index too high"
      (set! (event-array-ref (make-event-array/blob 5) 5)
            (make-event/blob))))


  (test-group "array item mapper (map)"
    (let* ((events (get-events 4))
           (addr (armor-address events))
           (size sizeof-FOO_Event))
      (test "Maps proc with each index and array item"
            (list (list 0 #t (+ addr (* 0 size)))
                  (list 1 #t (+ addr (* 1 size)))
                  (list 2 #t (+ addr (* 2 size)))
                  (list 3 #t (+ addr (* 3 size))))
            (begin0
             (event-array-map
              (lambda (i ev)
                (list i (event? ev) (armor-address ev)))
              events)
             (free-event-array! events))))

    (let* ((events1 (get-events 4))
           (events2 (get-events 7))
           (events3 (get-events 2))
           (addr1 (armor-address events1))
           (addr2 (armor-address events2))
           (addr3 (armor-address events3))
           (size sizeof-FOO_Event))
      (test "Stops when smallest array is exhausted"
            (list (list 0
                        #t (+ addr1 (* 0 size))
                        #t (+ addr2 (* 0 size))
                        #t (+ addr3 (* 0 size)))
                  (list 1
                        #t (+ addr1 (* 1 size))
                        #t (+ addr2 (* 1 size))
                        #t (+ addr3 (* 1 size))))
            (begin0
             (event-array-map
              (lambda (i ev1 ev2 ev3)
                (list i
                      (event? ev1) (armor-address ev1)
                      (event? ev2) (armor-address ev2)
                      (event? ev3) (armor-address ev3)))
              events1 events2 events3)
             (free-event-array! events1)
             (free-event-array! events2)
             (free-event-array! events3))))

    (test "Items are valid even after mapping ends"
          '("#<event key>"
            "#<event sensor>"
            "#<event key>"
            "#<event sensor>"
            "#<event none>")
          (let* ((events (get-events 5))
                 (results (event-array-map
                           (lambda (i ev) ev)
                           events)))
            (begin0
             (map (cut sprintf "~A" <>) results)
             (free-event-array! events))))

    (test "Items are children of the array"
          '(#t #t #t)
          (let* ((events (get-events 3)))
            (begin0
             (event-array-map
              (lambda (i ev)
                (eq? events (%jiffi:armor-parent ev)))
              events)
             (free-event-array! events))))

    (let ((n 1000))
      ;; Two threads each get N items, so the final children count
      ;; should be 2*N. But if the event type is not thread-safe, a
      ;; race condition will cause the count to be less.
      (define (race-cond-check make-arr arr-map free-arr!)
        (let* ((arr (make-arr n))
               ;; Prevent the items from being GC'd, which could affect
               ;; the final children count.
               (items1 '())
               (items2 '())
               (thread1
                (make-thread
                 (lambda ()
                   (set! items1 (arr-map (lambda (i item) item) arr)))))
               (thread2
                (make-thread
                 (lambda ()
                   (set! items2 (arr-map (lambda (i item) item) arr))))))
          ;; Verify assumptions
          (assert (= 0 (armor-kids-count arr)))

          (thread-start! thread1)
          (thread-start! thread2)
          (thread-join! thread2)
          (thread-join! thread1)

          (begin0
           (armor-kids-count arr)
           (free-arr! arr))))

      (test-assert "May have a race condition if array is not thread-safe"
        (>= (* 2 n)
            (race-cond-check make-event-array
                             event-array-map
                             free-event-array!)
            n))

      (test "No race condition if array is thread-safe"
            (* 2 n)
            (race-cond-check make-safe-array
                             safe-array-map
                             free-safe-array!))))


  (test-group "array item iterator (for-each)"
    (let* ((events (get-events 4))
           (addr (armor-address events))
           (size sizeof-FOO_Event))
      (test "Calls proc with each index and array item"
            (list (list 0 #t (+ addr (* 0 size)))
                  (list 1 #t (+ addr (* 1 size)))
                  (list 2 #t (+ addr (* 2 size)))
                  (list 3 #t (+ addr (* 3 size))))
            (let ((results '()))
              (event-array-for-each
               (lambda (i ev)
                 (set! results
                       (cons
                        (list i (event? ev) (armor-address ev))
                        results)))
               events)
              (begin0
               (reverse results)
               (free-event-array! events)))))

    (let* ((events1 (get-events 4))
           (events2 (get-events 7))
           (events3 (get-events 2))
           (addr1 (armor-address events1))
           (addr2 (armor-address events2))
           (addr3 (armor-address events3))
           (size sizeof-FOO_Event))
      (test "Stops when smallest array is exhausted"
            (list (list 0
                        #t (+ addr1 (* 0 size))
                        #t (+ addr2 (* 0 size))
                        #t (+ addr3 (* 0 size)))
                  (list 1
                        #t (+ addr1 (* 1 size))
                        #t (+ addr2 (* 1 size))
                        #t (+ addr3 (* 1 size))))
            (let ((results '()))
              (event-array-for-each
               (lambda (i ev1 ev2 ev3)
                 (set! results
                       (cons
                        (list i
                              (event? ev1) (armor-address ev1)
                              (event? ev2) (armor-address ev2)
                              (event? ev3) (armor-address ev3))
                        results)))
               events1 events2 events3)
              (begin0
               (reverse results)
               (free-event-array! events1)
               (free-event-array! events2)
               (free-event-array! events3)))))

    (test "Items are nullified after mapping ends"
          '(#t #t #t #t #t)
          (let* ((events (get-events 5))
                 (results '()))
            (event-array-for-each
             (lambda (i ev)
               (set! results (cons ev results)))
             events)
            (begin0
             (map armor-null? results)
             (free-event-array! events))))

    (test "Items are children of the array"
          '(#t #t #t)
          (let* ((events (get-events 3))
                 (results '()))
            (event-array-for-each
             (lambda (i ev)
               (set! results
                     (cons
                      (eq? events (%jiffi:armor-parent ev))
                      results)))
             events)
            (free-event-array! events)
            results))

    (test "Docs example"
          '(0 key sensor 1 key sensor 2 key sensor)
          (let ((array1 (make-event-array 20))
                (array2 (make-event-array 3))
                (results '()))
            (event-array-for-each
             (lambda (i event1 event2)
               (set! (event-type event1) 'key)
               (set! (event-type event2) 'sensor)
               (set! results
                     (append results
                             (list i
                                   (event-type event1)
                                   (event-type event2)))))
             array1
             array2)
            results)))


  (test-group "array item pointer getter (ref*)"
    (test "Returns a tagged pointer or locative"
          '(#t #t)
          (let ((ptr-arr (make-event-array/autofree 5))
                (blob-arr (make-event-array/blob 5)))
            (list (tagged-pointer? (event-array-ref* ptr-arr 0) 'event)
                  (locative? (event-array-ref* blob-arr 4)))))

    (test "Pointer points to correct address (pointer)"
          (list 1234
                (+ 1234 (* 4 sizeof-FOO_Event)))
          (let ((arr (wrap-event-array (address->pointer 1234) 5)))
            (list (pointer->address (event-array-ref* arr 0))
                  (pointer->address (event-array-ref* arr 4)))))

    (let* ((blob (object-evict (alloc-event-array/blob 5)))
           (loc (make-locative blob)))
      (test "Locative points to correct address (locative)"
            (list (pointer->address loc)
                  (+ (pointer->address loc)
                     (* 4 sizeof-FOO_Event)))
            (let ((arr (wrap-event-array loc 5)))
              (list (pointer->address (event-array-ref* arr 0))
                    (pointer->address (event-array-ref* arr 4))))))

    (let ((blob (object-evict (alloc-event-array/blob 5))))
      (test "Locative points to correct address (blob)"
            (list (pointer->address (make-locative blob))
                  (+ (pointer->address (make-locative blob))
                     (* 4 sizeof-FOO_Event)))
            (let ((arr (wrap-event-array blob 5)))
              (list (pointer->address (event-array-ref* arr 0))
                    (pointer->address (event-array-ref* arr 4))))))

    (test "Pointers are readable (populate-events!)"
          '("#<key-event a mods: ()>"
            "#<sensor-event id: 1 value: 1.5>"
            "#<key-event c mods: (rctrl)>"
            "#<sensor-event id: 3 value: 4.5>"
            "#<event none>"
            "#<event none>"
            "#<event none>")
          (let* ((count 7)
                 (evs (make-event-array/autofree count)))
            (populate-events! count evs)
            (map (lambda (i)
                   (let ((ev (event-array-ref* evs i)))
                     (case (event-type ev)
                       ((key)    (sprintf "~A" (wrap-key-event ev)))
                       ((sensor) (sprintf "~A" (wrap-sensor-event ev)))
                       (else     (sprintf "~A" (wrap-event ev))))))
                 (iota (event-array-length evs)))))

    (test "Pointers are readable (get-events)"
          '("#<key-event a mods: ()>"
            "#<sensor-event id: 1 value: 1.5>"
            "#<key-event c mods: (rctrl)>")
          (let ((evs (get-events 3)))
            (map (lambda (i)
                   (let ((ev (event-array-ref* evs i)))
                     (case (event-type ev)
                       ((key)    (sprintf "~A" (wrap-key-event ev)))
                       ((sensor) (sprintf "~A" (wrap-sensor-event ev)))
                       (else     (sprintf "~A" (wrap-event ev))))))
                 (iota (event-array-length evs)))))

    (test "Modifying pointer modifies the array memory"
          '(sensor key none)
          (let* ((evs (make-event-array/blob 3)))
            (set! (event-type (event-array-ref* evs 0)) 'sensor)
            (set! (event-type (event-array-ref* evs 1)) 'key)
            (set! (event-type (event-array-ref* evs 2)) 'none)
            (list (event-array-get-type evs 0)
                  (event-array-get-type evs 1)
                  (event-array-get-type evs 2))))

    (test-error "Error if is index too low"
      (event-array-ref*
       (make-event-array/autofree 5)
       -1))

    (test-error "Error if is index too high"
      (event-array-ref*
       (make-event-array/autofree 5)
       5)))


  (test-group "array item pointer iterator (map*)"
    (let* ((events (get-events 4))
           (addr (armor-address events))
           (size sizeof-FOO_Event))
      (test "Maps proc with each index and item pointer"
            (list (list 0 (+ addr (* 0 size)))
                  (list 1 (+ addr (* 1 size)))
                  (list 2 (+ addr (* 2 size)))
                  (list 3 (+ addr (* 3 size))))
            (begin0
             (event-array-map*
              (lambda (i ptr)
                (assert (tagged-pointer? ptr) 'event)
                (list i (pointer->address ptr)))
              events)
             (free-event-array! events))))

    (let* ((events1 (get-events 4))
           (events2 (get-events 7))
           (events3 (get-events 2))
           (addr1 (armor-address events1))
           (addr2 (armor-address events2))
           (addr3 (armor-address events3))
           (size sizeof-FOO_Event))
      (test "Stops when smallest array is exhausted"
            (list (list 0
                        (+ addr1 (* 0 size))
                        (+ addr2 (* 0 size))
                        (+ addr3 (* 0 size)))
                  (list 1
                        (+ addr1 (* 1 size))
                        (+ addr2 (* 1 size))
                        (+ addr3 (* 1 size))))
            (begin0
             (event-array-map*
              (lambda (i ptr1 ptr2 ptr3)
                (list i
                      (pointer->address ptr1)
                      (pointer->address ptr2)
                      (pointer->address ptr3)))
              events1 events2 events3)
             (free-event-array! events1)
             (free-event-array! events2)
             (free-event-array! events3)))))


  (test-group "array item pointer iterator (for-each*)"
    (let* ((events (get-events 4))
           (addr (armor-address events))
           (size sizeof-FOO_Event))
      (test "Calls proc with each index and item pointer"
            (list (list 0 (+ addr (* 0 size)))
                  (list 1 (+ addr (* 1 size)))
                  (list 2 (+ addr (* 2 size)))
                  (list 3 (+ addr (* 3 size))))
            (let ((results '()))
              (event-array-for-each*
               (lambda (i ptr)
                 (assert (tagged-pointer? ptr 'event))
                 (set! results
                       (cons
                        (list i (pointer->address ptr))
                        results)))
               events)
              (begin0
               (reverse results)
               (free-event-array! events)))))

    (let* ((events1 (get-events 4))
           (events2 (get-events 7))
           (events3 (get-events 2))
           (addr1 (armor-address events1))
           (addr2 (armor-address events2))
           (addr3 (armor-address events3))
           (size sizeof-FOO_Event))
      (test "Stops when smallest array is exhausted"
            (list (list 0
                        (+ addr1 (* 0 size))
                        (+ addr2 (* 0 size))
                        (+ addr3 (* 0 size)))
                  (list 1
                        (+ addr1 (* 1 size))
                        (+ addr2 (* 1 size))
                        (+ addr3 (* 1 size))))
            (let ((results '()))
              (event-array-for-each*
               (lambda (i ptr1 ptr2 ptr3)
                 (set! results
                       (cons
                        (list i
                              (pointer->address ptr1)
                              (pointer->address ptr2)
                              (pointer->address ptr3))
                        results)))
               events1 events2 events3)
              (begin0
               (reverse results)
               (free-event-array! events1)
               (free-event-array! events2)
               (free-event-array! events3)))))))


(test-group "define-array-copy!"
  (define (build-simplum-array . xs)
    (let ((array (make-simplum-array/autofree (length xs))))
      (simplum-array-for-each
       (lambda (i simplum)
         (simplum-x-set! simplum (list-ref xs i)))
       array)
      array))

  (define (make-to) (build-simplum-array 90 91 92 93))
  (define (make-from) (build-simplum-array 94 95 96))
  (define (get-x i simplum) (simplum-x simplum))

  (test "Does not modify `from`"
        '(94 95 96)
        (let ((to (make-to))
              (from (make-from)))
          (simplum-array-copy! to 0 from)
          (simplum-array-map get-x from)))

  (test "Copies all of `from` by default"
        '(94 95 96 93)
        (let ((to (make-to))
              (from (make-from)))
          (simplum-array-copy! to 0 from)
          (simplum-array-map get-x to)))

  (test "`at` can be greater than 0"
        '(90 94 95 96)
        (let ((to (make-to))
              (from (make-from)))
          (simplum-array-copy! to 1 from)
          (simplum-array-map get-x to)))

  (test-error "Error if `at` is negative"
    (let ((to (make-to))
          (from (make-from)))
      (simplum-array-copy! to -1 from)))

  (test "Can change `start`"
        '(95 96 92 93)
        (let ((to (make-to))
              (from (make-from)))
          (simplum-array-copy! to 0 from 1)
          (simplum-array-map get-x to)))

  (test-error "Error if `start` is negative"
    (let ((to (make-to))
          (from (make-from)))
      (simplum-array-copy! to 0 from -1)))

  (test "Can change `end`"
        '(95 91 92 93)
        (let ((to (make-to))
              (from (make-from)))
          (simplum-array-copy! to 0 from 1 2)
          (simplum-array-map get-x to)))

  (test-error "Error if `end` >= length of `from`"
    (let ((to (make-to))
          (from (make-from)))
      (simplum-array-copy! to 0 from 0 4)))

  (test-error "Error if `start` > `end`"
    (let ((to (make-to))
          (from (make-from)))
      (simplum-array-copy! to 0 from 2 1)))

  (test-error "Error if `to` is too short"
    (let ((to (make-to))
          (from (make-from)))
      (simplum-array-copy! to 2 from)))

  (test "Same array, non-overlapping, at <= start"
        '(92 93 92 93)
        (let ((to (make-to)))
          (simplum-array-copy! to 0 to 2 4)
          (simplum-array-map get-x to)))

  (test "Same array, non-overlapping, at > start"
        '(90 91 90 91)
        (let ((to (make-to)))
          (simplum-array-copy! to 2 to 0 2)
          (simplum-array-map get-x to)))

  (test "Same array, overlapping, at <= start"
        '(91 92 93 93)
        (let ((to (make-to)))
          (simplum-array-copy! to 0 to 1 4)
          (simplum-array-map get-x to)))

  (test "Same array, overlapping, at > start"
        '(90 90 91 93)
        (let ((to (make-to)))
          (simplum-array-copy! to 1 to 0 2)
          (simplum-array-map get-x to)))

  (test "Can pass bare data"
        '(95 91 92 93)
        (let ((to (make-to))
              (from (make-from)))
          (simplum-array-copy! (unwrap-simplum-array to) 0
                               (unwrap-simplum-array from) 1 2)
          (simplum-array-map get-x to))))
