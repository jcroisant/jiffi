
(test-group "define-foreign-values"
  (test-assert "With consts"
    (and (< 3.141 π 3.142)
         (< 2.718 φ 2.719)))

  (test "With enums"
        '(0 1 2 4)
        (list FOO_BLEND_NONE
              FOO_BLEND_ADD
              FOO_BLEND_SUB
              FOO_BLEND_MUL))

  (test "With #define's"
        '("Bar!" "Buzz!")
        (list FOO_BAR FOO_BUZZ))

  (test "With C expressions"
        '(4.0 5.0 #t)
        (list four
              five
              (< 1.414 sqrt2 1.415)))

  #+(not chicken-4)
  (test-assert "Exports if export: #t"
    (exports? libfoo φ))

  #+(not chicken-4)
  (test-assert "Does not export if export: #f"
    (not (exports? libfoo FOO_SECRET1)))

  #+(not chicken-4)
  (test-assert "Does not export if export clause omitted"
    (not (exports? libfoo FOO_SECRET2))))
