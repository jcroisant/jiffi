(import scheme)

(cond-expand
 (chicken-4
  (import chicken)
  (use files posix system extras lolevel
       test
       srfi-1
       srfi-18
       irregex
       ports
       jiffi))
 (else
  (import (chicken pathname)
          (chicken process-context)
          (chicken base)
          (chicken blob)
          (chicken condition)
          (chicken format)
          (chicken gc)
          (chicken irregex)
          (chicken keyword)
          (chicken locative)
          (chicken memory)
          (chicken memory representation)
          (chicken port)
          (srfi 1)
          (srfi 18)
          object-evict
          compile-file
          test
          jiffi)))


(define tests-dir (pathname-directory (program-name)))
(if tests-dir
    (change-directory tests-dir))


;; Compile and load libfoo.scm
(cond-expand
 (chicken-4
  (define-system jiffi-tests
    (compiled-scheme-file
     "libfoo.scm"
     options: '("-C -I.")))
  (load-system jiffi-tests))
 (else
  (display "  compiling jiffi tests\n")
  (compile-file
   "libfoo.scm"
   load: #t
   options: '("-C -I."))))

(import libfoo)


(define-syntax begin0
  (syntax-rules ()
    ((begin0 BODY-0 BODY ...)
     (let-values ((vals BODY-0))
       BODY ...
       (apply values vals)))))


;; Evaluates to #t if the body signalled an error.
(define-syntax error?
  (syntax-rules ()
    ((error? BODY ...)
     (condition-case (begin BODY ... #f)
       (e (exn)
          #t)))))

;; Evaluates to #t if the body did NOT signal an error.
;; Usage: (test-assert "..." (no-error? body ...))
(define-syntax no-error?
  (syntax-rules ()
    ((no-error? BODY ...)
     (condition-case (begin BODY ... #t)
       (e (exn)
          (print-error-message e)
          #f)))))


;; Evaluates to #t if BODY signals an error whose message matches the
;; irregex PATTERN. Signals an error if BODY does not signal any
;; error, or if it signals an error that does not match PATTERN.
(define-syntax error-matches?
  (syntax-rules ()
    ((error-matches? PATTERN BODY ...)
     (if (eq? 'no-error
              (condition-case (begin BODY ... 'no-error)
                (e (exn)
                   (let ((error-message
                          ((condition-property-accessor 'exn 'message) e)))
                     (if (irregex-search PATTERN error-message)
                         #t
                         (error (sprintf
                                 "expected error matching ~S, but got error"
                                 'PATTERN)
                                error-message))))))
         (error (sprintf
                 "expected error matching ~S, but got NO error"
                 'PATTERN))
         #t))))


;; Evaluates to #t if BODY prints error output matching the irregex
;; PATTERN. Signals an error if BODY does not print any error output,
;; or if it prints error output that does not match PATTERN.
(define-syntax warning-matches?
  (syntax-rules ()
    ((warning-matches? PATTERN BODY ...)
     (let ((err-out-str
            ;; with-error-output-to-string not available on CHICKEN 4
            (call-with-output-string
             (lambda (out-port)
               (with-error-output-to-port
                out-port
                (lambda () BODY ...))))))
       (if (string=? "" err-out-str)
           (error (sprintf
                   "expected warning matching ~S, but got NO error output"
                   'PATTERN)))
       (if (irregex-search PATTERN err-out-str)
           #t
           (error (sprintf
                   "expected warning matching ~S, but got error output"
                   'PATTERN)
                  err-out-str))))))


;; Evaluates to #t if MODULE exports IDENTIFIER.
;; TODO: Implement in chicken 4
(define-syntax exports?
  (syntax-rules ()
    ((not-exported MODULE IDENTIFIER)
     (condition-case
         (begin (eval '(let () (import MODULE) IDENTIFIER))
                #t)
       (e (exn runtime) #f)))))


;; Return #t if all bytes in the buffer are zero.
(define (bytes-zeroed? buf-ptr size)
  (let loop ((i 0))
    (cond
     ((= i size)
      #t)
     ((zero? (pointer-u8-ref (pointer+ buf-ptr i)))
      (loop (add1 i)))
     (else
      #f))))


;; Call proc with the armor's children list, wait for the thread to
;; finish if necessary, and return result.
(define (with-armor-kids armor proc)
  (let* ((lock (%jiffi:armor-lock armor))
         (do-it (lambda () (proc (%jiffi:armor-kids armor))))
         (result (if lock (lock do-it) (do-it))))
    (if (thread? result)
        (thread-join! result)
        result)))

(define (armor-kids-count armor)
  (with-armor-kids armor length))

(define (armor-has-child? armor child)
  (with-armor-kids
   armor
   (lambda (kids)
     (any (lambda (weak-loc)
            (eq? child (locative->object weak-loc)))
          kids))))


(include "test-enum.scm")
(include "test-extract-args.scm")
(include "test-foreign-value.scm")
(include "test-function.scm")
(include "test-gc-root.scm")
(include "test-armor.scm")
(include "test-struct.scm")
(include "test-array.scm")


(test-exit)
