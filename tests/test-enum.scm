
(test-group "define-enum-group"
  (test-group "constants"
    (test "Basic constants"
          '(0 1 2)
          (list FOO_EVENT_TYPE_NONE
                FOO_EVENT_TYPE_KEY
                FOO_EVENT_TYPE_SENSOR))

    (test "Renamed constants"
          '(0 0 1 2 9)
          (list power/empty
                power/none
                power/low
                power/high
                power/full))

    (test "Available conditional constants are defined"
          10
          power/wired)

    #+(not chicken-4)
    (test-assert "Unavailable conditional constants are not defined"
      (not (exports? libfoo power/bla)))

    #+(not chicken-4)
    (test-assert "Are not exported if export-constants: #f"
      (not (exports? libfoo SECRET1))))


  (test-group "symbol->int"
    (test "Converts symbol to expected integer value"
          FOO_EVENT_TYPE_KEY
          (event-type->int 'key))

    (test "Available conditional symbols work"
          power/wired
          (power-level->int 'wired))

    (test-error "Unavailable conditional symbols don't work"
      (power-level->int 'charge))

    (test-error "Signals error if symbol not recognized"
      (power-level->int 'infinite))

    (test "Calls not-found-callback if provided"
          '(foo infinite bar)
          (power-level->int
           'infinite
           (cut list 'foo <> 'bar)))

    (test "Allows integers if allow-ints: #t"
          1234
          (keysym->int 1234))

    (test-error "Does not allows integers if allow-ints: #f"
      (eval '(begin
               (import libfoo)
               (event-type->int 1234))))

    (test "Works even if constants are not exported"
          1357
          (secret1->int 'secret1))

    #+(not chicken-4)
    (test-assert (exports? libfoo secret1->int))

    #+(not chicken-4)
    (test-assert "Is not automatically exported"
      (not (exports? libfoo secret2->int))))


  (test-group "int->symbol"
    (test "Converts integer value to expected symbol"
          'high
          (int->power-level power/high))

    (test "Available conditional values work"
          'wired
          (int->power-level power/wired))

    (test-error "Unavailable conditional values don't work"
      (int->power-level 42))

    (test-error "Signals error if value not recognized"
      (int->power-level 123))

    (test "Calls not-found-callback if provided"
          '(foo 321 bar)
          (int->power-level
           321
           (cut list 'foo <> 'bar)))

    (test "Works even if constants are not exported"
          'secret1
          (int->secret1 1357))

    #+(not chicken-4)
    (test-assert (exports? libfoo int->secret1))

    #+(not chicken-4)
    (test-assert "Is not automatically exported"
      (not (exports? libfoo int->secret2)))))


(test-group "define-enum-packer"
  (test "Returns integers as-is"
        42
        (pack-keymods 42))

  (test "Returns 0 for an empty list"
        0
        (pack-keymods '()))

  (test "Converts and packs a list of recognized symbols"
        #b1101
        (pack-keymods '(lctrl shift)))

  (test "Converts and packs a list of integers and recognized symbols"
        45
        (pack-keymods '(lctrl shift 32)))

  (test "Converts and packs a single recognized symbol"
        #b1100
        (pack-keymods 'shift))

  (test-error "Signals error if symbol in list can't be converted"
    (pack-keymods '(foo)))

  (test "Invokes callback if symbol in list can't be converted"
        17
        (pack-keymods '(lctrl foo) (lambda (sym) 16)))

  (test-error "Signals error if single symbol can't be converted"
    (pack-keymods 'foo))

  (test "Invokes callback if single symbol can't be converted"
        16
        (pack-keymods 'foo (lambda (sym) 16))))


(test-group "define-enum-unpacker"
  (test '()
        (unpack-keymods FOO_KMOD_NONE))
  (test '(lctrl)
        (unpack-keymods FOO_KMOD_LCTRL))
  (test '(lctrl rctrl ctrl)
        (unpack-keymods FOO_KMOD_CTRL)))
