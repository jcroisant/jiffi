
(test-group "define-binding"
  (test "With only Scheme name"
        5
        (FOO_ADD1 4))

  (test "With Scheme name and C name"
        5
        (add-one 4))

  (test "With automatic return value and argument conversion"
        '(c (rctrl lshift) 123 4.5)
        (let ((ke (create-key-event 'c '(rctrl lshift)))
              (se (create-sensor-event 123 4.5)))
          (list (key-event-sym ke)
                (key-event-mods ke)
                (sensor-event-id se)
                (sensor-event-value se))))

  (test "With manual return value and argument conversion"
        '(c (rctrl lshift) 123 4.5)
        (let ((ke (create-key-event/manual 'c '(rctrl lshift)))
              (se (create-sensor-event/manual 123 4.5)))
          (list (key-event-sym ke)
                (key-event-mods ke)
                (sensor-event-id se)
                (sensor-event-value se))))

  (test "With manual return value and argument conversion (bb)"
        'high
        (let ((subject (create-blackbox)))
          (begin0
           (blackbox-power subject)
           (destroy-blackbox! subject))))

  (test "Can define safe lambda"
        9.8596
        (do-callback squaref-ptr 3.14)))


(test-group "define-binding**"
  (test 64.0 (squaref 8.0))
  (test 16 (square-int 4))
  (test 5 (add-three 2))
  (test "Can define safe lambda"
        9.8596
        (do-callback2 squaref-ptr 3.14)))


(test-group "foreign-lambda**"
  (test "With no args"
        "foo"
        (return-foo))

  (test "With a mix of bare strings and format clauses"
        2343
        (do-some-math 123 456))

  (test 42 (return-42)))


(test-group "define-callback"
  (test-assert "Defines variable as a pointer"
    (pointer? cubef-ptr))

  (test "Pointer can be passed as a C callback"
        30.959144
        (do-callback cubef-ptr 3.14))

  (test "Also defines callable Scheme procedure"
        30.959144
        (FOO_Cubef 3.14)))


(test-group "iterator callback with user data"
  (define *results* (make-parameter '()))
  (iterate 100000 (lambda (i) (*results* (cons i (*results*)))))
  (test 100000 (length (*results*))))


(test-group "event callbacks"
  (define *key-events* (make-parameter '()))
  (define (push-key-event event)
    (*key-events* (cons event (*key-events*))))

  (define *sensor-events* (make-parameter '()))
  (define (push-sensor-event event)
    (*sensor-events* (cons event (*sensor-events*))))

  (event-callback-set! 'key push-key-event)
  (event-callback-set! 'sensor push-sensor-event)

  (run-event-callbacks (get-events 3))

  (test "Runs the callbacks as expected"
        '(("#<key-event c mods: (rctrl)>"
           "#<key-event a mods: ()>")
          ("#<sensor-event id: 1 value: 1.5>"))
        (list (map (lambda (ev) (sprintf "~A" (event->key-event ev)))
                   (*key-events*))
              (map (lambda (ev) (sprintf "~A" (event->sensor-event ev)))
                   (*sensor-events*)))))
