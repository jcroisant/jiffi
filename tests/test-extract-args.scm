
(include "../lib/extract-args.scm")

(test-group "extract-args helper"
  (test "Base case"
        '(("head1" "head2")
          ((key1: . "value1")
           (key2: . "default2")
           (key3: . "value3"))
          ("body1" "body2"))
        (receive
            (extract-args
             '(my-macro
               "head1"
               "head2"
               key3: "value3"
               key1: "value1"
               "body1"
               "body2")
             head: 2
             keys: '((key1: required!)
                     (key2: "default2")
                     (key3: "default3"))
             body: #t)))

  (test "Omitted body args"
        '(("head1" "head2")
          ((key1: . "value1")
           (key2: . "default2")
           (key3: . "default3"))
          ())
        (receive
            (extract-args
             '(my-macro
               "head1"
               "head2"
               key1: "value1")
             head: 2
             keys: '((key1: required!)
                     (key2: "default2")
                     (key3: "default3"))
             body: #t)))

  (test-assert "Too few head args"
    (error-matches? "too few arguments before keywords"
      (extract-args
       '(my-macro
         "head1"
         key3: "value3"
         key1: "value1"
         "body1"
         "body2")
       head: 2
       keys: '((key1: required!)
               (key2: "default2")
               (key3: "default3"))
       body: #t)))

  (test-assert "Missing required keyword arg"
    (error-matches? "missing .*keyword .*key1"
      (extract-args
       '(my-macro
         "head1"
         "head2"
         key3: "value3"
         "body1"
         "body2")
       head: 2
       keys: '((key1: required!)
               (key2: "default2")
               (key3: "default3"))
       body: #t)))

  (test-assert "Duplicate keyword arg"
    (warning-matches? "duplicate keyword .*key2.* in form"
      (extract-args
       '(my-macro
         "head1"
         "head2"
         key2: "value2"
         key1: "value1"
         key2: "dupe2"
         "body1"
         "body2")
       head: 2
       keys: '((key1: required!)
               (key2: "default2")
               (key3: "default3"))
       body: #t)))

  (test-assert "Unrecognized keyword arg"
    (warning-matches? "unrecognized keyword .*key4.* in form"
      (extract-args
       '(my-macro
         "head1"
         "head2"
         key1: "value1"
         key4: "value4"
         "body1"
         "body2")
       head: 2
       keys: '((key1: required!)
               (key2: "default2")
               (key3: "default3"))
       body: #t)))

  (test-assert "Body args not allowed"
    (error-matches? "too many arguments"
      (extract-args
       '(my-macro
         "head1"
         "head2"
         key3: "value3"
         key1: "value1"
         "body1")
       head: 2
       keys: '((key1: required!)
               (key2: "default2")
               (key3: "default3"))
       body: #f))))
