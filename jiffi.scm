;;
;; jiffi: CHICKEN Scheme helpers for foreign function interfaces.
;;
;; Copyright © 2021–2022  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(module jiffi ()
  (import scheme)
  (cond-expand
   (chicken-4
    (import chicken foreign)
    (import-for-syntax chicken)
    (use extras lolevel srfi-1)
    (use-for-syntax srfi-1
                    (only irregex irregex-replace/all)
                    (rename (only extras random)
                            (random pseudo-random-integer))))
   (else
    (import (chicken base)
            (only (chicken bitwise)
                  bitwise-and
                  bitwise-ior)
            (only (chicken blob)
                  blob?
                  blob-size
                  make-blob)
            (chicken foreign)
            (chicken format)
            (only (chicken gc)
                  set-finalizer!)
            (only (chicken locative)
                  locative?
                  make-locative
                  make-weak-locative
                  locative->object)
            (only (chicken memory)
                  address->pointer
                  allocate
                  free
                  pointer?
                  pointer->address
                  tag-pointer
                  tagged-pointer?)
            (only (chicken memory representation)
                  make-record-instance
                  record-instance?
                  record-instance-length
                  record-instance-slot
                  record-instance-slot-set!)
            (only (chicken module)
                  export)
            (only (chicken syntax)
                  begin-for-syntax
                  define-for-syntax)
            (chicken type)
            (srfi 1))
    (import-for-syntax (only (chicken format)
                             sprintf)
                       (only (chicken irregex)
                             irregex-replace/all)
                       (only (chicken keyword)
                             keyword?)
                       (only (chicken platform)
                             feature?)
                       (only (chicken random)
                             pseudo-random-integer)
                       (srfi 1))))

  (include "lib/helpers.scm")

  (include "lib/enum.scm")
  (include "lib/foreign-value.scm")
  (include "lib/function.scm")
  (include "lib/gc-root.scm")
  (include "lib/armor.scm")
  (include "lib/struct.scm")
  (include "lib/array.scm")
  )
