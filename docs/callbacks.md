# Callbacks in Jiffi

Some C library functions accept a C function pointer as an argument,
which the libray function calls. This allows the C library user to
implement some custom behavior. Some common examples of this are event
handlers, hooks, and iterators.

This guide explains various techniques for implementing callbacks
using Jiffi. We will start at the lowest level, defining callbacks in
C, then gradually build up to more advanced techniques allowing users
to define callbacks in Scheme, even in interpreted code.

All the techniques in this guide can also be used with CHICKEN's
built-in features such as `foreign-safe-lambda`, `define-external`,
and `CHICKEN_new_gc_root`. Jiffi just makes it a bit easier.

Table of contents:

[[_TOC_]]


## Safe Bindings

When defining a binding to a C library function that invokes a Scheme
callback, you must pass `safe: #t` to
[`define-binding`](https://wiki.call-cc.org/eggref/5/jiffi#define-binding) or
[`define-binding**`](https://wiki.call-cc.org/eggref/5/jiffi#define-binding-2).
This generates special code to save and restore important state
information so that the C function can safely call back into the
Scheme environment.

In this guide we will use the example of the
[freeglut](http://freeglut.sourceforge.net/) library, which uses C
callbacks heavily. There are many function that set various callbacks,
but the callbacks are **invoked** when we call `glutMainLoop`, so that
is the binding that needs to be safe:

```scheme
(define-binding glutMainLoop
  return: void      ; could be omitted, void is the default
  args:   ()        ; could be omitted, () is the default
  safe:   #t)
```

If you are only using callbacks functions written in C, it is not
necessary for the binding to be safe. But if you use any of the other
techniques described in this guide, it must be safe.


## Defining Callbacks in C

The freeglut library has many functions that set a callback, but we
will focus on `glutMouseFunc`, which sets the callback to be invoked
when the user presses or releases a mouse button:

```c
void glutMouseFunc(void (*func)(int button, int state, int x, int y));
```

The C syntax `void (*func)(int button, int state, int x, int y)` means
that `glutMouseFunc` takes one argument (here called `func`, but the
name doesn't matter), which must be a pointer to a `void` procedure
that takes 4 `int` arguments (again, the argument names don't matter).

We can write a straightforward binding to this procedure like so:

```scheme
(define-binding glutMouseFunc
  args: ((function void (int int int int)) func))
```

The [foreign type specifier](https://wiki.call-cc.org/man/5/Foreign%20type%20specifiers)
`(function void (int int int int))` specifies the same callback type
as the C code above. When calling the binding in Scheme, we will need
to pass a pointer object, as if the foreign type were `c-pointer`. The
difference is that CHICKEN will coerce the pointer to the correct type
to make the C compiler happy.

This binding does not need to be safe, because `glutMouseFunc` does
not invoke any callback, it only sets a variable somewhere and then
returns. The callback is only invoked later, by `glutMainLoop`.

Now that we have a binding to the C library function, we need to
define the callback function. CHICKEN allows us to write inline C code
using `foreign-declare`, so one technique is to write the callback
function in C and then use `foreign-value` to get a pointer to it:

```scheme
(foreign-declare "
  void my_mouse_func(int button, int state, int x, int y) {
    printf(\"[func] Mouse button %d state %d at position %d, %d\\n\",
           button, state, x, y);
  }
")

;; This could use (function ...) instead of c-pointer like before,
;; but it is not necessary and makes no difference to the behavior.
(define my_mouse_func (foreign-value "my_mouse_func" c-pointer))

(glutMouseFunc my_mouse_func)
```

If you have a lot of C functions, you may prefer to put them in a C
file so that you can use your editor's C mode, and then use
`(foreign-declare "#include \"my_c_functions.c\"")` in your Scheme
file.

Writing callbacks in C gives us the best possible performance, which
may be important for callbacks that are invoked very frequently, but
this technique has some big downsides:

* We have to write C code instead of Scheme
* We cannot access the Scheme environment, except by using CHICKEN's
  [embedding API](https://wiki.call-cc.org/man/5/Embedding)
* The callback has to be compiled, so it can't be defined in evaluated
  code (using `csi` or `eval`)


## Callbacks Written with `define-callback`

The above technique can be improved by using Jiffi's
[`define-callback`](https://wiki.call-cc.org/eggref/5/jiffi#define-callback)
macro. It lets us write the callback body in Scheme, but it creates a
C function that can be called the same as any C function, including as
a callback. The Scheme code inside the function body can access the
Scheme environment (global or module-level variables, procedures, and
syntax), so it can serve as a bridge between the C environment and the
Scheme environment.

The callback must have the correct return type and argument types (but
the argument names don't matter):

```scheme
(define-callback my-mouse-callback
  return: void      ; could be omitted, void is the default
  args: ((int button)
         (int state)
         (int x)
         (int y))
  (printf "Mouse button ~A state ~A at position ~A, ~A~%"
          button state x y))

;; The variable my-mouse-callback is defined to hold a C function
;; pointer, which can be passed as the callback:
(glutMouseFunc my-mouse-callback)
```

This solves two of the downsides from earlier: the callback is written
in Scheme, and it can access the Scheme environment. But,
`define-callback` needs to be compiled, so we still cannot define a
callback in interpreted code.


## Callback Adapters

The technique can made more flexible, so that any Scheme procedure can
be used as a callback, even in interpreted code. This technique takes
advantage of the fact that callbacks defined with `define-callback`
can call other Scheme procedures.

The basic idea is simple: define a global or module-level Scheme
procedure, and define a callback "adapter" which calls that procedure:

```scheme
(define (my-mouse-procedure button state x y)
  (printf "Mouse button ~A state ~A at position ~A, ~A~%"
          button state x y))

(define-callback mouse-func-adapter
  args: ((int button)
         (int state)
         (int x)
         (int y))
  (my-mouse-procedure button state x y))

;; Pass the adapter, not the Scheme procedure
(glutMouseFunc mouse-func-adapter)
```

We can improve this further by using a parameter to hold the
procedure. This allows users to provide their own Scheme procedure
callback at runtime, even in interpreted code:

```scheme
(define *mouse-callback* (make-parameter #f))

(define-callback mouse-func-adapter
  args: ((int button)
         (int state)
         (int x)
         (int y))
  (let ((proc (*mouse-callback*)))
    (when proc
      (proc button state x y))))

;; Users only need the setter, the rest is implementation details.
(export mouse-func-set!)

(define (mouse-func-set! proc-or-false)
  ;; Update the parameter
  (*mouse-callback* proc-or-false)
  (if proc-or-false
      ;; If procedure was given, pass the new version of the adapter
      (glutMouseFunc mouse-func-adapter)
      ;; Otherwise, pass #f (NULL) so GLUT disables mouse callback
      (glutMouseFunc #f)))

;; In user code...

(mouse-func-set!
 (lambda (button state x y)
   (printf "Mouse button ~A state ~A at position ~A, ~A~%"
           button state x y)))
```

## Flexible Callbacks

Each of the three techniques above have a different balance of
performance versus convenience:

* Defining the callback in C gives the best performance, but it cannot
  easily access the Scheme environment, and it must be compiled.
* Using `define-callback` makes it easy to access the Scheme
  environment, but performance is not as good as C code, and it still
  must be compiled.
* Using callback adapters allows us to also use interpreted procedures
  as callbacks, but performance is not as good because of the
  additional step of looking up and calling the procedure from a
  global variable.

We (or the user of the library bindings) might want to use different
techniques in different cases, depending on how important performance
is. Fortunately, it is easy to support _all_ of these techniques, and
let the user decide which to use:

```scheme
;; callback can be a procedure, function pointer, or #f
(define (mouse-func-set! callback)
  (cond
   ;; Given a procedure, set the global variable and pass the adapter
   ;; as the callback function.
   ((procedure? callback)
    (*mouse-callback* callback)
    (glutMouseFunc mouse-func-adapter))

   ;; Given a C function pointer, possibly from define-callback,
   ;; clear the global variable and pass the pointer as the callback.
   ((pointer? callback)
    (*mouse-callback* #f)
    (glutMouseFunc callback))

   ;; glutMouseFunc also allows the callback to be NULL (#f) to
   ;; disable mouse callbacks.
   ((not callback)
    (*mouse-callback* #f)
    (glutMouseFunc #f))

   (else
    (error "Invalid callback" callback))))
```


## Dispatching to Multiple Procedures

The example of `glutMouseFunc` only requires one callback procedure,
but some C library functions allow you to set many different
callbacks. For example, consider a hypothetical library that allows us
to set callbacks for 20 different event types (mouse clicks, keyboard
buttons, etc.):

```c
/* Defines the FOO_EventCallback type to mean a pointer to a void
 * function that takes a FOO_Event pointer */
typedef void (*FOO_EventCallback)(FOO_Event *event);

/* Private array of callbacks, used by the C library */
FOO_EventCallback __FOO_EventCallbacks[20] = {};

void FOO_SetEventCallback(int type, FOO_EventCallback callback) {
  __FOO_EventCallbacks[type] = callback;
}
```

We could define 20 different adapters and 20 different parameters, but
that would make the code very cumbersome and repetitive. Instead, we
can define 1 adapter and 1 global data structure that can hold 20
procedures. The data structure could be an association list or hash
table, but in this case the possible keys (event types) are a finite
number of consecutive integers, so a vector is perfect:

```scheme
(define event-callbacks (make-vector 20 #f))

(define-callback event-callback-adapter
  args: ((FOO_Event* event))
  ;; Use find out what type of event it is,
  ;; then look up the appropriate callback procedure.
  (let* ((type (event-type event))
         (proc (vector-ref event-callbacks type)))
    (when proc
      (proc event))))

(define-binding FOO_SetEventCallback
  args: ((int type)
         ((function void ((c-pointer "FOO_Event")))
          callback)))

(export event-callback-set!)

(define (event-callback-set! type proc)
  ;; Store the procedure in the vector so the adapter can find it.
  (vector-set! event-callbacks type proc)
  ;; Pass the adapter as the callback function.
  (FOO_SetEventCallback type event-callback-adapter))

;; In user code...

(event-callback-set! 19 (lambda (event) (print event)))
```


## Local Callbacks via User Data Pointers

The above examples all involve callback procedures that are defined
globally, but it is also sometimes possible to use Scheme procedures
that are created locally using `lambda`. This is useful, for example,
with an iterator C function which invokes the callback for each item
in a collection.

This technique only works if the C function accepts an extra pointer
argument, often named `void *userdata` or `void *data`, which the C
function passes as an argument to the callback. For example:

```c
typedef void (*FOO_IterateCallback)(int i, void *userdata);

void FOO_Iterate(int loops, FOO_IterateCallback callback, void *userdata) {
  for (int i=0; i < loops; i++) {
    callback(i, userdata);
  }
}
```

Like in the techniques above, we can define an adapter using
`define-callback`. But instead of retrieving the callback procedure
from a global variable, we will pass along the procedure using the
`userdata` argument. Using `scheme-object` as the foreign type will
convert from a Scheme value to a `C_word` (the C type that CHICKEN
uses for Scheme values), or vice versa. Here is our first attempt, but
it has a flaw:

```scheme
;; BAD TECHNIQUE!
(define-callback foo-iterate-adapter
  args: ((int i)
         (scheme-object userdata))
  (assert (procedure? userdata)
          "Not a procedure" userdata i)
  (userdata i))

(define (foo-iterate loops proc)
  ;; You can define bindings inside a procedure to help keep your
  ;; code organized. It is a matter of personal preference.
  (define-binding** FOO_Iterate
    args: ((int loops)
           ((function void (int c-pointer)) callback)
           (scheme-object userdata))
    ;; Need to cast userdata from a C_word to a void pointer to
    ;; make the C compiler happy.
    "FOO_Iterate(loops, callback, (void*)userdata);")

  (FOO_Iterate loops foo-iterate-adapter proc))

(foo-iterate 10 (lambda (i) (print i)))
```

The above code will _usually_ work, because the callback procedure is
only called 10 times, and it does not allocate new objects. But the
example below will certainly fail at some point during iteration:

```scheme
(define *results* (make-parameter '()))

;; Iterate 1 million times, prepending to the results list.
(foo-iterate
 1000000
 (lambda (i) (*results* (cons i (*results*)))))

;; Error: Not a procedure
;; #<invalid forwarded object>
;; 11418
```

The iteration number where the assertion fails will vary, but
eventually `userdata` will no longer be a valid procedure, but instead
will be `#<invalid forwarded object>`.

This happens because the callback procedure eventually causes the
garbage collector (GC) to run. When CHICKEN's GC runs, it moves
non-garbage Scheme values to a new location in memory — including the
callback procedure itself! But `FOO_Iterate` will continue to pass the
old, invalid location to the adapter.

To solve this, we can use a
[GC root](https://wiki.call-cc.org/eggref/5/jiffi#gc-roots).
A GC root holds a reference to a Scheme value, and the reference will
continue to be valid even if the GC moves the Scheme value to a new
location in memory.

A GC root is not necessary if the adapter retrieves the procedure from
a global or module-level variable, like in the earlier examples,
because even if the GC runs, the variable will automatically refer to
the new location. It is only necessary here because the C function is
passing an out-of-date pointer.

Here is the final, reliable version of the code, using a GC root:

```scheme
;; Safe technique, using a GC root
(define-callback foo-iterate-adapter
  args: ((int i)
         ;; c-pointer instead of scheme-object
         (c-pointer userdata))
  ;; Use the GC root to get the procedure
  (let ((proc (gc-root-ref userdata)))
    (proc i)))

(define (foo-iterate loops proc)
  (define-binding FOO_Iterate
    args: ((int loops)
           ((function void (int c-pointer)) callback)
           ;; c-pointer instead of scheme-object
           (c-pointer userdata)))

  (call-with-gc-root proc
    (lambda (proc-root)
      (FOO_Iterate loops foo-iterate-adapter proc-root))))
```

The `foo-iterate` binding uses Jiffi's `call-with-gc-root` to create a
GC root for `proc`, and passes the GC root pointer as the `userdata`
argument. Then, the adapter uses `gc-root-ref` to retrive `proc`. Note
that because a GC root is a pointer, the code now uses the `c-pointer`
foreign type instead of `scheme-object`.

The code will now work correctly even if the GC runs during iteration:

```scheme
(define *results* (make-parameter '()))

(foo-iterate 1000000
 (lambda (i) (*results* (cons i (*results*)))))

(length (*results*))
;; 1000000
```


## Limitations of `define-callback`

`define-callback` is based on CHICKEN's
[`define-external`](https://wiki.call-cc.org/man/5/Module%20(chicken%20foreign)#define-external),
so it has the same limitations. Specifically, it does not capture the
lexical environment, which means that the callback's body cannot use
local variables from the surrounding environment.

You can avoid this problem entirely if you use `define-callback` only
in the global scope or directly inside a module. In other words, avoid
calling `define-callback` inside of a `let`, `lambda`, or other form
that introduces local variables.

The callback body can safely use global variables, variables imported
from other modules, and variables defined in the current module
(whether or not those variables are exported).

The callback body can also safely use `let`, `lambda`, `define`, etc.
to introduce local variables _inside the body_. The problem is only if
the callback body tries to "look outside" to find a local variable
that was introduced in the surrounding environment.

Example:

```scheme
;; Example safe binding that invokes a callback.
(define-binding** invoke-callback
  safe: #t
  args: (((function void ()) callback))
  "callback();")

;; Good. The let and lambda are inside the define-callback.
;; The local variables are introduced inside the body.
(define-callback say-hello
  (let ((messages (list "hello" "world!")))
    (map (lambda (message) (print message))
         messages)))
(invoke-callback say-hello)

;; BAD! The define-callback is inside the let, and looks outside to
;; find the local variable `message`.
(let ((message (list "hello" "world!")))
  (define-callback bad-say-hello
    (print message))
  (invoke-callback bad-say-hello))

;; BAD! The define-callback is inside a procedure definition, which
;; is just a convenient syntax for lambda. It looks outside to find
;; the local variable `message`.
(define (bad-make-callback message)
  (define-callback bad-callback
    (print message))
  bad-callback)
(invoke-callback (bad-make-callback (list "hello" "world!")))
```

It is technically possible to use `define-callback` inside a `let`,
`lambda`, etc., if the callback body does not use any local variables
from the surrounding lexical environment. But this is not recommended,
because it provides no benefit and increases the risk of bugs.
